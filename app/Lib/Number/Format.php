<?php
/**
 * Classe de Formatações
 *
 * Classe com diversas formatações para numerações.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       Number.Format
 * @since         Version 0.1.0

/**
 * Class Format
 *
 * @package Number.Format
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class Format {

    /**
     * Formata um número com a formação de cep.
     *
     * @param integer $number O número.
     * @since Version 0.1.0
     */
    public static function cep($number) {
        if (preg_match('/^(\d{2})(\d{3})(\d{3})$/', $number, $matches))
            return $matches[1] . '.' . $matches[2] . '-' . $matches[3];
        else
            return $number;
    }

    /**
     * Formata um número com a formação de cpf/cnpj.
     *
     * @param integer $number O número.
     * @since Version 0.1.0
     */
    public static function cpfcnpj($number) {
        if (preg_match('/^(\d{3})(\d{3})(\d{3})(\d{2})$/', $number, $matches))
            return $matches[1] . '.' . $matches[2] . '.' . $matches[3] . '-' . $matches[4];
        elseif (preg_match('/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})$/', $number, $matches))
            return $matches[1] . '.' . $matches[2] . '.' . $matches[3] . '/' . $matches[4] . '-' . $matches[5];
        else
            return $number;
    }

    /**
     * Formata um número com a formação de horário.
     *
     * @param integer $number O número.
     * @since Version 0.1.0
     */
    public static function horary($number) {
        if (preg_match('/^(\d{1})(\d{2})$/', $number, $matches))
            return $matches[1] . ':' . $matches[2];
        elseif (preg_match('/^(\d{2})(\d{2})$/', $number, $matches))
            return $matches[1] . ':' . $matches[2];
        else
            return $number;
    }

    /**
     * Formata um número com a formação de telefone.
     *
     * @param integer $number O número.
     * @since Version 0.1.0
     */
    public static function phone($number) {
        if (preg_match('/^(\d{4})(\d{4})$/', $number, $matches))
            return $matches[1] . '-' . $matches[2];
        elseif (preg_match('/^(\d{5})(\d{4})$/', $number, $matches))
            return $matches[1] . '-' . $matches[2];
        elseif (preg_match('/^(\d{2})(\d{4})(\d{4})$/', $number, $matches))
            return '(' . $matches[1] . ') ' . $matches[2] . '-' . $matches[3];
        elseif (preg_match('/^(\d{2})(\d{5})(\d{4})$/', $number, $matches))
            echo '(' . $matches[1] . ') ' . $matches[2] . '-' . $matches[3];
        else
            return $number;
    }

}
