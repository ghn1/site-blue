<?php
/**
 * Classe de Validações
 *
 * Classe com diversas validações para numerações.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       Number.Validate
 * @since         Version 0.1.0

/**
 * Class Validate
 *
 * @package Number.Validate
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class Validate {

    /**
     * Validação de CPF e CNPJ, se a string passada for um CPF ou CNPJ válido.
     * 
     * @since Version 0.1.0
     * @param string $check Uma string de CPF ou CNPJ válido.
     * @return boolean A validação da string passada.
     */
    public static function cpfcnpj($check) {
        $number_verifier = str_split(preg_replace('/\D/', '', $check));
        if (in_array(count($number_verifier), [11,14]))
            $cpfcnpj = array_splice($number_verifier, 0, count($number_verifier) - 2);
        else
            return false;
        $verifier = [];
        for ($n = 0; $n < 2; $n++) {
            $sum = 0;
            $default = [];
            if (count($cpfcnpj) < 11) {
                for ($i = 10 + $n; $i > 1; $i--)
                    $default[] = $i;
            } else {
                for ($i = 5 + $n; $i > 1; $i--)
                    $default[] = $i;
                for ($i = 9; $i > 1; $i--)
                    $default[] = $i;
            }
            for ($i = 0; $i < count($cpfcnpj) + $n; $i++)
                $sum += $cpfcnpj[$i] * $default[$i];
            $result = $sum % 11;
            $verifier[$n] = ($result < 2) ? 0 : 11 - $result;
            if ($n == 0)
                $cpfcnpj[count($cpfcnpj)] = $verifier[$n];
        }
        unset($cpfcnpj[count($cpfcnpj) - 1]);
        $rtn = false;
        if ($number_verifier == $verifier)
            $rtn = true;
        $cpfcnpjver = array_merge($cpfcnpj, $number_verifier);
        if (count(array_unique($cpfcnpjver)) == 1)
            $rtn = false;
        return $rtn;
    }

}
