<?php
/**
 * Classe de Imagens
 *
 * Classe com diversas utilidades de tratamentos com arquivos de imagens.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       Utility.Image
 * @since         Version 0.1.4

/**
 * Class Image
 *
 * @package Utility.Image
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class Image {

    /**
     * Corta a imagem e redimensiona de acordo com os parâmetros.
     *
     * @param string $filename O arquivo de imagem a ser tratado.
     * @param array $options Opções extras de acordo da seleção da imagem.
     * @return avoid|string Retorna o novo nome do arquivo caso seja alterado o formato.
     * @since Version 0.1.4
     */
    public static function cropResize($filename, $options = []) {
        
        /* O tamanho da largura e altura do arquivo de imagem. */
        list($img_width, $img_height) = getimagesize($filename);

        /* O tamanho da largura e altura da seleção na imagem. */
        $crop_height = empty($options['h2']) ? $img_height : $options['h2'];
        $crop_width = empty($options['w2']) ? $img_width : $options['w2'];

        /* Escala o novo tamanho da largura e altura na proporção do tamanho. */
        if (isset($options['width']) && isset($options['height'])
                || ($crop_height == $options['h1'] && $crop_width == $options['w1'])) {
            if ($crop_height == $options['h1'] && $crop_width == $options['w1']) {
                $crop_height = $img_height;
                $crop_width = $img_width;
            }
            if (isset($options['width']) && isset($options['height'])) {
                if (isset($options['x']) && isset($options['y']))
                    $scale = min($options['width'] / $crop_width, $options['height'] / $crop_height);
                else
                    $scale = max($options['width'] / $crop_width, $options['height'] / $crop_height);
            } else
                $scale = 1;
            $orig_height = empty($options['h1']) ? $crop_height : $options['h1'];
            $orig_width = empty($options['w1']) ? $crop_width : $options['w1'];
        } else {
            $scale = min($img_width / $crop_width, $img_height / $crop_height);
            $orig_height = empty($options['h1']) ? $img_height : $options['h1'];
            $orig_width = empty($options['w1']) ? $img_width : $options['w1'];
        }
        $x = empty($options['x']) ? 0 : $options['x'];
        $y = empty($options['y']) ? 0 : $options['y'];
        
        $new_orig_width = $orig_width * $scale;
        $new_orig_height = $orig_height * $scale;
        if (isset($options['max_width']) || isset($options['max_height'])) {
            if (isset($options['max_width']) && $new_orig_width > $options['max_width'])
                $scale_w = $options['max_width'] / $crop_width;
            if (isset($options['max_height']) && $new_orig_height > $options['max_height'])
                $scale_h = $options['max_height'] / $crop_height;
            $scale_max = min($scale_w, $scale_h);
            if ($scale_max == 0)
                $scale_max = max($scale_w, $scale_h);
            if ($scale_max != 0) {
                $scale = $scale_max;
                $new_orig_width = $orig_width * $scale;
                $new_orig_height = $orig_height * $scale;
            }
        }
        
        /* X e Y da seleção da imagem do tamanho escalado. */
        if (isset($options['x']) && isset($options['y'])) {
            $x *= $new_orig_width / $orig_width;
            $y *= $new_orig_height / $orig_height;
            $new_crop_width = $crop_width * $scale;
            $new_crop_height = $crop_height * $scale;
        } else {
            if (isset($options['width']) && isset($options['height'])) {
                if ($new_orig_width > $options['width'])
                    $x = $new_orig_width - $options['width'];
                if ($new_orig_height > $options['height'])
                    $y = $new_orig_height - $options['height'];
            }
            $new_crop_width = $new_orig_width - $x;
            $new_crop_height = $new_orig_height - $y;
            $x = $x / 2;
            $y = $y / 2;
        }
        
        $new_img = imagecreatetruecolor($new_crop_width, $new_crop_height);
        $extension = strtolower(substr(strrchr(basename($filename), '.'), 1));
        switch ($extension) {
            case 'gif':
                $src_img = imagecreatefromgif($filename);
                break;
            case 'jpg': case 'jpeg':
                $src_img = imagecreatefromjpeg($filename);
                break;
            case 'png':
                $src_img = imagecreatefrompng($filename);
                break;
            case 'webp':
                $src_img = imagecreatefromwebp($filename);
                break;
            default:
                exit('Formato de arquivo de imagem desconhecida.');
        }
        
        $format = (isset($options['format'])) ? $options['format'] : $extension;
        switch ($format) {
            case 'gif':
                $ext_img = '.gif';
                $write_image = 'imagegif';
                break;
            case 'jpg': case 'jpeg':
                $ext_img = '.jpg';
                $write_image = 'imagejpeg';
                $quality = isset($options['quality']) ? $options['quality'] : 100;
                if ($extension == 'png')
                    imagefill($new_img, 0, 0, imagecolorallocate($new_img, 255, 255, 255));
                break;
            case 'png':
                imagecolortransparent($new_img, imagecolorallocate($new_img, 255, 255, 255));
                imagealphablending($new_img, false);
                imagesavealpha($new_img, true);
                $ext_img = '.png';
                $write_image = 'imagepng';
                break;
            case 'webp':
                $ext_img = '.webp';
                $write_image = 'imagewebp';
                break;
            default:
                exit('Formato de arquivo de imagem desconhecida.');
        }
        
        if ($format != $extension) {
            unlink($filename);
            $pathinfo = pathinfo($filename);
            $filename = $pathinfo['dirname'] . DS . $pathinfo['filename'] . $ext_img;
            $new_filename = $pathinfo['filename'] . $ext_img;
        }
        
        imagecopyresampled($new_img, $src_img, 0 - $x, 0 - $y, 0, 0, $new_orig_width, $new_orig_height, $img_width, $img_height);
        $write_image($new_img, $filename, $quality);
        imagedestroy($src_img);
        imagedestroy($new_img);
        if (isset($new_filename))
            return $new_filename;
    }

}
