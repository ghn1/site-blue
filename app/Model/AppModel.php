<?php
/**
 * Modelo de aplicação para CakePHP.
 *
 * Este arquivo é um arquivo modelo de toda a aplicação. Você pode colocar
 * todos os métodos relacionados ao modelo em toda a aplicação aqui.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**
 * Modelo de aplicação para Cake.
 *
 * Adicione seus métodos de aplicação em toda a classe abaixo, seus modelos
 * os herdarão.
 *
 * @package       app.Model
 */
class AppModel extends Model {

    /** @var int Define o quão profundo deve buscar os dados do modelo associado. */
    public $recursive = -1;

}
