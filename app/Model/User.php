<?php
/**
 * Modelo de dados do usuário
 *
 * Este arquivo é um arquivo de modelo de dados do usuário.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       user.Model
 * @since         Version 0.1.10
 */

App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * Modelo de Usuário
 *
 * @package user.Model
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class User extends AppModel {

    /** @var array Define as regras de validação. */
    public $validate = [
        'name' => [
            'required' => ['rule' => 'notBlank'],
            'minlength' => ['rule' => ['minLength', 4]]
        ],
        'email' => [
            'required' => ['rule' => 'notBlank'],
            'validate' => ['rule' => ['email', true]],
            'unique' => ['rule' => 'isUnique']
        ],
        'password' => ['required' => ['rule' => 'notBlank', 'on' => 'create']],
        'password2' => ['pconfirm' => ['rule' => ['passwdConfirm']]], 
    ];

    /**
     * Trata os dados antes de salvar.
     *
     * @since Version 0.1.6
     */
    public function beforeSave($options = []) {
        if (!empty($this->data[$this->alias]['password']))
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        elseif (isset($this->data[$this->alias]['password']))
            unset($this->data[$this->alias]['password']);
        
        return true;
    }

    /**
     * Confirma se as senhas digitadas coincidem
     * 
     * @since Version 0.1.0
     * @return boolean Retorna TRUE se as senhas coincidirem.
     *                 Caso contrário retornará FALSE.
     */
    public function passwdConfirm() { 
        return ($this->data[$this->alias]['password'] === $this->data[$this->alias]['password2']);
    }

}