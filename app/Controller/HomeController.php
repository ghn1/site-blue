<?php
/**
 * Controller da home
 *
 * Este arquivo é um arquivo de controller da página inicial do website e do
 * painel administrativo.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       home.Controller
 * @since         Version 0.1.0
 */

App::uses('AppController', 'Controller');

/**
 * Controller Home
 *
 * @package home.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class HomeController extends AppController {

    /**
     * Carrega a página inicial do website.
     * 
     * @since Version 0.1.0
     */   
    public function index() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home');
    }public function home2() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_2');
    }public function home3() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_3');
    }public function home4() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_4');
    }public function home5() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_5');
    }public function home6() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_6');
    }public function home7() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_7');
    }public function home8() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_8');
    }public function home9() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/home_9');
    }
   
    /**
     * Carrega a página inicial do painel administrativo.
     * 
     * @since Version 0.1.0
     */
    public function admin_index() {
        $this->render('/Pages/admin/home');
    }

}
