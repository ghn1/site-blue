<?php
/**
 * Controller da home
 *
 * Este arquivo é um arquivo de controller da página inicial do website e do
 * painel administrativo.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       home.Controller
 * @since         Version 0.1.0
 */

App::uses('AppController', 'Controller');

/**
 * Controller Home
 *
 * @package home.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class ServicesController extends AppController {
  
    public function service1() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/services');
    }public function service2() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/service_single');
    }
}
   
