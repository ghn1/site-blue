<?php
/**
 * Controller de usuário
 *
 * Este arquivo é um arquivo de controller para exibição, inserção, alteração e exclusão
 * de dados de usuários utilizado como credenciais de acessos restritos ao sistema.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       users.Controller
 * @since         Version 0.1.6
 */

App::uses('AppController', 'Controller');
App::uses('Folder', 'Utility');

/**
 * Controller Users
 *
 * @package users.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class UsersController extends AppController {

    /**
     * Carrega a página de listagem dos usuários do painel administrativo.
     *
     * @since Version 0.1.3
     */
    public function admin_index() {
        if (isset($this->request->data['search']))
            $this->Session->write('UserSearch', $this->request->data['search']);
        $options['fields'] = ['id', 'name', 'email'];
        $options['conditions'] = [
            'id !=' => 1,
            'OR' => [
                'name REGEXP "(?=.*' . str_replace(' ', ')(?=.*', $this->Session->read('UserSearch')) . ')"',
                'email REGEXP "(?=.*' . str_replace(' ', ')(?=.*', $this->Session->read('UserSearch')) . ')"',
            ]
        ];
        if (isset($this->request->named['sort'])) {
            $dir = (strtolower($this->request->named['direction']) == 'desc') ? 'desc' : 'asc';
            $options['order'] = [$this->request->named['sort'] => $dir];
        } else
            $options['order'] = ['id' => 'desc'];
        $options['page'] = isset($this->request->named['page']) ? $this->request->named['page'] : 1;
        $options['limit'] = (isset($this->request->named['limit'])) ? $this->request->named['limit'] : 10;
        $this->paginate = $options;
        $this->set('data', $this->paginate('User'));
        $this->set('named', $this->request->named);
        $this->set('search', $this->Session->read('UserSearch'));
        $this->title = __('Usuários') . ' | ' . $this->title;
        $this->render('/Pages/admin/users/list');
    }

    /**
     * Carrega a página de formulário de cadastro dos usuários do painel administrativo.
     *
     * @since Version 0.1.6
     */
    public function admin_form() {
        $path = WWW_ROOT . 'img' . DS . 'user' . DS;
        if ($this->request->is(['post', 'put'])) {
            if ($this->User->save($this->request->data)) {
                if (!empty($this->request->data['User']['picture'])) {
                    if ($this->request->data['User']['picture'] === 'delete')
                        (new Folder($path . $this->User->id))->delete();
                    elseif (strstr($this->request->data['User']['picture'], ';', true) == 'data:image/png') {
                        (new Folder($path . $this->User->id))->delete();
                        mkdir($path . $this->User->id, 0755, true);
                        $file = fopen($path . $this->User->id . DS . md5(uniqid(rand(), true)) . '.png', 'wb');
                        $data64 = explode(',', $this->request->data['User']['picture']);
                        fwrite($file, base64_decode($data64[1]));
                        fclose($file);
                    }
                }
                $this->Flash->pnotify(__('Cadastro salvo com sucesso!'), [
                    'key' => 'admin', 'params' => [
                        'styling' => 'bootstrap3',
                        'addclass' => 'stack-bottomright',
                        'type' => 'success',
                        'delay' => 3000
                    ]
                ]);
                $this->redirect(['action' => 'index']);
            }
        } elseif (isset($this->request->named['id'])) {
            $id = $this->request->named['id'];
            if ($id != 1 && $data = $this->User->findById($id)) {
                unset($data['User']['password']);
                if ($files = (new Folder($path . $data['User']['id']))->find('.*\.png'))
                    $data['User']['picture'] = 'user/' . $data['User']['id'] . '/' . $files[0];
                $this->request->data = $data;
            } else
                $this->redirect(['action' => 'index']);
        }
        $this->title = __('Usuários') . ' | ' . $this->title;
        $this->addStyle(['admin/croppie', 'admin/users/form']);
        $this->addScript(['admin/croppie.min', 'admin/bootstrap-show-password.min']);
        $this->render('/Pages/admin/users/form');
    }

    /**
     * Exclue os dados do usuário do painel administrativo.
     *
     * @since Version 0.1.1
     */
    public function admin_delete() {
        $this->autoRender = false;
        if ($this->request->is('post')) {
            if ($this->User->deleteAll(['id' => $this->request->data['id']])) {
                $folder = new Folder();
                $path = WWW_ROOT . 'img' . DS . 'user' . DS;
                if (is_array($this->request->data['id'])) {
                    foreach ($this->request->data['id'] as $id)
                        $folder->delete($path . $id);
                } else
                    $folder->delete($path . $this->request->data['id']);
            }
        }
    }

}