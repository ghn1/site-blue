<?php
/**
 * Controller da home
 *
 * Este arquivo é um arquivo de controller da página inicial do website e do
 * painel administrativo.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       home.Controller
 * @since         Version 0.1.0
 */

App::uses('AppController', 'Controller');

/**
 * Controller Home
 *
 * @package home.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class PagesController extends AppController {
  
    public function page1() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/about_1');
    }public function page2() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/about_2');
    }public function page3() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/team');
    }public function page4() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/case');
    }public function page5() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/case_single');
    }public function page6() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/testmonial');
    }public function page7() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/career');
    }public function page8() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/pricing_table');
    }public function page9() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/faq');
    }
}
   
