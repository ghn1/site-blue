<?php
/**
 * Controller da home
 *
 * Este arquivo é um arquivo de controller da página inicial do website e do
 * painel administrativo.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       home.Controller
 * @since         Version 0.1.0
 */

App::uses('AppController', 'Controller');

/**
 * Controller Home
 *
 * @package home.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class FeaturesController extends AppController {
  
    public function feature1() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/addons_1');
    }public function feature2() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/addons_2');
    }public function feature3() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/addons_3');
    }public function feature4() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/404');
    }
}
   
