<?php
/**
 * Controller de nível de aplicativo
 *
 * Este arquivo é um arquivo de controller de toda a aplicação. Você pode colocar todos
 * os métodos relacionados com o controller de todo o aplicativo aqui.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licenciado sob a licença MIT
 * Para informações completas de direitos autorais e de licença, consulte o LICENSE.txt
 * A redistribuição dos arquivos devem manter o aviso de direitos autorais acima.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
App::uses('Folder', 'Utility');

/**
 * Controller de aplicação
 *
 * Adicione seus métodos de todo o aplicativo na classe abaixo, seus controllers
 * herdarão eles.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    /** @var array Os componentes adicionados na aplicação. */
    public $components = ['Session', 'Cookie', 'Flash', 'Auth', 'DebugKit.Toolbar'];

    /** @var array As classes model adicionados na aplicação.  */
    public $uses = ['User'];

    /** @var array Define os meta tags a ser adicionado no "<head>" da página. */
    protected $metatag = [];

    /** @var string Define o título da página html. */
    protected $title = '';

    /** @var boolean Define se o acesso é atraves de dispositivo móvel. */
    protected $is_mobile;

    /** @var array A lista de scripts vazia. */
    private $script = [];

    /** @var array A lista de estilos vazia. */
    private $style = [];

    /**
     * Adiciona na array os scripts a ser usados.
     *
     * @since Version 0.1.0
     * @param string|array $uri A uri de script para ser adicionado.
     */
    protected function addScript($uri) {
        if (is_array($uri)) {
            foreach ($uri as $uri2)
                $this->addScript($uri2);
        } elseif (!in_array($uri, $this->script))
            $this->script[] = $uri;
    }

    /**
     * Adiciona na array os estilos a ser usados.
     *
     * @since Version 0.1.2
     * @param string|array $path O caminho do estilo para ser adicionado.
     * @param string $options [optional] As opções do estilo.
     */
    protected function addStyle($path, $options = array()) {
        if (is_array($path)) {
            foreach ($path as $path2)
                $this->addStyle($path2, $options);
        } elseif (!array_key_exists($path, $this->style))
            $this->style = array_merge_recursive($this->style, [$path => $options]);
    }

    /**
     * O Callback beforeFilter para execução das tarefas a ser executadas antes
     * de cada ação dos controllers.
     *
     * @since Version 0.1.7
     */
    public function beforeFilter() {
        parent::beforeFilter();
        
        $this->Cookie->time = '90 days';
        $this->is_mobile = (new Mobile_Detect())->isMobile();
        $this->set('is_mobile', $this->is_mobile);

        if (!$this->Session->check('ModernizrBrowser.checked') && !$this->is_mobile) {
            $this->addStyle('change-browser');
            $this->addScript(['modernizr-browser.min', 'change-browser']);
            $this->Session->write('ModernizrBrowser.checked', true);
        }
        
        if ($this->request->prefix == 'admin') {
            if (!$this->request->is('ssl') && !in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1']))
                $this->redirect('https://' . env('SERVER_NAME') . $this->here);
            Security::setHash('md5');
            $this->Auth->authenticate = ['Form' => ['fields' => ['username' => 'email']]];
            $this->Auth->authorize = ['Controller'];
            $this->Auth->loginRedirect = $this->Auth->unauthorizedRedirect = ['controller' => 'home', 'action' => 'index'];
            $this->Auth->logoutRedirect = $this->Auth->loginAction = ['controller' => 'login', 'action' => 'index'];
            if ($this->Auth->loggedIn() || $this->Cookie->check('Auth.User')) {
                $user = $this->Auth->loggedIn() ? $this->Auth->user() : $this->Cookie->read('Auth.User');
                if ($this->User->findCountByIdAndEmail($user['id'], $user['email'])) {
                    $this->Auth->login($user);
                    $this->set('userdata', $user);
                } else {
                    $this->Flash->pnotify(__('Sua conta foi excluida!'), [
                        'key' => 'admin', 'params' => [
                            'styling' => 'bootstrap3',
                            'addclass' => 'alert-with-icon stack-modal',
                            'type' => 'error',
                            'icon' => 'font-icon font-icon-warning',
                            'delay' => 3000
                        ]
                    ]);
                    $this->Cookie->delete('Auth');
                    $this->redirect($this->Auth->logout());
                }
            }
            $this->title = __('Painel Administrativo da Tribo Propaganda');
            $this->layout = 'admin';
            $this->metatag = [
                ['name' => 'viewport', 'content' => 'width=device-width,maximum-scale=1.0,user-scalabre=0,initial-scale=1.0'],
                ['http-equiv' => 'expires', 'content' => '0'],
                ['name' => 'robots', 'content' => 'noarchive']
            ];
            $this->addStyle(['admin/font-awesome.min', 'admin/ladda-themeless.min', 'admin/sweetalert']);
            $this->addStyle(['admin/bootstrap-table.min', 'admin/datatables.min', 'admin/main']);
            $this->addScript(['admin/jquery.min', 'admin/tether.min', 'admin/bootstrap.min']);
            $this->addScript(['admin/bootstrap-maxlength', 'admin/spin.min', 'admin/ladda.min']);
            $this->addScript(['admin/sweetalert.min', 'admin/jquery.blockUI', 'admin/pnotify']);
            $this->addScript(['admin/popper.min', 'admin/datatables.min', 'admin/plugins', 'admin/main']);
        } else {
            $this->Auth->allow();
            $this->title = __('Nome do Site');
            $this->metatag = [
                ['name' => 'keywords', 'content' => 'Aqui vai as palavras chaves (separadas por vírgulas cada palavra, nunca espaço)'],
                ['name' => 'viewport', 'content' => (!$this->is_mobile ? 'width=1920' : 'width=device-width,maximum-scale=1.0,user-scalabre=0') . ',initial-scale=1.0'],
                ['http-equiv' => 'expires', 'content' => '0'],
                ['name' => 'robots', 'content' => 'noarchive']   
            ];
            
            $this->addStyle('bootstrap.min');
            $this->addStyle('animate'); 
            $this->addStyle('style'); 
            $this->addStyle('morris'); 
            $this->addStyle('responsive'); 
            $this->addStyle('font-awesome.min'); 
            $this->addStyle('icon-font'); 
            $this->addStyle('font-awesome.min'); 
            $this->addStyle('owl.carousel.min'); 
            $this->addStyle('owl.theme.default.min'); 
            $this->addStyle('colorbox');
            if ($this->is_mobile)
                $this->metatag[] = ['name' => 'theme-color', 'content' => '#c60009'];
        }
    }

    /**
     * O Callback beforeRender para execução das tarefas a ser executadas após
     * a lógica da ação de um controller, mas antes da view ser renderizada.
     *
     * @since Version 0.1.1
     */
    public function beforeRender() {
        parent::beforeRender();
        
        if (Configure::read('debug') == 0) {
            if ($this->name == 'CakeError' && !$this->params['admin'])
                $this->layout = 'error';
        }

        $this->set('head_title', $this->title);
        $this->set('head_metas', $this->metatag);
        $this->set('head_styles', $this->style);
        $this->set('head_scripts', $this->script);
    }
 
    /**
     * Fornece a autorização da autenticação do usuário logado se ele está
     * ou não autorizado.
     * 
     * @since Version 0.1.1
     * @param array $user Os dados do usuários logado.
     * @return boolean Retorna TRUE se o usuário está autorizado.
     *                 Caso contrário retornará FALSE.
     */
    public function isAuthorized($user) {
        return !empty($user);
    }

}
