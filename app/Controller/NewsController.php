<?php
/**
 * Controller da home
 *
 * Este arquivo é um arquivo de controller da página inicial do website e do
 * painel administrativo.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       home.Controller
 * @since         Version 0.1.0
 */

App::uses('AppController', 'Controller');

/**
 * Controller Home
 *
 * @package home.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class NewsController extends AppController {
  
    public function new1() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/news_left_sidebar');
    }public function new2() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/news_right_sidebar');
    }public function new3() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/news_single');
    }
}
   
