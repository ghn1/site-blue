<?php
/**
 * Controller de contato
 *
 * Este arquivo é um arquivo de controller das páginas de contatos.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       contact.Controller
 * @since         Version 0.1.3
 */

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');

/**
 * Controller Contacts
 *
 * @package contact.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class ContactsController extends AppController {
    
    public function contact1() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/contact_1');
    }public function contact2() {
        $this->set('head_descr', __(''));
        $this->render('/Pages/contact_2');
    }
    
    /** @var string A chave gerado pelo Google Maps. */
    protected $gmapkey = 'AIzaSyC6VEIOWNnFoZhFV_IVPnXKVblqI9M2JYs';

    /**
     * Carrega a página de contato do painel administrativo.
     * 
     * @since Version 0.1.3
     */
    
    public function admin_index() {
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $city = isset($data['jales']) ? 'jales' : 'fernandopolis';
            if (empty(array_filter($data[$city], function($v){return empty($v);}))) {
                $email = new CakeEmail('smtp');
                $email->viewVars(['name' => $data[$city]['name']]);
                $email->viewVars(['email' => $data[$city]['email']]);
                $email->viewVars(['subject' => $data[$city]['subject']]);
                $email->viewVars(['message' => $data[$city]['message']]);
                $email->template('admin/contacts', 'admin');
                $email->emailFormat('both');
                $email->to(($city == 'jales') ? 'tribodp@gmail.com' : 'tribofernandopolis@gmail.com');
                $email->bcc('contato@cesar.augustus.nom.br');
                $email->replyTo($data[$city]['email']);
                $email->subject('Contato pelo painel admin. do site: ' . Router::url('/', true));
                if ($email->send())
                    $this->Flash->pnotify(__('Mensagem enviada com sucesso!'), [
                        'key' => 'admin', 'params' => [
                            'styling' => 'bootstrap3',
                            'addclass' => 'stack-bottomright',
                            'type' => 'success',
                            'delay' => 3000
                        ]
                    ]);
                else
                    $this->Flash->pnotify(__('Erro ao enviar mensagem. Tente novamente ou entre em contato conosco por telefone.'), [
                        'key' => 'admin', 'params' => [
                            'text' => __('O e-mail ou senha é inválido.'),
                            'styling' => 'bootstrap3',
                            'addclass' => 'alert-with-icon stack-modal',
                            'type' => 'error',
                            'icon' => 'font-icon font-icon-warning',
                            'delay' => 6000
                        ]
                    ]);
            } else
                $this->set($data);
        }
        $this->title = __('Contato') . ' | ' . $this->title;
        $this->addStyle(['admin/jquery.flex.label', 'admin/contacts.min']);
        $this->addScript(($this->request->is('ssl') ? 'https' : 'http') . '://maps.google.com/maps/api/js?key=' . $this->gmapkey);
        $this->addScript(['admin/google-maps/infobox', 'admin/google-maps/script-init']);
        $this->addScript('admin/jquery.flex.label');
        $this->render('/Pages/admin/contacts');
    }

}
