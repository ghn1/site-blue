<?php
/**
 * Controller de login
 *
 * Este arquivo é um arquivo de controller para acessos restritos ao sistema, através
 * da autenticação usando as credenciais cadastradas no sistema.
 *
 * @copyright     Copyright (c) 2017-2018 Cesar Augustus Silva
 * @package       login.Controller
 * @since         Version 0.1.6
 */

App::uses('AppController', 'Controller');

/**
 * Controller Login
 *
 * @package login.Controller
 * @author Cesar A. Silva <contato@cesar.augustus.nom.br>
 */
class LoginController extends AppController {

    /**
     * Carrega a página de login de acesso pra autenticar as credencias de
     * usuário do painel administrativo.
     *
     * @since Version 0.1.4
     */
    public function admin_index() {
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->Flash->pnotify(__('Login bem-sucedido!'), [
                    'key' => 'admin', 'params' => [
                        'styling' => 'bootstrap3',
                        'addclass' => 'stack-bottomright',
                        'type' => 'success',
                        'delay' => 3000
                    ]
                ]);
                if ($this->request->data['User']['remember'])
                    $this->Cookie->write('Auth.User', $this->Auth->user());
            } else {
                $this->Flash->pnotify(__('Acesso Negado!'), [
                    'key' => 'admin', 'params' => [
                        'text' => __('O e-mail ou senha é inválido.'),
                        'styling' => 'bootstrap3',
                        'addclass' => 'alert-with-icon stack-modal',
                        'type' => 'error',
                        'icon' => 'font-icon font-icon-warning',
                        'delay' => 3000
                    ]
                ]);
            }
        }
        if ($this->Auth->loggedIn())
            $this->redirect($this->Auth->redirectUrl());
        $this->title = 'Login | ' . $this->title;
        $this->addStyle(['admin/jquery.flex.label', 'admin/login']);
        $this->addScript('admin/jquery.flex.label');
        $this->render('/Pages/admin/login');
    }

    /**
     * Exclui os dados de acesso na sessão e redireciona para controller 'Home'
     * do painel administrativo.
     *
     * @since Version 0.1.4
     */
    public function admin_logout() {
        $this->Flash->pnotify(__('Você fez logout!'), [
            'key' => 'admin', 'params' => [
                'styling' => 'bootstrap3',
                'addclass' => 'stack-modal',
                'type' => 'success',
                'delay' => 3000
            ]
        ]);
        $this->Cookie->delete('Auth');
        $this->redirect($this->Auth->logout());
    }

}