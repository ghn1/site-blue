<?php
/**
 * Este é o arquivo de configuração do núcleo.
 *
 * Use-o para configurar o comportamento central do Cake.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licenciado sob a licença MIT
 * Para informações completas de direitos autorais e de licença, consulte o LICENSE.txt
 * A redistribuição dos arquivos devem manter o aviso de direitos autorais acima.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

//setLocale(LC_ALL, 'deu');
//Configure::write('Config.language', 'deu');

/**
 * CakePHP Nível de depuração:
 *
 * Modo de produção:
 * 	0: Não há mensagens de erro, erros ou avisos. Redirecionamento de mensagens instantâneas.
 *
 * Modo de desenvolvimento:
 * 	1: Erros e avisos mostrados, caches de modelos atualizados, mensagens instantâneas interrompidas.
 * 	2: Como em 1, mas também com mensagens de depuração completas e saída SQL.
 *
 * No modo de produção, as mensagens instantâneas redirecionam após um intervalo de tempo.
 * No modo de desenvolvimento, você precisa clicar na mensagem flash para continuar.
 */
	Configure::write('debug', 2);

/**
 * Configure o manipulador de Erro usado para lidar com erros para sua aplicação. Por padrão
 * ErrorHandler::handleError() é usado. Ele exibirá erros usando Debugger, quando debug > 0
 * and log errors with CakeLog when debug = 0.
 *
 * Opções:
 *
 * - `handler` - callback - O retorno de chamada para lidar com erros. Você pode definir isso para
 *    qualquer tipo de chamada, incluindo funções anônimas.
 *   Certifique-se de adicionar App::uses('MyHandler', 'Error'); ao usar uma classe de manipulador personalizado
 * - `level` - integer - O nível de erros que você está interessado em capturar.
 * - `trace` - boolean - Inclua traços de pilha para erros em arquivos de log.
 *
 * @see ErrorHandler para obter mais informações sobre o gerenciamento e configuração de erros.
 */
	Configure::write('Error', array(
		'handler' => 'ErrorHandler::handleError',
		'level' => E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED,
		'trace' => true
	));

/**
 * Configure o manipulador de Exceção usado para exceções não captadas. Por padrão,
 * ErrorHandler::handleException() é usado. Ele exibirá uma página HTML para a exceção, e
 * enquanto debug > 0, erros de estrutura como Missing Controller serão exibidos. Quando debug = 0,
 * erros de estrutura serão coagidos em erros HTTP genéricos.
 *
 * Opções:
 *
 * - `handler` - callback - O retorno de chamada para lidar com exceções. Você pode definir isso para
 *   qualquer tipo de retorno de chamada, incluindo funções anônimas.
 *   Certifique-se de adicionar App::uses('MyHandler', 'Error'); ao usar uma classe de manipulador personalizado
 * - `renderer` - string - A classe responsável por renderizar exceções não captadas. Se você escolher uma classe
 *    personalizada, você deve colocar o arquivo para essa classe no app/Lib/Error. Esta classe precisa implementar
 *    um método de renderização.
 * - `log` - boolean - As exceções devem ser registradas?
 * - `extraFatalErrorMemory` - integer - Aumenta o limite de memória no desligamento para que erros fatais sejam
 *    registrados. Especificamos quantidade em megabytes ou use 0 para desabilitar (padrão: 4 MB)
 * - `skipLog` - array - lista de exceções para saltar para registro. Exceções que
 *   estender uma das exceções listadas também será ignorada para o registro.
 *   Exemplo: `'skipLog' => array('NotFoundException', 'UnauthorizedException')`
 *
 * @see ErrorHandler para obter mais informações sobre manipulação e configuração de exceções.
 */
	Configure::write('Exception', array(
		'handler' => 'ErrorHandler::handleException',
		'renderer' => 'ExceptionRenderer',
		'log' => true
	));

/**
 * Codificação de charset de largura da aplicação
 */
	Configure::write('App.encoding', 'UTF-8');

/**
 * Para configurar CakePHP *não* usar mod_rewrite e usar
 * CakePHP URLs bonitas, remova esses arquivos .htaccess:
 *
 * /.htaccess
 * /app/.htaccess
 * /app/webroot/.htaccess
 *
 * E descomente o App.baseUrl abaixo. Mas tenha em mente
 * que recursos do plugin, como arquivos de imagens, CSS e JavaScript
 * não funcionará sem reescrita de URL!
 * Para contornar esse problema, você deve simular ou copiar
 * os recursos do plugin no diretório webroot do seu aplicativo. Isto é
 * recomendado mesmo quando você está usando mod_rewrite. Manipulação estática
 * ativos através do Dispatcher é incrivelmente ineficiente e
 * incluído principalmente como uma conveniência de desenvolvimento - e
 * assim não recomendado para aplicações de produção.
 */
	//Configure::write('App.baseUrl', env('SCRIPT_NAME'));

/**
 * Para configurar o CakePHP para usar um URL de domínio específico
 * para qualquer geração de URL dentro do aplicativo, defina a seguinte
 * variável de configuração para o endereço http (s) para seu domínio. Este
 * substituirá a detecção automática de URL de base completa e pode ser
 * útil ao gerar links da CLI (por exemplo, enviar e-mails)
 */
	//Configure::write('App.fullBaseUrl', 'http://example.com');

/**
 * O diretório base no qual o aplicativo reside. Deve ser usado se o aplicativo
 * é executado em uma subpasta e App.fullBaseUrl estiver configurado.
 */
	//Configure::write('App.base', '/my_app');

/**
 * Caminho da Web para o diretório de imagens públicas sob webroot.
 * Se não for definido, o padrão é 'img/'
 */
	//Configure::write('App.imageBaseUrl', 'img/');

/**
 * Caminho da Web para o diretório de arquivos CSS no webroot.
 * Se não for definido, o padrão é 'css/'
 */
	//Configure::write('App.cssBaseUrl', 'css/');

/**
 * Caminho da Web para o diretório js files em webroot.
 * Se não for definido, o padrão é 'js/'
 */
	//Configure::write('App.jsBaseUrl', 'js/');

/**
 * Descomente a definição abaixo para usar rotas de prefixo CakePHP.
 *
 * O valor da definição determina os nomes das rotas
 * e suas ações de controlador associadas:
 *
 * Defina para uma série de prefixos que deseja usar em seu aplicativo. Use para
 * admin ou outras rotas prefixadas.
 *
 * 	Routing.prefixes = array('admin', 'manager');
 *
 * Habilita:
 *	`admin_index()` e `/admin/controller/index`
 *	`manager_index()` e `/manager/controller/index`
 */
	Configure::write('Routing.prefixes', array('admin'));

/**
 * Desligue todo o armazenamento de cache em toda a aplicação.
 */
	//Configure::write('Cache.disable', true);

/**
 * Habilitar verificação de cache.
 *
 * Se definido como verdadeiro, para o armazenamento em cache de visualização,
 * você ainda deve usar o controlador public $cacheAction dentro de seus
 * controladores para definir as configurações de cache. Você pode configurá-lo
 * de todo o controlador, definindo public $cacheAction = true, ou em cada ação
 * usando $this->cacheAction = true.
 */
	//Configure::write('Cache.check', true);

/**
 * Ativar prefixos de exibição de cache.
 *
 * Se configurado, ele será precedido do nome do cache para o armazenamento em
 * cache do arquivo de exibição. Isso é útil se você implantar o mesmo aplicativo
 * por meio de vários subdomínios e idiomas, por exemplo. Cada versão pode ter
 * seu próprio espaço de nomes de cache de exibição.
 * Nota: O nome final do arquivo de cache será então `prefix_cachefilename`.
 */
	//Configure::write('Cache.viewPrefix', 'prefix');

/**
 * Configuração da sessão.
 *
 * Contém uma série de configurações para usar para a configuração da sessão. A chave padrão é
 * usado para definir uma predefinição padrão para usar nas sessões, todas as configurações
 * declaradas aqui serão substituídas as configurações da configuração padrão.
 *
 * ## Opções
 *
 * - `Session.cookie` - O nome do cookie para usar. O padrão é 'CAKEPHP'
 * - `Session.timeout` - O número de minutos para o qual você deseja que as sessões vivam.
 *    Esse tempo limite é gerenciado pelo CakePHP
 * - `Session.cookieTimeout` - O número de minutos que você deseja que os cookies de sessão vivam.
 * - `Session.checkAgent` - Deseja que o agente do usuário seja verificado ao iniciar as sessões?
 *    Você pode querer definir o valor como falso, ao lidar com versões antigas do IE, Chrome Frame ou
 *    de determinados dispositivos de navegação na web e AJAX.
 * - `Session.defaults` - A configuração padrão configurada para usar como base para sua sessão.
 *    Existem quatros padrões: php, cake, cache, database.
 * - `Session.handler` - Pode ser usado para habilitar um manipulador de sessão personalizado. Espera
 *    uma série de callables, que pode ser usado com `session_save_handler`. A utilização desta opção
 *    irá adicionar automaticamente `session.save_handler` para a matriz ini.
 * - `Session.autoRegenerate` - Ativando esta configuração, ativa a renovação automática de sessões e
 *    sessões que mudam com freqüência. Consulte CakeSession::$requestCountdown.
 * - `Session.cacheLimiter` - Configure os cabeçalhos de controle do cache usados para o cookie da sessão.
 *   Veja http://php.net/session_cache_limiter para valores aceitos.
 * - `Session.ini` - Uma matriz associativa de valores ini adicionais para definir.
 *
 * Os padrões embutidos são:
 *
 * - 'php' - Usa configurações definidas em seu php.ini.
 * - 'cake' - Salva arquivos de sessão no diretório /tmp do CakePHP.
 * - 'database' - Usa as sessões de banco de dados do CakePHP.
 * - 'cache' - Use a classe Cache para salvar sessões.
 *
 * Para definir um manipulador de sessão personalizado, salve-o em /app/Model/Datasource/Session/<name>.php.
 * Certifique-se de que a classe implements `CakeSessionHandlerInterface` e configure Session.handler para <name>
 *
 * Para usar sessões de banco de dados, execute o esquema app/Config/Schema/sessions.php usando
 * o comando do bolo: o esquema do bolo cria sessões
 */
	Configure::write('Session', array(
		'defaults' => 'php',
		'cookieTimeout' => 0,
		'checkAgent' => false,
		'ini' => array(
			'session.cookie_secure' => false
                )
	));

/**
 * Uma seqüência aleatória usada em métodos de hashing de segurança.
 */
	Configure::write('Security.salt', '4grEAxc4e84mwO5NvjZCzKQe1eBxrrzfban0013X');

/**
 * Uma cadeia numérica aleatória (apenas dígitos) usada para criptografar/descriptografar strings.
 */
	Configure::write('Security.cipherSeed', '224439273480045277831697927995');

/**
 * Aplicar timestamps com o último tempo modificado para ativos estáticos (js, css, imagens).
 * Anexará um parâmetro de string de consulta contendo a hora em que o arquivo foi modificado.
 * Isto é útil para invalidar caches do navegador.
 *
 * Defina para `true` para aplicar timestamps quando debug > 0. Defina para 'force' para sempre ativar
 * timestamping independentemente do valor de depuração.
 */
	//Configure::write('Asset.timestamp', true);

/**
 * Comprimir saída CSS removendo comentários, espaços em branco, repetição de tags, etc.
 * Isso requer um diretório /var/cache para ser gravado pelo servidor web para armazenamento em cache.
 * e /vendors/csspp/csspp.php
 *
 * Para usar, prefira o URL do link CSS com '/ccss/' em vez de '/css/' ou use HtmlHelper::css().
 */
	//Configure::write('Asset.filter.css', 'css.php');

/**
 * Conecte seu próprio compressor de JavaScript personalizado soltando um script em seu webroot para
 * lidar com a saída e configurando a configuração abaixo para o nome do script.
 *
 * Para usar, prefira os URLs do link de JavaScript com '/cjs/' em vez de '/js/' ou use JsHelper::link().
 */
	//Configure::write('Asset.filter.js', 'custom_javascript_output_filter.php');

/**
 * O nome da classe e o banco de dados usados nas listas de controle de acesso do CakePHP.
 */
	Configure::write('Acl.classname', 'DbAcl');
	Configure::write('Acl.database', 'default');

/**
 * Desconecte esta linha e corrija o fuso horário do servidor para corrigir os
 * erros relacionados com a data e hora.
 */
	//date_default_timezone_set('UTC');

/**
 * `Config.timezone` está disponível no qual você pode configurar o timezone string dos usuários.
 * Se um método da classe CakeTime é chamado com o parâmetro $timezone como nulo e `Config.timezone`
 * é definido, então o valor de `Config.timezone` será usado. Esse recurso permite que você defina o
 * fuso horário dos usuários apenas uma vez em vez de passá-lo cada vez em chamadas de função.
 */
	//Configure::write('Config.timezone', 'Europe/Paris');

/**
 * Configuração do mecanismo de cache
 * Configurações padrão fornecidas abaixo
 *
 * Mecanismo de armazenamento de arquivos.
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'File', //[requerido]
 *		'duration' => 3600, //[opcional]
 *		'probability' => 100, //[opcional]
 * 		'path' => CACHE, //[opcional] use o diretório tmp do sistema - lembre-se de usar o caminho absoluto
 * 		'prefix' => 'cake_', //[opcional] prefixa cada arquivo de cache com esta string
 * 		'lock' => false, //[opcional] usa bloqueio de arquivo
 * 		'serialize' => true, //[opcional]
 * 		'mask' => 0664, //[opcional]
 *	));
 *
 * APC (http://pecl.php.net/package/APC)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Apc', //[requerido]
 *		'duration' => 3600, //[opcional]
 *		'probability' => 100, //[opcional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[opcional] prefira cada arquivo de cache com essa string
 *	));
 *
 * Xcache (http://xcache.lighttpd.net/)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Xcache', //[requerido]
 *		'duration' => 3600, //[opcional]
 *		'probability' => 100, //[opcional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[opcional] prefira cada arquivo de cache com essa string
 *		'user' => 'user', //usuário das configurações do xcache.admin.user
 *		'password' => 'password', //senha de texto sem formatação (xcache.admin.pass)
 *	));
 *
 * Memcached (http://www.danga.com/memcached/)
 *
 * Usa a extensão memcached. Veja http://php.net/memcached
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Memcached', //[requerido]
 *		'duration' => 3600, //[opcional]
 *		'probability' => 100, //[opcional]
 * 		'prefix' => Inflector::slug(APP_DIR) . '_', //[opcional] prefira cada arquivo de cache com essa string
 * 		'servers' => array(
 * 			'127.0.0.1:11211' // localhost, porta padrão 11211
 * 		), //[optional]
 * 		'persistent' => 'my_connection', // [opcional] O nome da conexão persistente.
 * 		'compress' => false, // [opcional] comprimir dados em Memcached (mais lento, mas usa menos memória)
 *	));
 *
 *  Wincache (http://php.net/wincache)
 *
 * 	 Cache::config('default', array(
 *		'engine' => 'Wincache', //[requerido]
 *		'duration' => 3600, //[opcional]
 *		'probability' => 100, //[opcional]
 *		'prefix' => Inflector::slug(APP_DIR) . '_', //[opcional] prefira cada arquivo de cache com essa string
 *	));
 */

/**
 * Configure os manipuladores de cache que CakePHP usará para metadados internos,
 * como mapas de classes e esquema de modelo.
 *
 * Por padrão, o arquivo é usado, mas para melhorar o desempenho você deve usar o APC.
 *
 * Nota: 'default' e outros caches de aplicativos devem ser configurados no app/Config/bootstrap.php.
 *       Verifique os comentários no bootstrap.php para obter mais informações sobre os mecanismos de cache disponíveis
 *       e suas configurações.
 */
$engine = 'File';

// No modo de desenvolvimento, os caches devem expirar rapidamente.
$duration = '+999 days';
if (Configure::read('debug') > 0) {
	$duration = '+10 seconds';
}

// Prefixar cada aplicação no mesmo servidor com uma string diferente, para evitar conflitos Memcache e APC.
$prefix = 'myapp_';

/**
 * Configure o cache usado para o cache de estrutura geral. As informações do caminho,
 * as listas de objetos e os arquivos de cache de tradução são armazenados com esta configuração.
 */
Cache::config('_cake_core_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_core_',
	'path' => CACHE . 'persistent' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));

/**
 * Configure o cache para caches de modelo e fonte de dados. Essa configuração de cache
 * é usada para armazenar descrições de esquema e listas de tabelas em conexões.
 */
Cache::config('_cake_model_', array(
	'engine' => $engine,
	'prefix' => $prefix . 'cake_model_',
	'path' => CACHE . 'models' . DS,
	'serialize' => ($engine === 'File'),
	'duration' => $duration
));
