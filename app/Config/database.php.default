<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licenciado sob a licença MIT
 * Para informações completas de direitos autorais e de licença, consulte o LICENSE.txt
 * A redistribuição dos arquivos devem manter o aviso de direitos autorais acima.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Classe de configuração do banco de dados.
 *
 * Você pode especificar várias configurações para produção, desenvolvimento e teste.
 *
 * datasource => O nome de uma fonte de dados suportada; as opções válidas são as seguintes:
 *  Database/Mysql - MySQL 4 & 5,
 *  Database/Sqlite - SQLite (somente PHP5),
 *  Database/Postgres - PostgreSQL 7 e superior,
 *  Database/Sqlserver - Microsoft SQL Server 2005 e superior
 *
 * Você pode adicionar fontes de dados de banco de dados personalizadas (ou substituir
 * fontes de dados existentes), adicionando o arquivo apropriado ao app/Model/Datasource/Database.
 * Os fontes de dados devem ser chamados 'MyDatasource.php',
 *
 *
 * persistent => true / false
 * Determina se o banco de dados deve ou não usar uma conexão persistente
 *
 * host =>
 * o host que você se conecta ao banco de dados. Para adicionar um número de soquete
 * ou porta, use 'port' => #
 *
 * prefix =>
 * Usa o prefixo fornecido para todas as tabelas neste banco de dados. Esta configuração pode
 * ser substituída por uma base de tabela com a propriedade Model::$tablePrefix.
 *
 * schema =>
 * Para o Postgres/Sqlserver especifica qual esquema você gostaria de usar as tabelas em.
 * O Postgres padrão é 'public'. Para SQLServer, o padrão é para esvaziar e usar
 * o esquema padrão do usuário conectado (tipicamente 'dbo').

 *
 * encoding =>
 * Para o MySQL, o Postgres especifica a codificação de caracteres a ser usada quando se liga ao
 * banco de dados. Usa padrão de banco de dados não especificado.
 *
 * sslmode =>
 * Para o Postgres especifica se 'disable', 'allow', 'prefer', ou 'require' SSL para a
 * conexão. O valor padrão é 'allow'.
 *
 * unix_socket =>
 * Para que o MySQL se conecte via socket, especifique o parâmetro `unix_socket` em vez de `host` e `port`
 *
 * settings =>
 * Array de pares de chave/valor, na conexão executa instruções SET para cada par
 * Para o MySQL : http://dev.mysql.com/doc/refman/5.6/en/set-statement.html
 * Para o Postgres : http://www.postgresql.org/docs/9.2/static/sql-set.html
 * Para o SQLServer : http://msdn.microsoft.com/en-us/library/ms190356.aspx
 *
 * flags =>
 * Uma array de chave/valor de opções de conexão específicas do driver.
 */
class DATABASE_CONFIG {

	public $default = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'user',
		'password' => 'password',
		'database' => 'database_name',
		'prefix' => '',
		//'encoding' => 'utf8',
	);

	public $test = array(
		'datasource' => 'Database/Mysql',
		'persistent' => false,
		'host' => 'localhost',
		'login' => 'user',
		'password' => 'password',
		'database' => 'test_database_name',
		'prefix' => '',
		//'encoding' => 'utf8',
	);
}
