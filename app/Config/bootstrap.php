<?php
/**
 * Este arquivo é carregado automaticamente pelo arquivo app/webroot/index.php após core.php
 *
 * Este arquivo deve carregar/criar qualquer configurações de configuração de todo o aplicação, como
 * Caching, Logging, carregamento de arquivos de configuração adicionais.
 *
 * Você também deve usar esse arquivo para incluir todos os arquivos que fornecem funções/constantes
 * globais que sua aplicação usa.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licenciado sob a licença MIT
 * Para informações completas de direitos autorais e de licença, consulte o LICENSE.txt
 * A redistribuição dos arquivos devem manter o aviso de direitos autorais acima.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.10.8.2117
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

// Configure uma configuração de cache 'padrão' para uso na aplicação.
Cache::config('default', array('engine' => 'File'));

/**
 * As configurações abaixo podem ser usadas para definir caminhos adicionais para models, views e controllers.
 *
 * App::build(array(
 *     'Model'                     => array('/path/to/models/', '/next/path/to/models/'),
 *     'Model/Behavior'            => array('/path/to/behaviors/', '/next/path/to/behaviors/'),
 *     'Model/Datasource'          => array('/path/to/datasources/', '/next/path/to/datasources/'),
 *     'Model/Datasource/Database' => array('/path/to/databases/', '/next/path/to/database/'),
 *     'Model/Datasource/Session'  => array('/path/to/sessions/', '/next/path/to/sessions/'),
 *     'Controller'                => array('/path/to/controllers/', '/next/path/to/controllers/'),
 *     'Controller/Component'      => array('/path/to/components/', '/next/path/to/components/'),
 *     'Controller/Component/Auth' => array('/path/to/auths/', '/next/path/to/auths/'),
 *     'Controller/Component/Acl'  => array('/path/to/acls/', '/next/path/to/acls/'),
 *     'View'                      => array('/path/to/views/', '/next/path/to/views/'),
 *     'View/Helper'               => array('/path/to/helpers/', '/next/path/to/helpers/'),
 *     'Console'                   => array('/path/to/consoles/', '/next/path/to/consoles/'),
 *     'Console/Command'           => array('/path/to/commands/', '/next/path/to/commands/'),
 *     'Console/Command/Task'      => array('/path/to/tasks/', '/next/path/to/tasks/'),
 *     'Lib'                       => array('/path/to/libs/', '/next/path/to/libs/'),
 *     'Locale'                    => array('/path/to/locales/', '/next/path/to/locales/'),
 *     'Vendor'                    => array('/path/to/vendors/', '/next/path/to/vendors/'),
 *     'Plugin'                    => array('/path/to/plugins/', '/next/path/to/plugins/'),
 * ));
 */

/**
 * As regras de Inflector personalizadas podem ser configuradas para pluralizar ou singularizar a tabela, o model,
 * os nomes das controllers ou qualquer outra string passada para as funções de inflection
 *
 * Inflector::rules('singular', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 * Inflector::rules('plural', array('rules' => array(), 'irregular' => array(), 'uninflected' => array()));
 */

/**
 * Os plugins precisam ser carregados manualmente, você pode carregá-los um a um ou todos eles em uma única chamada
 * Descomente uma das linhas abaixo, como você precisa. Certifique-se de ler a documentação no CakePlugin para usar
 * formas mais avançadas de carregar os plugins
 *
 * CakePlugin::loadAll(); // Carrega todos os plugins de uma só vez
 * CakePlugin::load('DebugKit'); // Carrega um único plugin chamado DebugKit
 */
CakePlugin::load('DebugKit');

/**
 * Para preferir a tradução do aplicativo sobre a tradução do plugin, você pode configurar
 *
 * Configure::write('I18n.preferApp', true);
 */

/**
 * Você pode anexar ouvintes de eventos ao ciclo de vida da solicitação como Filtro Dispatcher. Por padrão, o CakePHP agrupa dois filtros:
 *
 * - AssetDispatcher filtro servirá seus arquivos de ativos (css, imagens, js, etc.) de seus temas e plugins
 * - CacheDispatcher filtro irá ler a variável de configuração Cache.check e tentar servir conteúdo em cache gerado a partir de controladores
 *
 * Sinta-se à vontade para remover ou adicionar filtros como você vê em sua aplicação. Alguns exemplos:
 *
 * Configure::write('Dispatcher.filters', array(
 *		'MyCacheFilter', //  usarei a classe MyCacheFilter no pacote Routing/Filter em sua aplicação.
 *		'MyCacheFilter' => array('prefix' => 'my_cache_'), //  usará a classe MyCacheFilter no pacote Routing/Filter em sua aplicação com array de configurações.
 *		'MyPlugin.MyFilter', // usará a classe MyFilter no pacote Routing/Filter no plugin MyPlugin.
 *		array('callable' => $aFunction, 'on' => 'before', 'priority' => 9), // Um tipo de retorno de chamada PHP válido para ser chamado antes do envio
 *		array('callable' => $anotherMethod, 'on' => 'after'), // Um tipo de retorno de chamada PHP válido para ser chamado depois do envio
 *
 * ));
 */
Configure::write('Dispatcher.filters', array(
	'AssetDispatcher',
	'CacheDispatcher'
));

/**
 * Configura opções padrão de registro de arquivos
 */
App::uses('CakeLog', 'Log');
CakeLog::config('debug', array(
	'engine' => 'File',
	'types' => array('notice', 'info', 'debug'),
	'file' => 'debug',
));
CakeLog::config('error', array(
	'engine' => 'File',
	'types' => array('warning', 'error', 'critical', 'alert', 'emergency'),
	'file' => 'error',
));

/**
 * Arquivo Autoload para que você possa instanciar todas as classes de fornecedores diretamente
 */
App::import('Vendor', ['file' => 'autoload']);
