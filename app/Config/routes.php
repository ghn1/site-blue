<?php
/**
 * Configuração de rotas
 *
 * Neste arquivo, você configura rotas de seus controllers e suas actions.
 * As rotas são mecanismo muito importante que permite conectar livremente
 * URLs diferentes para escolher os controllers e suas actions (funções).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licenciado sob a licença MIT
 * Para informações completas de direitos autorais e de licença, consulte o LICENSE.txt
 * A redistribuição dos arquivos devem manter o aviso de direitos autorais acima.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Aqui, estão todas os caminhos conectando cada um no seu determinado controller,
 * suas actions e alguns parâmetros.
 */

    /** Rotas para controller chamado 'Home'. */
    Router::connect('/', ['controller' => 'home']);
    Router::connect('/admin', ['controller' => 'home', 'admin' => true]);
    
    Router::connect('/hometkkkkkwo', ['controller' => 'home', 'action' => 'home2']);
    Router::connect('/homethree', ['controller' => 'home', 'action' => 'home3']);
    Router::connect('/homefour', ['controller' => 'home', 'action' => 'home4']);
    Router::connect('/homefive', ['controller' => 'home', 'action' => 'home5']);
    Router::connect('/homesix', ['controller' => 'home', 'action' => 'home6']);
    Router::connect('/homeseven', ['controller' => 'home', 'action' => 'home7']);
    Router::connect('/homeeight', ['controller' => 'home', 'action' => 'home8']);
    Router::connect('/homenine', ['controller' => 'home', 'action' => 'home9']);
    
    Router::connect('/about1', ['controller' => 'pages', 'action' => 'page1']);
    Router::connect('/about2', ['controller' => 'pages', 'action' => 'page2']);
    Router::connect('/team', ['controller' => 'pages', 'action' => 'page3']);
    Router::connect('/case', ['controller' => 'pages', 'action' => 'page4']);
    Router::connect('/casesingle', ['controller' => 'pages', 'action' => 'page5']);
    Router::connect('/testmonial', ['controller' => 'pages', 'action' => 'page6']);
    Router::connect('/career', ['controller' => 'pages', 'action' => 'page7']);
    Router::connect('/pricingtable', ['controller' => 'pages', 'action' => 'page8']);
    Router::connect('/faq', ['controller' => 'pages', 'action' => 'page9']);
    
    Router::connect('/services', ['controller' => 'services', 'action' => 'service1']);
    Router::connect('/servicesingle', ['controller' => 'services', 'action' => 'service2']);
    
    Router::connect('/addons1', ['controller' => 'features', 'action' => 'feature1']);
    Router::connect('/addons2', ['controller' => 'features', 'action' => 'feature2']);
    Router::connect('/addons3', ['controller' => 'features', 'action' => 'feature3']);
    Router::connect('/404', ['controller' => 'features', 'action' => 'feature4']);
    
    Router::connect('/newleftsidebar', ['controller' => 'news', 'action' => 'new1']);
    Router::connect('/newrightsidebar', ['controller' => 'news', 'action' => 'new2']);
    Router::connect('/newsingle', ['controller' => 'news', 'action' => 'new3']);
    
    Router::connect('/contact1', ['controller' => 'contacts', 'action' => 'contact1']);
    Router::connect('/contact2', ['controller' => 'contacts', 'action' => 'contact2']);
/**
 * Carrega todas as rotas plugin. Consulte a documentação CakePlugin sobre como
 * personalizar o carregamento de rotas plugin.
 */
	CakePlugin::routes();

/**
 * Carrega as rotas padrão do CakePHP. Somente remova isto se você não quiser usar
 * as rotas padrão embutidas.
 */
	require CAKE . 'Config' . DS . 'routes.php';
