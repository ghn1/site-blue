<?php if (isset($this->params['admin'])): ?>
<div class="page-content">
    <div class="container-fluid">
        <div class="row">
            <div class="dahsboard-column">
                <div class="page-error-box">
                    <div class="error-code">404</div>
                    <div class="error-title">Página não encontrada</div>
                    <a href="<?php echo Router::url(['controller' => 'home', 'action' => 'index', 'admin' => true]); ?>" class="btn btn-sm">Página Inicial</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php endif;
