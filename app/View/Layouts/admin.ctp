<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $head_title ?></title>
        <?php if (isset($head_descr)) echo $this->Html->meta('description', $head_descr); ?>
        <?php foreach ($head_metas as $meta) echo $this->Html->meta($meta); ?>
        <?php if (file_exists(WWW_ROOT . 'tribo.ico')) echo $this->Html->meta('favicon.ico', '/tribo.ico', array('type' => 'icon')); ?>
        <?php if (isset($head_styles)) foreach ($head_styles as $path => $options) echo $this->Html->css($path, $options); ?>
        <?php if (isset($head_scripts)) echo $this->Html->script($head_scripts); ?>
        <?php if (isset($userdata)): ?>
        <!-- BEGIN JIVOSITE CODE {literal} -->
        <script type='text/javascript'>
        (function(){ var widget_id = 'fTOpikNR7v';var d=document;var w=window;function l(){
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = '//code.jivosite.com/script/widget/'+widget_id; var ss = document.getElementsByTagName('script')[0]; ss.parentNode.insertBefore(s, ss);}if(d.readyState=='complete'){l();}else{if(w.attachEvent){w.attachEvent('onload',l);}else{w.addEventListener('load',l,false);}}})();</script>
        <!-- {/literal} END JIVOSITE CODE -->
        <?php endif; ?>
    </head>
    <body class="with-side-menu wet-aspalt-theme">
        <?php if (isset($userdata)): ?>
        <header class="site-header">
            <div class="container-fluid">
                <?php
                echo $this->Html->link(
                    $this->Html->image('admin/logo.png', ['alt' => 'Logo', 'class' => 'hidden-md-down']) .
                    $this->Html->image('admin/logo-mob.png', ['alt' => 'Logo Mobile', 'class' => 'hidden-lg-up']),
                    '#', ['escape' => false, 'class' => 'site-logo']
                );
                ?>
                <button class="hamburger hamburger--htla">
                    <span>toggle menu</span>
                </button>
                <div class="site-header-content">
                    <div class="site-header-content-in">
                        <div class="site-header-shown">
                            <div class="dropdown dropdown-notification messages">
                                <?php
                                echo $this->Html->link(
                                    $this->Html->tag('i', '', ['class' => 'font-icon font-icon-mail']),
                                    '/webmail', ['escape' => false, 'class' => 'header-alarm', 'target' => '_blank']
                                );
                                ?>
                            </div>
                            <div class="dropdown user-menu" data-id="<?= $userdata['id'] ?>">
                                <button class="dropdown-toggle" id="dd-user-menu" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <?php
                                    if ($files = (new Folder(WWW_ROOT . 'img' . DS . 'user' . DS . $userdata['id']))->find('.*\.png'))
                                        echo $this->Html->image('user/' . $userdata['id'] . '/' . $files[0], ['alt' => $userdata['name']]);
                                    else
                                        echo $this->Html->image('user/empty/picture.png', ['alt' => 'Sem imagem']);
                                    ?>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dd-user-menu">
                                    <?php
                                    echo $this->Html->link(
                                        $this->Html->tag('span', '', ['class' => 'font-icon glyphicon glyphicon-question-sign']) . __('Ajuda'),
                                        '#', ['escape' => false, 'class' => 'dropdown-item', 'onclick' => 'jivo_api.open();']
                                    );
                                    echo $this->Html->link(
                                        $this->Html->tag('span', '', ['class' => 'font-icon font-icon-mail']) . __('Contato'),
                                        ['controller' => 'contacts'],
                                        ['escape' => false, 'class' => 'dropdown-item']
                                    );
                                    echo $this->Html->div('dropdown-divider', '');
                                    echo $this->Html->link(
                                        $this->Html->tag('span', '', ['class' => 'font-icon glyphicon glyphicon-log-out']) . __('Sair'),
                                        ['controller' => 'login', 'action' => 'logout'],
                                        ['escape' => false, 'class' => 'dropdown-item']
                                    );
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="mobile-menu-left-overlay"></div>
        <nav class="side-menu">
            <ul class="side-menu-list">
                <li class="<?php echo ($this->params['controller'] == 'users') ? 'blue active' : 'grey'; ?>">
                    <?php
                    echo $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'font-icon font-icon-users']) .
                        $this->Html->tag('span', __('Usuários'), ['class' => 'lbl']),
                        ['controller' => 'users', 'action' => 'index'],
                        ['escape' => false]
                    );
                    ?>
                </li>
            </ul>
        </nav>
        <footer class="page-footer">
            <div class="footer-copyright valign-wrapper">
                <?php echo $this->Html->div('container text-center', __('&copy; %s Tribo Propaganda. Todos os direitos reservados', date('Y'))); ?>
            </div>
        </footer>
        <?php
        endif;
        echo $this->Flash->render('admin');
        echo $this->fetch('content');
        ?>
    </body>
</html>