<?php
$head_title = 'BiziPress - BiziPress Insurance HTML5 Template';
?>
<!DOCTYPE html>
<html class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt-br" lang="pt-br" xmlns:fb="http://ogp.me/ns/fb#">
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $head_title ?></title>
        <?php if (isset($head_descr)) echo $this->Html->meta('description', $this->Text->truncate(strip_tags($head_descr), 160, ['exact' => false])); ?>
        <?php foreach ($head_metas as $meta) echo $this->Html->meta($meta); ?>
        <?php if (file_exists(WWW_ROOT . 'favicon.ico')) echo $this->Html->meta('favicon.ico', '/favicon.ico', array('type' => 'icon')); ?>
        <?php if (isset($head_styles)) foreach ($head_styles as $path => $options) echo $this->Html->css($path, $options); ?>
        <?php if (isset($head_scripts)) echo $this->Html->script($head_scripts); ?>
        <?php echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">' ?>
        <?php echo '<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">' ?>
        
    </head>
    <body>
            <div id="content">
                <?php echo $this->Flash->render(); ?>
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
    </body>
</html>
