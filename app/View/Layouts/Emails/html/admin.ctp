<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <style type="text/css">
            #templateBody,#templateContainer,#templateFooter,#templateHeader,#templatePreheader{background-color:#FFF}
            .calltoaction,.subtitle,.txt-block{text-transform:uppercase}
            #outlook a{padding:0}
            .ExternalClass,.ReadMsgBody{width:100%}
            .ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:120%}
            a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%}
            table,td{mso-table-lspace:0;mso-table-rspace:0}
            img{-ms-interpolation-mode:bicubic;border:0;height:auto;line-height:100%;outline:0;text-decoration:none}
            table{border-collapse:collapse!important}
            #bodyCell,#bodyTable,body{height:100%!important;margin:0;padding:0;width:100%!important}
            #bodyCell{padding:40px}
            #templateContainer{width:600px;border:1px solid #D9D9D9;box-shadow:0 0 20px 0 rgba(0,0,0,.1)}
            #bodyTable,body{background-color:#F7F7F7}
            .bodyContent,.footerContent,.nuHeader{background:#FFF}
            #templateHeader{border-top:1px solid #FFF;border-bottom:1px solid #FFF}
            #headerImage{height:auto;max-width:600px}
            .subtitle{color:#00aff0!important;font-style:normal;font-weight:400;line-height:140%}
            .calltoaction{font-size:14px;padding:16px;border:1px solid #00aff0;border-radius:5px;color:#00aff0;text-decoration:none;text-align:center;display:block;margin:0 auto;width:80%;font-family:Helvetica,Arial,sans-serif}
            .headerContent,.preheaderContent{text-align:left;font-family:Helvetica,Arial,sans-serif}
            .calltoaction.small{font-size:10px;padding:8px}
            .feature-icon{padding-bottom:16px}
            .sub{font-size:13px;color:#555}
            .huge-txt{font-size:28px}
            .highlight{color:#00aff0!important;font-weight:600!important}
            .block-wrapper{padding-left:35%;padding-right:35%}
            .txt-block{font-size:12px;padding:10px;border:1px solid #00aff0;border-radius:4px;margin-bottom:10px}
            .superHighlight{color:#00aff0!important;font-weight:400!important;font-size:22px}
            .greenHighlight{color:#76BF26!important;font-weight:600!important}
            .purple{color:#00aff0!important}
            .green{color:#76BF26!important}
            .blue{color:#00BCC9!important}
            .red{color:#E5615C!important}
            .warning{color:#C93648}
            .shortDivider{margin-right:auto;margin-left:auto;width:30px;border:0;border-top:1px solid #DDD}
            .sectionDivider{width:80%;border:0;border-top:1px solid #DDD}
            .spaceS{padding-bottom:8px!important}
            .spaceM{padding-bottom:16px!important}
            .spaceL{padding-bottom:24px!important}
            .spaceXL{padding-bottom:40px!important}
            .closing{margin-bottom:0!important;padding-bottom:0!important}
            .preheaderContent{color:grey;font-size:10px;line-height:125%}
            .preheaderContent a .yshortcuts,.preheaderContent a:link,.preheaderContent a:visited{color:#00aff0;font-weight:700;text-decoration:none}
            .headerContent{color:#505050;font-size:20px;font-weight:700;line-height:100%;vertical-align:middle;padding:0}
            .headerContent a .yshortcuts,.headerContent a:link,.headerContent a:visited{color:#00aff0;font-weight:400;text-decoration:none}
            .nuHeader{padding-top:40px;padding-bottom:40px}
            .nuHeader img{max-width:80px;width:80px}
            .bodyContent{color:#797979;font-family:Helvetica,Arial,sans-serif;font-size:16px;line-height:160%;padding:64px 72px 40px}
            .hidden{color:#FFF;font-size:0;height:0}
            .bodyContent td{padding-bottom:32px;text-align:center}
            .bodyContent a .yshortcuts,.bodyContent a:link,.bodyContent a:visited{color:#00aff0;font-weight:700;text-decoration:none}
            .bodyContent img{display:inline;height:auto;max-width:560px}
            .footerContent td{padding-bottom:16px;text-align:center}
            .realTable td,.sectionExtra td{text-align:left}
            .footerContent td p{color:#999;font-size:12px}
            .footerContent .contact{padding-left:12%;padding-right:12%}
            .footerContent td.shortDivider{padding-bottom:8px}
            .footerContent a .yshortcuts,.footerContent a span,.footerContent a:link,.footerContent a:visited{color:#00aff0;font-weight:700;text-decoration:none}
            .sectionHowto td{padding-bottom:40px}
            .sectionFeatures td{padding-left:8%;padding-right:8%}
            .sectionExtra{font-size:13px!important}
            .sectionExtra ul{list-style:disc;margin-left:16px}
            .inviteCode{font-size:38px;font-family:Monaco,Inconsolata,Consolas,monospace;color:#00aff0}
            .subtitle.address{text-transform:capitalize}
            .realTable{margin:0 auto}
            .realTable tr{border:1px solid #DDD}
            .realTable td{padding:14px}
            .realTable td.right{text-align:right!important}
            .realTable+.sub{margin-top:8px}
            @media only screen and (max-width:656px) {
                #templateBody,#templateContainer,#templateFooter,#templateHeader,#templatePreheader{background-color:transparent}
                .bodyContent,.footerContent,.nuHeader{background:transparent}
                #headerImage,#templateContainer{max-width:600px!important;width:100%!important}
                #headerImage,#templateContainer,body{width:100%!important}
                h1,h2,h4{line-height:120%!important}
                a,blockquote,body,li,p,table,td{-webkit-text-size-adjust:none!important}
                body{min-width:100%!important}
                #bodyCell{padding:0!important}
                #templateContainer{border:none;box-shadow:none}
                #templatePreheader{display:none!important}
                #headerImage{height:auto!important}
                h1{font-size:26px!important}
                h2{font-size:22px!important}
                h3{font-size:18px!important;line-height:140%!important}
                h4{font-size:16px!important}
                .sectionDivider{width:90%}
                .headerContent{font-size:20px!important;line-height:125%!important}
                .nuHeader{padding-top:32px;padding-bottom:32px}
                .nuHeader img{max-width:60px}
                .bodyContent{font-size:18px!important;line-height:125%!important;padding:48px 24px 32px!important}
                .footerContent{font-size:14px!important;line-height:115%!important;padding-right:24px!important;padding-left:24px!important}
                .sectionFeatures td{padding-left:0;padding-right:0}
            }
        </style>
    </head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;padding:0;height:100% ;width:100% ;background-color:#F7F7F7;">
        <center>
            <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse ;height:100% ;margin:0;padding:0;width:100% ;background-color:#F7F7F7;">
                <tr>
                    <td align="center" valign="top" id="bodyCell" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;height:100% ;margin:0;padding:40px;width:100% ;font-family:Helvetica, Arial, sans-serif;line-height:160%;">
                        <table border="0" cellpadding="0" cellspacing="0" id="templateContainer" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse ;width:600px;background-color:#FFFFFF;border:1px solid #D9D9D9;box-shadow:0px 0px 20px 0px rgba(0,0,0,0.10);">
                            <!-- Begin Content -->
                            <?php echo $this->fetch('content'); ?>
                            <!-- End Content -->
                        </table>
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>