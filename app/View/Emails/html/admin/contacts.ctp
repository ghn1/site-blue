<tr>
    <td class="bodyContent" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-family:Helvetica, Arial, sans-serif;line-height:160%;color:797979;font-size:16px;padding-top:64px;padding-bottom:40px;padding-right:72px;padding-left:72px;background:#FFFFFF;">
        <table border="0" cellpadding="0" cellspacing="0" width="100%" id="templateBody" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;border-collapse:collapse ;background-color:#FFFFFF;">
            <tr>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-family:Helvetica, Arial, sans-serif;line-height:160%;padding-bottom:0px;text-align:left;">
                    <p style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;"><strong class="highlight" style="color:#00aff0 ;font-weight:600 ;">Nome:</strong> <?php echo $name; ?></p>
                </td>
            </tr>
            <tr>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-family:Helvetica, Arial, sans-serif;line-height:160%;padding-bottom:16px;text-align:left;">
                    <p style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;"><strong class="highlight" style="color:#00aff0 ;font-weight:600 ;">Assunto:</strong> <?php echo $subject; ?></p>
                </td>
            </tr>
            <tr>
                <td width="100%" style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-family:Helvetica, Arial, sans-serif;line-height:160%;padding-bottom:16px;text-align:center;">
                    <hr class="sectionDivider" style="width:100%;border:0;border-top:1px solid #DDD;">
                </td>
            </tr>
            <tr>
                <td style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;mso-table-lspace:0pt;mso-table-rspace:0pt;font-family:Helvetica, Arial, sans-serif;line-height:160%;padding-bottom:32px;text-align:justify;">
                    <p style="-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;margin:0;"><?php echo nl2br($message); ?></p>
                </td>
            </tr>
        </table>
    </td>
</tr>
