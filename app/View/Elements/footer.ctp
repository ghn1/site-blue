<!-- Footer start-->
      <footer class="footer" id="footer">
         <div class="footer-top">
            <div class="container">
               <div class="footer-top-bg row">
                  <div class="col-lg-4 footer-box"><i class="icon icon-map-marker2"></i>
                     <div class="footer-box-content">
                        <h3>Head Office</h3>
                        <p>1010 Avenue, NY 90001, USA</p>
                     </div>
                  </div>
                  <!-- Box 1 end-->
                  <div class="col-lg-4 footer-box"><i class="icon icon-phone3"></i>
                     <div class="footer-box-content">
                        <h3>Call Us</h3>
                        <p>(+87) 847-291-4353</p>
                     </div>
                  </div>
                  <!-- Box 2 end-->
                  <div class="col-lg-4 footer-box"><i class="icon icon-envelope"></i>
                     <div class="footer-box-content">
                        <h3>Mail Us</h3>
                        <p>mail@example.com</p>
                     </div>
                  </div>
                  <!-- Box 3 end-->
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Footer top end-->
         <div class="footer-main bg-overlay">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4 col-md-12 footer-widget footer-about">
                     <div class="footer-logo">
                        <?php echo $this->Html->link(
                            $this->Html->image('/images/footer-logo.png', ['alt' => '']),
                            ['controller' => 'home', 'action' => 'index'],
                            ['escape' => false]); ?>
                     </div>
                     <p>We are a awward winning multinational company. We believe in quality and standard worldwide.</p>
                     <div class="footer-social">
                        <ul>
                           <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                           <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                           <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                           <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                           <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                     </div>
                     <!-- Footer social end-->
                  </div>
                  <!-- About us end-->
                  <div class="col-lg-4 col-md-12 footer-widget">
                     <h3 class="widget-title">Useful Links</h3>
                     <ul class="list-dash">
                        <li><?php echo $this->Html->link('About Us', array('controller' => 'pages', 'action' => 'page1')); ?></li>
                        <li><?php echo $this->Html->link('Our Services', array('controller' => 'services', 'action' => 'service1')); ?></li>
                        <li><?php echo $this->Html->link('Projects', array('controller' => 'features', 'action' => 'feature1')); ?></li>
                        <li><?php echo $this->Html->link('Our Team', array('controller' => 'pages', 'action' => 'page3')); ?></li>
                        <li><?php echo $this->Html->link('Career', array('controller' => 'pages', 'action' => 'page7')); ?></li>
                        <li><?php echo $this->Html->link('Our Blog', array('controller' => 'features', 'action' => 'feature4')); ?></li>
                        <li><?php echo $this->Html->link('Why Need Agent?', array('controller' => 'pages', 'action' => 'page4')); ?></li>
                        <li><?php echo $this->Html->link('Investments', array('controller' => 'pages', 'action' => 'page8')); ?></li>
                        <li><?php echo $this->Html->link('Consultation', array('controller' => 'pages', 'action' => 'page9')); ?></li>
                        <li><?php echo $this->Html->link('Contact Us', array('controller' => 'contacts', 'action' => 'cotnact1')); ?></li>
                     </ul>
                  </div>
                  <div class="col-lg-4 col-md-12W footer-widget">
                     <h3 class="widget-title">Subscribe</h3>
                     <div class="newsletter-introtext">Don’t miss to subscribe to our new feeds, kindly fill the form below.</div>
                     <form class="newsletter-form" id="newsletter-form" action="#" method="post">
                        <div class="form-group">
                           <input class="form-control form-control-lg" id="newsletter-form-email" type="email" name="email" placeholder="Email Address"
                              autocomplete="off">
                           <button class="btn btn-primary"><i class="fa fa-paper-plane"></i></button>
                        </div>
                     </form>
                  </div>
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Footer Main-->
         <div class="copyright">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 col-md-12">
                     <div class="copyright-info"><span>Copyright © 2018 BiziPress. All Rights Reserved.</span></div>
                  </div>
                  <div class="col-lg-6 col-md-12">
                     <div class="footer-menu">
                        <ul class="nav unstyled">
                           <li><?php echo $this->Html->link('About', array('controller' => 'pages', 'action' => 'page1')); ?></li>
                           <li><?php echo $this->Html->link('Privacy Policy', array('controller' => 'contacts', 'action' => 'cotnact1')); ?></li>
                           <li><?php echo $this->Html->link('Investors', array('controller' => 'features', 'action' => 'feature4')); ?></li>
                           <li><?php echo $this->Html->link('Legals', array('controller' => 'features', 'action' => 'feature4')); ?></li>
                           <li><?php echo $this->Html->link('Contact', array('controller' => 'contacts', 'action' => 'cotnact1')); ?></li>
                        </ul>
                     </div>
                  </div>
               </div>
               <!-- Row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Copyright end-->
      </footer>
      <!-- Footer end-->