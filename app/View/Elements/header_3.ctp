<div class="site-top-2">
         <div class="top-bar highlight" id="top-bar">
            <div class="container">
               <div class="row align-items-center">
                  <div class="col-md-9 col-sm-12">
                     <ul class="top-info">
                        <li><span class="info-icon"><i class="icon icon-phone3"></i></span>
                           <div class="info-wrapper">
                              <p class="info-title">(+9) 847-291-4353</p>
                           </div>
                        </li>
                        <li><span class="info-icon"><i class="icon icon-envelope"></i></span>
                           <div class="info-wrapper">
                              <p class="info-title">mail@example.com</p>
                           </div>
                        </li>
                        <li class="last"><span class="info-icon"><i class="icon icon-map-marker2"></i></span>
                           <div class="info-wrapper">
                              <p class="info-title">1010 Avenue, NY, USA</p>
                           </div>
                        </li>
                     </ul>
                     <!-- Ul end-->
                  </div>
                  <!-- Top info end-->
                  <div class="col-lg-3 col-md-3 col-sm-12 text-lg-right text-md-center">
                     <ul class="top-social">
                        <li>
                           <a title="Facebook" href="#">
                      <span class="social-icon"><i class="fa fa-facebook"></i></span>
                   </a>
                           <a title="Twitter" href="#">
                      <span class="social-icon"><i class="fa fa-twitter"></i></span>
                   </a>
                           <a title="Google+" href="#">
                      <span class="social-icon"><i class="fa fa-google-plus"></i></span>
                   </a>
                           <a title="Linkdin" href="#">
                      <span class="social-icon"><i class="fa fa-linkedin"></i></span>
                   </a>
                           <a title="Skype" href="#">
                      <span class="social-icon"><i class="fa fa-instagram"></i></span>
                   </a>
                        </li>
                        <!-- List End -->
                     </ul>
                     <!-- Top Social End -->
                  </div>
                  <!--Col end-->
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Top bar end-->

         <header class="header-standard header-light" id="header">
            <div class="container">
               <div class="site-nav-inner">
                  <nav class="navbar navbar-expand-lg">
                     <div class="navbar-brand navbar-header">
                        <div class="logo">
                           <?php echo $this->Html->link(
                            $this->Html->image('/images/logo-dark.png', ['alt' => '']),
                            ['controller' => 'home', 'action' => 'index'],
                            ['escape' => false]); ?>
                        </div>
                        <!-- logo end-->
                     </div>
                     <!-- Navbar brand end-->
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span></button>
                     <!-- End of Navbar toggler-->
                     <div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                           <?php echo $this->element('navigation'); ?>
                        </ul>
                        <!--Nav ul end-->
                     </div>
                  </nav>
                  <!-- Collapse end-->
                  <div class="nav-search"><span id="search"><i class="icon icon-search"></i></span></div>
                  <!-- Search end-->
                  <div class="search-block" style="display: none;">
                     <input class="form-control" type="text" placeholder="Search"><span class="search-close">×</span>
                  </div>
                  <!-- Site search end-->
               </div>
               <!-- Site nav inner end-->
            </div>
            <!-- Container end-->
         </header>
         <!-- Header end-->
      </div>