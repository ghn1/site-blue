<script type="text/javascript">
    $(function(){
        new PNotify({
            <?php foreach ($params as $param => $value): ?>
            <?php echo $param . ': "' . $value . '",' . "\n"; ?>
            <?php endforeach; ?>
            title: "<?php echo $message; ?>"
        });
    });
</script>