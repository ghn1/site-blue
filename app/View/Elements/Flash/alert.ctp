<div class="alert-simple <?php echo h($params['class']); ?>">
    <?php
    if (is_array($message)):
        foreach ($message as $msg):
    ?>
    <p><?php echo h($msg) ?></p>
    <?php
        endforeach;
    else:
    ?>
    <p><?php echo h($message) ?></p>
    <?php endif; ?>
</div>