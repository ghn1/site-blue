<div class="site-top">
         <header class="header nav-down header-solid" id="header-2">
            <div class="container">
               <div class="row">
                  <div class="logo-area clearfix logo-wrapper logo-absolute">
                     <div class="logo col-lg-3 col-md-12">
                        <?php echo $this->Html->link(
                            $this->Html->image('/images/logo.png', ['alt' => '']),
                            ['controller' => 'home', 'action' => 'index'],
                            ['escape' => false]); ?>
                     </div>
                     <!-- logo end-->
                     <div class="col-lg-9 col-md-12 pull-right">
                        <ul class="top-info top-info-wrraper">
                           <li><span class="info-icon"><i class="icon icon-phone3"></i></span>
                              <div class="info-wrapper">
                                 <p class="info-title">(+9) 847-291-4353</p>
                                 <p class="info-subtitle">Call us now</p>
                              </div>
                           </li>
                           <li><span class="info-icon"><i class="icon icon-envelope"></i></span>
                              <div class="info-wrapper">
                                 <p class="info-title">mail@example.com</p>
                                 <p class="info-subtitle">Drop us line</p>
                              </div>
                           </li>
                           <li class="last"><span class="info-icon"><i class="icon icon-map-marker2"></i></span>
                              <div class="info-wrapper">
                                 <p class="info-title">1010 Avenue, NY, USA</p>
                                 <p class="info-subtitle">Visit Our Office</p>
                              </div>
                           </li>
                           <li class="header-get-a-quote"><a class="btn btn-primary" href="#">Get A Quote</a></li>
                        </ul>
                     </div>
                     <!-- Col End-->
                  </div>
                  <!-- Logo Area End-->
               </div>
            </div>
            <!-- Container end-->
            <div class="site-nav-inner site-navigation navdown nav-transparent">
               <div class="container">
                  <nav class="navbar navbar-expand-lg justify-content-end">
                     <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"><i class="icon icon-menu"></i></span></button>
                     <!-- End of Navbar toggler-->
                     <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                           <?php echo $this->element('navigation'); ?>
                        </ul>
                        <!--Nav ul end-->
                     </div>
                  </nav>
                  <!-- Collapse end-->
                  <div class="nav-search"><span id="search"><i class="icon icon-search"></i></span></div>
                  <!-- Search end-->
                  <div class="search-block" style="display: none;">
                     <input class="form-control" type="text" placeholder="Search"><span class="search-close">×</span>
                  </div>
                  <!-- Site search end-->
               </div>
            </div>
            <!-- Site nav inner end-->
         </header>
         <!-- Header end-->
      </div>