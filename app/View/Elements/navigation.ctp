<li class="nav-item dropdown active">
                                 <a class="nav-link" href="#" data-toggle="dropdown">Home<i class="fa fa-angle-down"></i></a>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link('Home One', array('controller' => 'home', 'action' => 'index'), array('class' => 'teste')); ?></li>
                                    <li><?php echo $this->Html->link('Home Two', array('controller' => 'home', 'action' => 'home2')); ?></li>
                                    <li><?php echo $this->Html->link('Home Three', array('controller' => 'home', 'action' => 'home3')); ?></li>
                                    <li><?php echo $this->Html->link('Home Four', array('controller' => 'home', 'action' => 'home4')); ?></li>
                                    <li><?php echo $this->Html->link('Home Five', array('controller' => 'home', 'action' => 'home5')); ?></li>
                                    <li><?php echo $this->Html->link('Home Six', array('controller' => 'home', 'action' => 'home6')); ?></li>
                                    <li><?php echo $this->Html->link('Home Seven', array('controller' => 'home', 'action' => 'home7')); ?></li>
                                    <li><?php echo $this->Html->link('Home Eight', array('controller' => 'home', 'action' => 'home8')); ?></li>
                                    <li><?php echo $this->Html->link('Home Nine', array('controller' => 'home', 'action' => 'home9')); ?></li>
                                 </ul>
                              </li>
                              <!-- Home li end-->
                              <li class="nav-item dropdown"><a class="nav-link" href="#" data-toggle="dropdown">Pages<i class="fa fa-angle-down"></i></a>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link('About Us 1', array('controller' => 'pages', 'action' => 'page1')); ?></li>
                                    <li><?php echo $this->Html->link('About Us 2', array('controller' => 'pages', 'action' => 'page2')); ?></li>
                                    <li><?php echo $this->Html->link('Our Team', array('controller' => 'pages', 'action' => 'page3')); ?></li>
                                    <li><?php echo $this->Html->link('Case All', array('controller' => 'pages', 'action' => 'page4')); ?></li>
                                    <li><?php echo $this->Html->link('Case Single', array('controller' => 'pages', 'action' => 'page5')); ?></li>
                                    <li><?php echo $this->Html->link('Testmonial', array('controller' => 'pages', 'action' => 'page6')); ?></li>
                                    <li><?php echo $this->Html->link('Career', array('controller' => 'pages', 'action' => 'page7')); ?></li>
                                    <li><?php echo $this->Html->link('Pricing Table', array('controller' => 'pages', 'action' => 'page8')); ?></li>
                                    <li><?php echo $this->Html->link('FAQ', array('controller' => 'pages', 'action' => 'page9')); ?></li>
                                 </ul>
                              </li>
                              <!-- Page li end-->
                              <li class="nav-item dropdown"><a class="nav-link" href="#" data-toggle="dropdown">Our Services<i class="fa fa-angle-down"></i></a>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link('Service All', array('controller' => 'services', 'action' => 'service1')); ?></li>
                                    <li><?php echo $this->Html->link('Service Single', array('controller' => 'services', 'action' => 'service2')); ?></li>
                                 </ul>
                              </li>
                              <!-- Service li end-->
                              <li class="nav-item dropdown"><a class="nav-link" href="#" data-toggle="dropdown">Features<i class="fa fa-angle-down"></i></a>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link('Addons 1', array('controller' => 'features', 'action' => 'feature1')); ?></li>
                                    <li><?php echo $this->Html->link('Addons 2', array('controller' => 'features', 'action' => 'feature2')); ?></li>
                                    <li><?php echo $this->Html->link('Addons 3', array('controller' => 'features', 'action' => 'feature3')); ?></li>
                                    <li><?php echo $this->Html->link('404', array('controller' => 'features', 'action' => 'feature4')); ?></li>
                                    <li class="dropdown-submenu"><a class="nav-link" href="#" data-toggle="dropdown">Parent Menu</a>
                                       <ul class="dropdown-menu">
                                          <li><a href="#">Child Menu 1</a></li>
                                          <li><a href="#">Child Menu 2</a></li>
                                          <li><a href="#">Child Menu 3</a></li>
                                       </ul>
                                    </li>
                                 </ul>
                              </li>
                              <!-- Features li end-->
                              <li class="nav-item dropdown">
                                 <a class="nav-link" href="#" data-toggle="dropdown">
                              News
                              <i class="fa fa-angle-down"></i>
                           </a>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link('News Left Sidebar', array('controller' => 'news', 'action' => 'new1')); ?></li>
                                    <li><?php echo $this->Html->link('News Right Sidebar', array('controller' => 'news', 'action' => 'new2')); ?></li>
                                    <li><?php echo $this->Html->link('News Single', array('controller' => 'news', 'action' => 'new3')); ?></li>
                                 </ul>
                              </li>
                              <!-- News li end-->
                              <li class="nav-item dropdown">
                                 <a class="nav-link" href="#" data-toggle="dropdown">
                              Contact
                              <i class="fa fa-angle-down"></i>
                           </a>
                                 <ul class="dropdown-menu" role="menu">
                                    <li><?php echo $this->Html->link('Contact 1', array('controller' => 'contacts', 'action' => 'contact1')); ?></li>
                                    <li><?php echo $this->Html->link('Contact 2', array('controller' => 'contacts', 'action' => 'contact2')); ?></li>
                                 </ul>
                              </li>