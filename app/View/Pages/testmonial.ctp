<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner1.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Testimonial</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'feature4']) ?>
                        </li>
                        <li>Testimonial</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container no-padding" id="main-container">
         <div id="ts-testimonial-static" class="ts-testimonial-static">
            <div class="container">
               <div class="row text-center">
                  <div class="col-md-12">
                     <h2 class="section-title"><span>Happy peoples</span>Our Clients</h2>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <div class="quote-item quote-classic mrb-40"><span class="quote-text faq-quote-text">The Forexnic loan has been  the most attractive loan products on the market, helping numerous</span>
                        <div class="quote-item-footer quote-footer-classic">
                            <?php echo $this->Html->image('/images/clients/testimonial1.png',
                                    ['class' => 'testimonial-thumb'],
                                    ['alt' => 'testimonial']); ?>
                           <div class="quote-item-info">
                              <p class="quote-author">Diane Santos</p><span class="quote-subtext">HR Denim</span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="quote-item quote-classic mrb-40"><span class="quote-text faq-quote-text">The Forexnic loan has been  the most attractive loan products on the market, helping numerous</span>
                        <div class="quote-item-footer quote-footer-classic">
                           <?php echo $this->Html->image('/images/clients/testimonial2.png',
                                    ['class' => 'testimonial-thumb'],
                                    ['alt' => 'testimonial']); ?>
                           <div class="quote-item-info">
                              <p class="quote-author">Carol Warren</p><span class="quote-subtext">Director Jaguar</span>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="quote-item quote-classic mrb-40"><span class="quote-text faq-quote-text">The Forexnic loan has been  the most attractive loan products on the market, helping numerous</span>
                        <div class="quote-item-footer quote-footer-classic">
                           <?php echo $this->Html->image('/images/clients/testimonial2.png',
                                    ['class' => 'testimonial-thumb'],
                                    ['alt' => 'testimonial']); ?>
                           <div class="quote-item-info">
                              <p class="quote-author">Jhon Cameron</p><span class="quote-subtext">Manager Walton</span>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div id="ts-testimonial-slide" class="ts-testimonial-slide solid-bg">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="testimonial-slide owl-carousel owl-theme">
                        <div class="row quote-item-area">
                           <div class="col-md-5">
                              <div class="quote-thumb">
                                  <?php echo $this->Html->image('/images/clients/testimonial1.png',
                                    ['class' => 'quote-thumb-img'],
                                    ['alt' => 'Julia Mayer']); ?>
                              </div>
                              <!-- Quote thumb end -->
                           </div>
                           <!-- Col end -->

                           <div class="col-md-7">
                              <div class="quote-item-content">
                                 <h3 class="quote-name">Julia Mayer </h3>
                                 <span class="quote-name-desg">
                                          HR Manager</span>
                                 <p class="quote-message">
                                    The Forexnic loan has been the most attractive loan products on the market, would not be able to obtain conventionally and
                                    at extremely. </p>
                              </div>
                              <!-- Quote content end -->
                           </div>
                           <!-- Col end -->
                        </div>
                        <div class="row quote-item-area">
                           <div class="col-md-5">
                              <div class="quote-thumb">
                                 <?php echo $this->Html->image('/images/clients/testimonial2.png',
                                    ['class' => 'quote-thumb-img'],
                                    ['alt' => 'Julia Mayer']); ?>

                              </div>
                              <!-- Quote thumb end -->
                           </div>
                           <!-- Col end -->

                           <div class="col-md-7">
                              <div class="quote-item-content">
                                 <h3 class="quote-name">Julia Mayer </h3>
                                 <span class="quote-name-desg">
                                          HR Manager</span>
                                 <p class="quote-message">
                                    The Forexnic loan has been the most attractive loan products on the market, would not be able to obtain conventionally and
                                    at extremely. </p>
                              </div>
                              <!-- Quote content end -->
                           </div>
                           <!-- Col end -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>


      <div class="gap-40"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/testimonial.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:26 GMT -->
</html>