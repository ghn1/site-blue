<body>

   <div class="body-inner">

      <?php echo $this->element('header_7'); ?>

      <div id="box-slide" class="owl-carousel owl-theme page-slider mrt-190">
         <div class="item" style="background-image:url(images/slider/bg4.jpg)">
            <div class="container">
               <div class="box-slider-content">
                  <div class="box-slider-text animated">
                     <h2 class="box-slide-title">27 Years Young</h2>
                     <h3 class="box-slide-sub-title">To Change Your World</h3>
                     <p class="box-slide-description">You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent
                        like.
                     </p>
                     <p>
                        <?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                     </p>
                  </div>
               </div>
            </div>

         </div>
         <!-- Item 1 end -->

         <div class="item" style="background-image:url(images/slider/bg5.jpg)">
            <div class="container">
               <div class="box-slider-content">
                  <div class="box-slider-text animated">
                     <h2 class="box-slide-title">We are the Leader</h2>
                     <h3 class="box-slide-sub-title">In Finance Industry</h3>
                     <p class="box-slide-description">You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent
                        like.
                     </p>
                     <p>
                        <?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <!-- Item 2 end -->

         <div class="item" style="background-image:url(images/slider/bg6.jpg)">
            <div class="container">
               <div class="box-slider-content">
                  <div class="box-slider-text animated">
                     <h2 class="box-slide-title">To Reach more People</h2>
                     <h3 class="box-slide-sub-title">We Care our Customers</h3>
                     <p class="box-slide-description">You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent
                        like.
                     </p>
                     <p>
                        <?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <!-- Item 3 end -->
      </div>
      <!-- Box owl carousel end-->

      <section id="ts-features-light" class="ts-features-light">
         <div class="container">
            <div class="row">
               <div class="col-lg-4 text-center">
                  <div class="ts-feature-box">
                     <div class="ts-feature-info">
                        <i class="icon icon-consut2"></i>
                        <h3 class="ts-feature-title">Serviços</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks</p>
                     </div>
                  </div>
                  <!-- feature box-1 end-->
               </div>
               <!-- col-1 end-->
               <div class="col-lg-4 text-center border-left">
                  <div class="ts-feature-box">
                     <div class="ts-feature-info">
                        <i class="icon icon-chart2"></i>
                        <h3 class="ts-feature-title">Serviços</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks</p>
                     </div>
                  </div>
                  <!-- feature box-2 end-->
               </div>
               <!-- col-2 end-->
               <div class="col-lg-4 text-center border-left">
                  <div class="ts-feature-box">
                     <div class="ts-feature-info">
                        <i class="icon icon-clock3"></i>
                        <h3 class="ts-feature-title">Serviços</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks</p>
                     </div>
                  </div>
                  <!-- feature box-2 end-->
               </div>
               <!-- col-3 end-->
            </div>
         </div>
      </section>

      <section class="ts-services solid-bg" id="ts-services">
         <div class="container">
            <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title"><span>Últimas</span>Notícias</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                         <?php echo $this->Html->image('/images/services/service1.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-pie-chart2"></i>
                        </span>
                        <h3 class="service-title">Financial Planning</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service1 end-->
               </div>
               <!-- Col 1 end-->
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-tax"></i>
                        </span>
                        <h3 class="service-title">Tax Planning</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service2 end-->
               </div>
               <!-- Col 2 end-->
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service3.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-savings"></i>
                        </span>
                        <h3 class="service-title">Saving Strategy</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service3 end-->
               </div>
               <!-- Col 3 end-->
            </div>
            <!-- Content Row 2 end-->
         </div>
         <!-- Container end-->
      </section>

      <section id="ts-team" class="ts-team">
         <div class="container">
            <div class="row text-center">
               <div class="col-lg-12">
                  <h2 class="section-title"><span>Comunicação</span>Fique por dentro</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                         <?php echo $this->Html->image('/images/team/team1.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Calendário 2018</h3>
                        <p class="team-designation">Acessar link.</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 1 end-->
                  </div>
                  <!-- Team classic 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Jornal da ACIJ</h3>
                        <p class="team-designation">Edição maio de 2018</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 2 end-->
                  </div>
                  <!-- Team classic 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team3.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 3 end-->
                  </div>
                  <!-- Team classic 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
      </section>
      <!-- Section Team end-->

      <section class="clients-area" id="clients-area">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 owl-carousel owl-theme text-center partners" id="partners-carousel">
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client1.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client12.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client3.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client4.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client5.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client6.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client7.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
               </div>
               <!-- Owl carousel end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Partners end-->

      <div class="map" id="map"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- For Chart-->
      <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
      <script type="text/javascript" src="js/morris.js"></script>
      <script type="text/javascript">
         Morris.Area({
            element: 'graph',
            behaveLikeLine: true,
            lineColors: ['#e36217', '#2154cf', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
            data: [{
                  x: '2012',
                  y: 9,
                  z: 7
               },
               {
                  x: '2013',
                  y: 6,
                  z: 8
               },
               {
                  x: '2014',
                  y: 6,
                  z: 5
               },
               {
                  x: '2015',
                  y: 8,
                  z: 10
               }
            ],
            xkey: 'x',
            ykeys: ['y', 'z'],
            labels: ['Y', 'Z']
         });
      </script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/index-7.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:42:50 GMT -->
</html>