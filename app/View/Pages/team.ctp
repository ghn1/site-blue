<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner1.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Who we are</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'feature4']) ?>
                        </li>
                        <li>team</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">

         <div class="container">
            <div class="row text-center">
               <div class="col-lg-12">
                  <h2 class="section-title"><span>Our People</span>Best Team</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                         <?php echo $this->Html->image('/images/team/team1.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 1 end-->
                  </div>
                  <!-- Team classic 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 2 end-->
                  </div>
                  <!-- Team classic 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 3 end-->
                  </div>
                  <!-- Team classic 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
      </section>

      <section class="ts-team solid-bg">
         <div class="container">
            <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title"><span>Our People</span>Best Team</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-3 col-md-6">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team1.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-6">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Patrick Ryan</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-6">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Craig Robinson</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-6">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team4.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Andrew Robinson</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>

      <section id="ts-team" class="ts-team">
         <div class="container">
            <div class="row text-center">
               <div class="col-lg-12">
                  <h2 class="section-title"><span>Our People</span>Best Team</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team1.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info team-list-border">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 1 end-->
                  </div>
                  <!-- Team classic 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info team-list-border">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic">
                           <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 2 end-->
                  </div>
                  <!-- Team classic 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info team-list-border">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 3 end-->
                  </div>
                  <!-- Team classic 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
      </section>

      <section class="call-to-action bg-pattern-3 solid-bg pab-120">
         <div class="container">
            <div class="row text-center">
               <div class="col-lg-12">
                  <h3 class="call-to-action-title">We Offer Financial Strategies & Superior Services</h3>
                  <p>
                     Our mission is to provide quality guidance, build relationships of
                     <br> trust, and develop innovative solutions
                  </p><a class="btn btn-primary" href="#">Get Free Quote</a>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- container end-->
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/team.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:07 GMT -->
</html>