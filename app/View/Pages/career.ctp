<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner1.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Career</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>career</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">

         <div id="ts-gallery-layout" class="ts-gallery-layout">
            <div class="container">
               <div class="row text-left">
                  <div class="col-lg-12">
                     <h2 class="section-title border-title-left">Life at BiziPress
                        <span class="section-title-tagline title-light">Our Office life</span>
                     </h2>
                  </div>
               </div>
               <!-- Row End-->
               <div class="row">
                  <div class="col-lg-12">
                     <div class="carrer-gallery-layout">
                        <div class="career-layout career-layout-double career-bg-1"></div>
                        <div class="career-layout career-bg-2"></div>
                        <div class="career-layout career-layout-small career-bg-3"></div>
                        <div class="career-layout career-layout-double career-bg-4"></div>
                        <div class="career-layout career-layout-small career-bg-5"></div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Container End -->
         </div>
         <!-- Gallery End -->
      </section>

      <section id="career-body" class="career-body">
         <div class="container">
            <div class="row">
               <div class="col-lg-8">
                  <div class="career-body-inner">
                     <div class="job-post-box">
                        <div class="heading">
                           <h2 class=" content-title border-none"><span class="shortheading">Posted on : February 17, 2018</span> Business Analyst </h2>
                        </div>
                        <div class="ts-text-block mrt-60">
                           <p>Show candidates personalized job recommendations, let them discover employees like them.You’ll
                              gain new skills and build on the strengths you bring to the firm. Business Analysts receive
                              exceptional training as well as frequent coaching and mentoring from colleagues on their teams.
                           </p>
                        </div>
                        <div class="job-box">
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-map-marker"></i></div>
                              <div class="desc"> Location: <strong>United States</strong></div>
                           </div>
                           <!-- Job info end -->
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-dollar"></i></div>
                              <div class="desc"> Sallary: <strong>$80K (Annual)</strong></div>
                           </div>
                           <!-- Job info end -->
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-graduation-cap"></i></div>
                              <div class="desc"> Education: <strong>MCom (±6 years)</strong></div>
                           </div>
                           <!-- Job info end -->
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-clock-o"></i></div>
                              <div class="desc"> Schedule: <strong>Full Time</strong></div>
                           </div>
                           <!-- Job info end -->
                        </div>
                        <!-- Job Box End -->
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="job-skill">
                                 <div class="skill-title">
                                    <h3>Responsibilities</h3>
                                 </div>
                                 <ul class="ts-list">
                                    <li>Assisting senior consultants in projects</li>
                                    <li>Share best practices and knowledge.</li>
                                    <li>Collaborate with technology, information security, and business partners</li>
                                    <li>Find and address performance issues</li>
                                    <li>Maintains records to allow for historical trending analysis</li>
                                 </ul>
                                 <!-- List End -->
                              </div>
                              <!-- Skill End -->
                           </div>
                           <!-- Col end -->
                           <div class="col-lg-6">
                              <div class="job-skill">
                                 <div class="skill-title">
                                    <h3>Skills/Experience</h3>
                                 </div>
                                 <ul class="ts-list">
                                    <li>Assisting senior consultants in projects</li>
                                    <li>Share best practices and knowledge.</li>
                                    <li>Collaborate with technology, information security, and business partners</li>
                                    <li>Find and address performance issues</li>
                                    <li>Maintains records to allow for historical trending analysis</li>
                                 </ul>
                                 <!-- List End -->
                              </div>
                              <!-- Skill End -->
                           </div>
                           <!-- Col end -->
                        </div>
                        <!-- Row End -->
                     </div>
                     <!-- Job Post Box End -->
                     <div class="job-post-box mrt-40">
                        <div class="heading">
                           <h2 class=" content-title border-none"><span class="shortheading">Posted on : February 17, 2018</span> Marketing Manager</h2>
                        </div>
                        <div class="ts-text-block mrt-40">
                           <p>Show candidates personalized job recommendations, let them discover employees like them.You’ll
                              gain new skills and build on the strengths you bring to the firm. Business Analysts receive
                              exceptional training as well as frequent coaching and mentoring from colleagues on their teams.
                           </p>
                        </div>
                        <div class="job-box">
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-map-marker"></i></div>
                              <div class="desc"> Location: <strong>United States</strong></div>
                           </div>
                           <!-- Job info end -->
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-dollar"></i></div>
                              <div class="desc"> Sallary: <strong>$80K (Annual)</strong></div>
                           </div>
                           <!-- Job info end -->
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-graduation-cap"></i></div>
                              <div class="desc"> Education: <strong>MCom (±6 years)</strong></div>
                           </div>
                           <!-- Job info end -->
                           <div class="job-info">
                              <div class="icon"> <i class="fa fa-clock-o"></i></div>
                              <div class="desc"> Schedule: <strong>Full Time</strong></div>
                           </div>
                           <!-- Job info end -->
                        </div>
                        <!-- Job Box End -->
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="job-skill">
                                 <div class="skill-title">
                                    <h3>Responsibilities</h3>
                                 </div>
                                 <ul class="ts-list">
                                    <li>Assisting senior consultants in projects</li>
                                    <li>Share best practices and knowledge.</li>
                                    <li>Collaborate with technology, information security, and business partners</li>
                                    <li>Find and address performance issues</li>
                                    <li>Maintains records to allow for historical trending analysis</li>
                                 </ul>
                                 <!-- List End -->
                              </div>
                              <!-- Skill End -->
                           </div>
                           <!-- Col end -->
                           <div class="col-lg-6">
                              <div class="job-skill">
                                 <div class="skill-title">
                                    <h3>Skills/Experience</h3>
                                 </div>
                                 <ul class="ts-list">
                                    <li>Assisting senior consultants in projects</li>
                                    <li>Share best practices and knowledge.</li>
                                    <li>Collaborate with technology, information security, and business partners</li>
                                    <li>Find and address performance issues</li>
                                    <li>Maintains records to allow for historical trending analysis</li>
                                 </ul>
                                 <!-- List End -->
                              </div>
                              <!-- Skill End -->
                           </div>
                           <!-- Col end -->
                        </div>
                        <!-- Row End -->
                     </div>
                     <!-- Job Post Box End -->
                  </div>
                  <!-- Career Body Inner End -->
               </div>
               <!-- Col End -->
               <div class="col-lg-4">
                  <div class="sidebar sidebar-right">
                     <div class="widget bo-padding no-border box-primary">
                        <div class="howto">
                           <h3>How to Apply</h3>
                           <p>
                              There is no preparation required for this step (we promise), so you can create your account as early as you’d like. All you’ll
                              need is some
                           </p>
                           <?php echo $this->Html->link(
                                   'Read More',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                   ['class' => 'btn btn-white']); ?>
                        </div>
                     </div>
                     <div class="widget no-padding no-border">
                        <h3 class="download-btn">
                            <?php echo $this->Html->link(
                                    $this->Html->tag('span', 'Brand Brochure') . 
                                    $this->Html->tag('i', '', ['class' => 'fa fa-download float-right']),
                                    ['controller' => 'features', 'action' => 'feature4'],
                                    ['escape' => false]); ?>
                            <!--<a href="#"><span>Brand Brochure</span><i class="fa fa-download float-right"></i></a>--></h3>
                     </div>
                     <div class="widget no-padding testimonial-static">
                        <div class="quote-item quote-classic"><span class="quote-text faq-quote-text">The Forexnic loan has been  the most attractive loan products on the market, helping numerous</span>
                           <div class="quote-item-footer quote-footer-classic">
                               <?php $this->Html->image('/images/clients/testmonial1.png',
                                       ['alt' => 'testmonial'],
                                       ['class' => 'testmonial-thumb']); ?>
                              <div class="quote-item-info">
                                 <p class="quote-author">Jhon Cameron</p><span class="quote-subtext">Manager Walton</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                  </div>
               </div>
               <!-- Col End -->
            </div>
            <!-- Row End -->
         </div>
         <!-- Container End -->
      </section>
      <!-- Career Body End -->

      <div class="gap-40"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Packery-->
      <script type="text/javascript" src="js/packery.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/career.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:27 GMT -->
</html>