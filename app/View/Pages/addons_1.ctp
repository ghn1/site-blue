<body>
   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner5.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Addons</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>Addons</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <h3 class="addons-title">Accordion</h3>
                  <div id="accordion">
                     <div class="card">
                        <div class="card-header" id="headingOne">
                           <h5 class="mb-0">
                              <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                 <p>What do I get when my account is paid off?</p>
                              </button>
                           </h5>
                        </div>
                        <div class="collapse show" id="collapseOne" aria-labelledby="headingOne" data-parent="#accordion">
                           <div class="card-body">
                              <p>A business strategy is the means by which it sets out to achieve its desired ends. You have
                                 ideas, goals, and dreams..</p>
                           </div>
                        </div>
                     </div>
                     <div class="card">
                        <div class="card-header" id="headingTwo">
                           <h5 class="mb-0">
                              <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                 <p>I have a small credit balance on my account?</p>
                              </button>
                           </h5>
                        </div>
                        <div class="collapse" id="collapseTwo" aria-labelledby="headingTwo" data-parent="#accordion">
                           <div class="card-body">
                              <p>A business strategy is the means by which it sets out to achieve its desired ends. You have
                                 ideas, goals, and dreams.</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- col end-->
               <div class="col-lg-6">
                  <h3 class="addons-title">Graph</h3>
                  <div id="graph"></div>
               </div>
               <!-- col end-->
            </div>
         </div>
      </section>

      <section id="skill-addons" class="skill-addons no-padding">
         <div class="ts-skills-area solid-bg" id="ts-skills-area">
            <div class="container-fluid no-padding">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="skills-image">
                        <?php echo $this->Html->image('/images/pages/about_1.png',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
                     </div>
                     <!-- Skills image end-->
                  </div>
                  <!-- Col end-->
                  <div class="col-lg-6 ts-padding">
                     <h3 class="addons-title">Progress Bar</h3>
                     <p class="intro-desc">A business strategy is the means by which it sets out to achieve its desired ends. You have ideas, goals,
                        and dreams. We have a culturally diverse, forward thinking team looking for talent like you and make
                        your dream come true.</p>
                     <div class="gap-20"></div>
                     <div class="ts-progress-bar">
                        <h3>Planning</h3>
                        <div class="progress">
                           <div class="progress-bar" style="width:75%; background:blue;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                              <div class="progress-value">75%</div>
                           </div>
                        </div>
                     </div>
                     <!-- End ts-progress bar 1-->
                     <div class="ts-progress-bar">
                        <h3>Consulting</h3>
                        <div class="progress">
                           <div class="progress-bar" style="width:90%; background:blue;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                              <div class="progress-value">90%</div>
                           </div>
                        </div>
                     </div>
                     <!-- End ts-progress bar 2-->
                     <div class="ts-progress-bar">
                        <h3>Marketing</h3>
                        <div class="progress">
                           <div class="progress-bar" style="width:80%; background:blue;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                              <div class="progress-value">80%</div>
                           </div>
                        </div>
                     </div>
                     <!-- End ts-progress bar 3-->
                  </div>
                  <!-- Col end-->
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container 2 end-->
         </div>
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- For Chart-->
      <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
      <script type="text/javascript" src="js/morris.js"></script>
      <script type="text/javascript">
         Morris.Area({
            element: 'graph',
            behaveLikeLine: true,
            lineColors: ['#e36217', '#2154cf', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
            data: [{
                  x: '2012',
                  y: 9,
                  z: 7
               },
               {
                  x: '2013',
                  y: 6,
                  z: 8
               },
               {
                  x: '2014',
                  y: 6,
                  z: 5
               },
               {
                  x: '2015',
                  y: 8,
                  z: 10
               }
            ],
            xkey: 'x',
            ykeys: ['y', 'z'],
            labels: ['Y', 'Z']
         });
      </script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/addons-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:30 GMT -->
</html>