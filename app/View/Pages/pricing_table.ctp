<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner1.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Our Pricing</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?></li>
                        <li>Pricing Table</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">
         <div class="pricing-box-default">
            <div class="container">
               <div class="row text-center">
                  <div class="col-md-12">
                     <h2 class="section-title"><span>Buy Plan</span>Our Pricing</h2>
                  </div>
                  <!-- Col End -->
               </div>
               <!-- Row End -->
               <div class="row mrt-40">
                  <div class="col-lg-4">
                     <div class="plan text-center">
                        <div class="price-icon-wrapper">
                           <i class=""></i>
                        </div>
                        <h2 class="plan-name">Starter Plan</h2>
                        <h3 class="plan-price">
                           <sup class="currency">$</sup>
                           <strong>49.99</strong>
                           <sub>/mo</sub>
                        </h3>
                        <ul class="list-unstyled">
                           <li>Invoices/Estimates</li>
                           <li>Online Payments</li>
                           <li>Projects and Time Sheet</li>
                           <li>Recurring Transations</li>
                           <li>Client Portal</li>
                        </ul>
                        <div class="text-center ">
                            <?php echo $this->Html->link(
                                    'Buy Now',
                                    ['controller' => 'features', 'action' => 'feature4'],
                                    ['class' => 'btn btn-primary', 'target' => '_self']); ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="plan featured text-center">
                        <div class="price-icon-wrapper">
                           <i class=""></i>
                        </div>
                        <h2 class="plan-name">Starter Plan</h2>
                        <h3 class="plan-price">
                           <sup class="currency">$</sup>
                           <strong>69.99</strong>
                           <sub>/mo</sub>
                        </h3>
                        <ul class="list-unstyled">
                           <li>Invoices/Estimates</li>
                           <li>Online Payments</li>
                           <li>Projects and Time Sheet</li>
                           <li>Recurring Transations</li>
                           <li>Client Portal</li>
                        </ul>
                        <div class="text-center ">
                           <?php echo $this->Html->link(
                                    'Buy Now',
                                    ['controller' => 'features', 'action' => 'feature4'],
                                    ['class' => 'btn btn-primary', 'target' => '_self']); ?>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-4">
                     <div class="plan text-center plan-last">
                        <div class="price-icon-wrapper">
                           <i class=""></i>
                        </div>
                        <h2 class="plan-name">Starter Plan</h2>
                        <h3 class="plan-price">
                           <sup class="currency">$</sup>
                           <strong>99.99</strong>
                           <sub>/mo</sub>
                        </h3>
                        <ul class="list-unstyled">
                           <li>Invoices/Estimates</li>
                           <li>Online Payments</li>
                           <li>Projects and Time Sheet</li>
                           <li>Recurring Transations</li>
                           <li>Client Portal</li>
                        </ul>
                        <div class="text-center ">
                           <?php echo $this->Html->link(
                                    'Buy Now',
                                    ['controller' => 'features', 'action' => 'feature4'],
                                    ['class' => 'btn btn-primary', 'target' => '_self']); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <!-- Container End -->
         </div>

         <div class="ts-price-box solid-bg">
            <div class="container">
               <div class="row text-center">
                  <div class="col-md-12">
                     <h2 class="section-title"><span>Buy Plan</span>Our Pricing</h2>
                  </div>
                  <!-- Col End -->
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="pricing-boxed">
                        <div class="single-price-box">
                           <div class="pricing-plan">
                              <div class="pricing-header border-left">
                                 <h2 class="plan-name">Starter Plan</h2>
                                 <h3 class="plan-price">
                                    <sup class="currency">$</sup>
                                    <strong>49.99</strong>
                                    <sub>/mo</sub>
                                 </h3>
                              </div>
                              <ul class="list-unstyled">
                                 <li>Invoices/Estimates</li>
                                 <li>Online Payments</li>
                                 <li>Projects and Time Sheet</li>
                                 <li>Recurring Transations</li>
                                 <li>Client Portal</li>
                              </ul>
                              <div>
                                <?php echo $this->Html->link(
                                    'Buy Now',
                                    ['controller' => 'features', 'action' => 'feature4'],
                                    ['class' => 'btn btn-primary'],
                                         ['target' => '_self']); ?>
                              </div>
                           </div>
                        </div>
                        <div class="single-price-box featured">
                           <div class="pricing-plan">
                              <div class="pricing-header">
                                 <h2 class="plan-name">
                                    Basic Plan
                                 </h2>
                                 <h3 class="plan-price">
                                    <sup class="currency">$</sup>
                                    <strong>69.99</strong>
                                    <sub>/mo</sub>
                                 </h3>
                              </div>
                              <ul class="list-unstyled">
                                 <li>Invoices/Estimates</li>
                                 <li>Online Payments</li>
                                 <li>Projects and Time Sheet</li>
                                 <li>Recurring Transations</li>
                                 <li>Client Portal</li>
                              </ul>
                              <div>
                                 <?php echo $this->Html->link(
                                    'Buy Now',
                                    ['controller' => 'features', 'action' => 'feature4'],
                                    ['class' => 'btn btn-primary'],
                                         ['target' => '_self']); ?>
                              </div>
                           </div>
                        </div>
                        <div class="single-price-box">
                           <div class="pricing-plan">
                              <div class="pricing-header border-right">
                                 <h2 class="plan-name">Advanced Plan</h2>
                                 <h3 class="plan-price">
                                    <sup class="currency">$</sup>
                                    <strong>99.99</strong>
                                    <sub>/mo</sub>
                                 </h3>
                              </div>
                              <ul class="list-unstyled">
                                 <li>Invoices/Estimates</li>
                                 <li>Online Payments</li>
                                 <li>Projects and Time Sheet</li>
                                 <li>Recurring Transations</li>
                                 <li>Client Portal</li>
                              </ul>
                              <div>
                                 <?php echo $this->Html->link(
                                    'Buy Now',
                                    ['controller' => 'features', 'action' => 'feature4'],
                                    ['class' => 'btn btn-primary'],
                                         ['target' => '_self']); ?>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- main Container End -->

      <div class="gap-40"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Packery-->
      <script type="text/javascript" src="js/packery.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/pricing-table.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:27 GMT -->
</html>