<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner2.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">News</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>news</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">
         <div class="container">
            <div class="row">
               <div class="col-lg-8">
                  <div class="post">
                     <div class="post-media post-image">
                        <?php echo $this->Html->image('/images/news/news1.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="post-body clearfix">
                        <div class="post-meta-left pull-left text-center"><span class="post-meta-date"><span class="day">27</span>June</span><span class="post-author">
                            <?php echo $this->Html->image('/images/news/avator1.png',
                                        ['alt' => ''],
                                        ['class' => 'avatar']); ?>
                                <?php echo $this->Html->link(
                                        'By John Doe',
                                        ['controller' => 'features', 'action' => 'feature4']); ?>
                            </span>
                           <span class="post-comment"><i class="icon icon-comment"></i><?php echo $this->Html->link(
                                   '',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                   ['class' => 'comments-link']); ?>07</span>
                        </div>
                        <!-- Post meta left-->
                        <div class="post-content-right">
                           <div class="entry-header">
                              <div class="post-meta"><span class="post-cat"><i class="icon icon-folder"></i><?php echo $this->Html->link(
                                   'Financial Planning',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                                 <span class="post-tag"><i class="icon icon-tag"></i><?php echo $this->Html->link(
                                   'Insurance, Business',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                              </div>
                              <h2 class="entry-title"><?php echo $this->Html->link(
                                   'Uber is selling off its auto-leasing business',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h2>
                           </div>
                           <!-- header end-->
                           <div class="entry-content">
                              <p>Financial engagements typically multifaceted solving specific digital marketing and challenges
                                 while building ongoing client capabilities. In addition to defining newer roles but helping
                                 develop...
                              </p>
                           </div>
                           <div class="post-footer text-right"><?php echo $this->Html->link(
                                   'Continue Reading',
                                   ['controller' => 'news', 'action' => 'new3'],
                                   ['class' => 'bt btn-primary']); ?>
                           </div>
                        </div>
                        <!-- Post content right-->
                     </div>
                     <!-- post-body end-->
                  </div>
                  <!-- 1st post end-->
                  <div class="post">
                     <div class="post-quote-wrapper">
                        <div class="post-quote-content text-center">
                           <div class="entry-header">
                              <h2 class="entry-title">
                                  <?php echo $this->Html->link(
                                   'Bitcoin is the gag gift you should buy this holiday season',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h2>
                           </div>
                           <div class="meta-author"><?php echo $this->Html->link(
                                   'Jhony Rick',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                           </div>
                           <div class="post-meta"><span class="post-cat"><i class="icon icon-folder"></i><?php echo $this->Html->link(
                                   'Financial Planning',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                               </span>
                              <span class="post-date"><i class="icon icon-calendar2"></i><?php echo $this->Html->link(
                                   '27 June, 2018',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                              <span class="post-tag"><i class="icon icon-tag"></i><?php echo $this->Html->link(
                                   'Insurance, Business',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                           </div>
                           <!-- Post meta end-->
                        </div>
                     </div>
                  </div>
                  <!-- 2nd post end-->
                  <div class="post">
                     <div class="post-media post-video">
                        <?php echo $this->Html->image('/images/news/news2.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                        <a class="popup" href="https://www.youtube.com/embed/XhveHKJWnOQ?autoplay=1&amp;loop=1">
                           <div class="video-icon"><i class="icon icon-play"></i></div>
                        </a>
                     </div>
                     <div class="post-body clearfix">
                        <div class="post-meta-left pull-left text-center"><span class="post-meta-date"><span class="day">27</span>June</span><span class="post-author">
                            <?php echo $this->Html->image('/images/news/avator1.png',
                                        ['alt' => ''],
                                        ['class' => 'avatar']); ?>
                                <?php echo $this->Html->link(
                                        'By John Doe',
                                        ['controller' => 'features', 'action' => 'feature4']); ?>
                            </span>
                           <span class="post-comment"><i class="icon icon-comment"></i>
                               <?php echo $this->Html->link(
                                   '',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'comments-link']); ?>07</span>
                        </div>
                        <!-- Post meta left-->
                        <div class="post-content-right">
                           <div class="entry-header">
                              <div class="post-meta"><span class="post-cat"><i class="icon icon-folder"></i><?php echo $this->Html->link(
                                   'Financial Planning',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                                 <span class="post-tag"><i class="icon icon-tag"></i><?php echo $this->Html->link(
                                   'Insurance, Business',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                              </div>
                              <h2 class="entry-title">
                                  <?php echo $this->Html->link(
                                   'Apple reveals its new Melbourne flagship',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h2>
                           </div>
                           <!-- header end-->
                           <div class="entry-content">
                              <p>Financial engagements typically multifaceted solving specific digital marketing and challenges
                                 while building ongoing client capabilities. In addition to defining newer roles but helping
                                 develop...
                              </p>
                           </div>
                           <div class="post-footer text-right">
                               <?php echo $this->Html->link(
                                   'Continue Reading',
                                   ['controller' => 'news', 'action' => 'new3'],
                                       ['class' => 'btn btn-primary']); ?>
                           </div>
                        </div>
                        <!-- Post content right-->
                     </div>
                     <!-- post-body end-->
                  </div>
                  <!-- 3rd post end-->
                  <div class="post">
                     <div class="post-media post-image">
                        <?php echo $this->Html->image('/images/news/news1.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="post-body clearfix">
                        <div class="post-meta-left pull-left text-center"><span class="post-meta-date"><span class="day">27</span>June</span><span class="post-author">
                            ]<?php echo $this->Html->image('/images/news/avator1.png',
                                        ['alt' => ''],
                                        ['class' => 'avatar']); ?>
                                <?php echo $this->Html->link(
                                        'By John Doe',
                                        ['controller' => 'features', 'action' => 'feature4']); ?>
                            </span>
                           <span class="post-comment"><i class="icon icon-comment"></i>
                               <?php echo $this->Html->link(
                                   '',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'comments-link']); ?>07</span>
                        </div>
                        <!-- Post meta left-->
                        <div class="post-content-right">
                           <div class="entry-header">
                              <div class="post-meta"><span class="post-cat"><i class="icon icon-folder"></i><?php echo $this->Html->link(
                                   'Financial Planning',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                                 <span class="post-tag"><i class="icon icon-tag"></i><?php echo $this->Html->link(
                                   'Insurance, Business',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                              </div>
                              <h2 class="entry-title">
                                  <?php echo $this->Html->link(
                                   'Help your business grow with help from social media',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h2>
                           </div>
                           <!-- header end-->
                           <div class="entry-content">
                              <p>Financial engagements typically multifaceted solving specific digital marketing and challenges
                                 while building ongoing client capabilities. In addition to defining newer roles but helping
                                 develop...
                              </p>
                           </div>
                           <div class="post-footer text-right">
                               <?php echo $this->Html->link(
                                   'Continue Reading',
                                   ['controller' => 'news', 'action' => 'new3'],
                                       ['class' => 'btn btn-primary']); ?>
                           </div>
                        </div>
                        <!-- Post content right-->
                     </div>
                     <!-- post-body end-->
                  </div>
                  <!-- 4th post end-->
                  <div class="paging text-center">
                     <ul class="pagination">
                        <li><a href="#"><i class="icon icon-arrow-left"></i></a></li>
                        <li class="active"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#"><i class="icon icon-arrow-right"></i></a></li>
                     </ul>
                  </div>
               </div>
               <!-- Content Col end-->
               <div class="col-lg-4">
                  <div class="sidebar sidebar-right">
                     <div class="widget widget-search">
                        <div class="input-group" id="search">
                           <input class="form-control" placeholder="Search" type="search"><span class="input-group-btn"><i class="fa fa-search"></i></span>
                        </div>
                     </div>
                     <div class="widget recent-posts">
                        <h3 class="widget-title">Popular Posts</h3>
                        <ul class="unstyled clearfix">
                           <li class="media">
                              <div class="media-left media-middle">
                                 <?php echo $this->Html->image('/images/news/news1.jpg',
                                 ['alt' => '']); ?>
                              </div>
                              <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 19 Jan, 2018</a></span>
                                 <h4 class="entry-title">
                                     <?php echo $this->Html->link(
                                   'Uber is selling off its auto-leasing',
                                   ['controller' => 'news', 'action' => 'new3']); ?>
                                    <small>by Jhon</small>
                                 </h4>
                              </div>
                              <div class="clearfix"></div>
                           </li>
                           <!-- 1st post end-->
                           <li class="media">
                              <div class="media-left media-middle">
                                 <?php echo $this->Html->image('/images/news/news2.jpg',
                                 ['alt' => '']); ?>
                              </div>
                              <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><?php echo $this->Html->link(
                                   '24 Jan, 2018',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                                 <h4 class="entry-title">
                                     <?php echo $this->Html->link(
                                   'Apple reveals its new flagship',
                                   ['controller' => 'news', 'action' => 'new3']); ?>
                                    <small>by Jhon</small>
                                 </h4>
                              </div>
                              <div class="clearfix"></div>
                           </li>
                           <!-- 2nd post end-->
                           <li class="media">
                              <div class="media-left media-middle">
                                 <?php echo $this->Html->image('/images/news/news3.jpg',
                                 ['alt' => '']); ?>
                              </div>
                              <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><?php echo $this->Html->link(
                                      '27 jan, 2018',
                                      ['controller' => 'features', 'action' => 'feature4']); ?></span>
                                 <h4 class="entry-title">
                                     <?php echo $this->Html->link(
                                   'Help your bisness grow with help',
                                   ['controller' => 'news', 'action' => 'new3']); ?>
                                    <small>by Jhon</small>
                                 </h4>
                              </div>
                              <div class="clearfix"></div>
                           </li>
                           <!-- 3rd post end-->
                        </ul>
                     </div>
                     <!-- Recent post end-->
                     <div class="widget">
                        <h3 class="widget-title">Categories</h3>
                        <ul class="widget-nav-tabs">
                           <li><?php echo $this->Html->link(
                                       'Tax Planning',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(05)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Saving Strategy',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(06)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Financial Planning',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(11)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Mutual Funds',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(13)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Business loan',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(13)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Insurance Consulting',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(13)', ['class' => 'posts-count']); ?></li>
                        </ul>
                     </div>
                     <!-- Categories end-->
                     <div class="widget">
                        <h3 class="widget-title">Archives </h3>
                        <ul class="widget-nav-tabs">
                           <li><?php echo $this->Html->link(
                                   'February 2016',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'January 2016',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'December 2015',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'November 2015',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'October 2015',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                        </ul>
                     </div>
                     <!-- Archives end-->
                     <div class="widget widget-ad">
                        <div class="widget-ad-bg bg-overlay overlay-color">
                           <p>Looking for Business support</p>
                           <?php echo $this->Html->link(
                                   'Read More',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                   ['class' => 'btn btn-primary']); ?>
                        </div>
                     </div>
                     <div class="widget widget-tags">
                        <h3 class="widget-title">Tags </h3>
                        <ul class="unstyled clearfix">
                           <li><?php echo $this->Html->link(
                                   'Construction',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Design',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Project',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Building',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Finance',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Safety',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Contracting',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Planning',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                        </ul>
                     </div>
                     <!-- Tags end-->
                  </div>
                  <!-- Sidebar end-->
               </div>
               <!-- Sidebar Col end-->
            </div>
            <!-- Main row end-->
         </div>
         <!-- Container end-->
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/news-right-sidebar.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:47:01 GMT -->
</html>