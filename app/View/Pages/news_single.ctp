<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner2.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">News</h1>
                     <ol class="breadcrumb">
                        <li><a href="/">Home</a></li>
                        <li>news</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">
         <div class="container">
            <div class="row">
               <div class="col-lg-8">
                  <div class="post-content post-single">
                     <div class="post-media post-image">
                         <?php echo $this->Html->image('/images/news/news1.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?><span class="post-meta-date"><span class="day">27</span>June</span>
                     </div>
                     <div class="post-body clearfix">
                        <div class="entry-header">
                           <div class="post-meta"><span class="post-author">
                               <?php echo $this->Html->image('/images/news/avator1.png',
                                        ['alt' => ''],
                                        ['class' => 'avatar']); ?>
                                <?php echo $this->Html->link(
                                        'By John Doe',
                                        ['controller' => 'features', 'action' => 'feature4']); ?>
                               </span>
                              <span class="post-cat"><i class="icon icon-folder"></i><?php echo $this->Html->link(
                                   'Financial Planning',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></span>
                              <span class="post-comment"><i class="icon icon-comment"></i><a class="comments-link" href="#"></a>07</span>
                              <span class="post-tag"><i class="icon icon-tag"></i><a href="#"> Insurance, Business</a></span>
                           </div>
                           <h2 class="entry-title">
                               <?php echo $this->Html->link(
                                   'Uber is selling off its auto-leasing business',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></h2>
                        </div>
                        <!-- header end-->
                        <div class="entry-content">
                           <p>What a crazy time. I have five children in colleghigh school graduates.jpge or pursing post graduate
                              studies (ages 18 through 26 for those who were wondering). Each of my children attends college
                              far from home, the closest of which is more than 800 miles away. While I miss being with my
                              older children, I know that a college experience can be the source of great growth and can
                              provide them with even greater employment opportunities in future.</p>
                           <p>Many families look to the college years for children/grandchildren with mixed emotions excitement
                              and trepidation. These two words rarely go together, but when it comes to college, it seems
                              to be a perfect match.</p>
                           <blockquote>
                              <p>Financial engagements are typically multifaceted, solving for specific digital marketing and
                                 challenges while building.
                                 <cite>- Anger Mathe</cite>
                              </p>
                           </blockquote>
                           <h3>Equifax Data Breach - Protect Yourself</h3>
                           <p>Many families look to the college years for children/grandchildren with mixed emotions excitement
                              and trepidation. These two words rarely go together, but when it comes to college, it seems
                              to be a perfect match.</p>
                           <ul class="list-round">
                              <li>This does not surprise us as Bitcoin has been mentioned in the news</li>
                              <li>Here we are in the last couple months of summer with its </li>
                              <li>Collaborate with technology, information security, and business partners</li>
                              <li>Find and address performance issues</li>
                           </ul>
                        </div>
                        <!-- entry content end-->
                        <div class="post-footer clearfix">
                           <div class="post-tags float-left"><strong>Tags: </strong>
                                       <?php echo $this->Html->link(
                                   'Financial',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                                       <?php echo $this->Html->link(
                                   'Our Advisor',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                                       <?php echo $this->Html->link(
                                   'Market',
                                   ['controller' => 'features', 'action' => 'feature4']); ?>
                           </div>
                           <!-- Post tags end-->
                           <div class="share-items float-right">
                              <ul class="post-social-icons unstyled">
                                 <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                 <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                 <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                 <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                 <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                              </ul>
                           </div>
                           <!-- Share items end-->
                        </div>
                        <!-- Post footer end-->
                     </div>
                     <!-- post-body end-->
                  </div>
                  <!-- Post content end-->
                  <nav class="post-navigation clearfix mrtb-40">
                     <div class="post-previous">
                         <?php echo $this->Html->link(
                                 $this->Html->tag('h3', 'Bitcoin is the gag gift you should buy this holiday season').
                                 $this->Html->tag('span', 'Previous Post' . $this->Html->tag('i', '', ['class' => 'fa fa-long-arrow-left'])),
                                 ['controller' => 'features', 'action' => 'feature4'],
                                 ['escape' => false]); ?>
                     </div>
                     <div class="post-next">
                         <?php echo $this->Html->link(
                                 $this->Html->tag('h3', 'Apple reveals its new Melbourne flagship store').
                                 $this->Html->tag('span', 'Previous Post' . $this->Html->tag('i', '', ['class' => 'fa fa-long-arrow-left'])),
                                 ['controller' => 'features', 'action' => 'feature4'],
                                 ['escape' => false]); ?>
                         
                     </div>
                  </nav>
                  <div class="author-box solid-bg mrb-40">
                     <div class="author-img float-left">
                         <?php echo $this->Html->image('/images/news/avator1.png',
                                 ['alt' => '']); ?>
                     </div>
                     <div class="author-info">
                        <div class="post-social-icons float-right unstyled"><a href="#"><i class="fa fa-twitter"></i></a><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-instagram"></i></a></div>
                        <h3>Jonhy Rick</h3>
                        <p class="author-url"><?php echo $this->Html->link('www.finances.com', 'www.finances.com'); ?></p>
                        <p>Life is good. Here we are in the last couple months of summer with its warm weather, family activities,
                           getting ready for the children.</p>
                     </div>
                  </div>
                  <!-- Author box end-->
                  <!-- Post comment start-->
                  <div class="comments-area" id="comments">
                     <h3 class="comments-heading">07 Comments</h3>
                     <ul class="comments-list">
                        <li>
                           <div class="comment">
                               <?php echo $this->Html->image('/images/news/avator1.png',
                                        ['alt' => ''],
                                        ['class' => 'comment-avatar float-left']); ?>
                              <div class="comment-body">
                                 <div class="meta-data"><span class="float-right">
                                         <?php echo $this->Html->link(
                                                 'Reply' . $this->Html->tag('i', '', ['class' => 'fa fa-mail-reply-all']),
                                                 ['controller' => 'features', 'action' => 'feature4'],
                                                 ['class' => 'comment-reply', 'escape' => false]
                                                 ); ?></span>
                                    <span class="comment-author">Michelle Aimber</span><span class="comment-date">January 04, 2018</span></div>
                                 <div class="comment-content">
                                    <p>Life is good. Here we are in the last couple months of summer with its warm weather,
                                       family activities, getting ready for the children.</p>
                                 </div>
                              </div>
                           </div>
                           <!-- Comments end-->
                           <ul class="comments-reply">
                              <li>
                                 <div class="comment">
                                    <?php echo $this->Html->image('/images/news/avator3.png',
                                        ['alt' => ''],
                                        ['class' => 'comment-avatar float-left']); ?>
                                    <div class="comment-body">
                                       <div class="meta-data"><span class="float-right"><?php echo $this->Html->link(
                                                 'Reply' . $this->Html->tag('i', '', ['class' => 'fa fa-mail-reply-all']),
                                                 ['controller' => 'features', 'action' => 'feature4'],
                                                 ['class' => 'comment-reply', 'escape' => false]); ?></span>
                                          <span class="comment-author">Adam Smith</span><span class="comment-date">January 17, 2018</span></div>
                                       <div class="comment-content">
                                          <p>Life is good. Here we are in the last couple months of summer with its warm weather,
                                             family activities, getting ready for the children.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <!-- Comments end-->
                              </li>
                           </ul>
                           <!-- comments-reply end-->
                           <div class="comment last">
                              <?php echo $this->Html->image('/images/news/avator2.png',
                                        ['alt' => ''],
                                        ['class' => 'comment-avatar float-left']); ?>
                              <div class="comment-body">
                                 <div class="meta-data"><span class="float-right"><?php echo $this->Html->link(
                                                 'Reply' . $this->Html->tag('i', '', ['class' => 'fa fa-mail-reply-all']),
                                                 ['controller' => 'features', 'action' => 'feature4'],
                                                 ['class' => 'comment-reply', 'escape' => false]); ?></span>
                                    <span class="comment-author">Henry kendel</span><span class="comment-date">January 17, 2016</span></div>
                                 <div class="comment-content">
                                    <p>Life is good. Here we are in the last couple months of summer with its warm weather,
                                       family activities, getting ready for the children.</p>
                                 </div>
                              </div>
                           </div>
                           <!-- Comments end-->
                        </li>
                        <!-- Comments-list li end-->
                     </ul>
                     <!-- Comments-list ul end-->
                  </div>
                  <!-- Post comment end-->
                  <div class="comments-form border-box">
                     <h3 class="title-normal">Leave a comment</h3>
                     <form role="form">
                        <div class="row">
                           <div class="col-lg-6">
                              <div class="form-group">
                                 <input class="form-control" id="name" name="name" placeholder="Full Name" type="text" required="">
                              </div>
                           </div>
                           <!-- Col 4 end-->
                           <div class="col-lg-6">
                              <div class="form-group">
                                 <input class="form-control" id="email" name="email" placeholder="Email Address" type="email" required="">
                              </div>
                           </div>
                           <div class="col-lg-12">
                              <div class="form-group">
                                 <input class="form-control" placeholder="Website" type="text" required="">
                              </div>
                           </div>
                           <div class="col-lg-12">
                              <div class="form-group">
                                 <textarea class="form-control required-field" id="message" placeholder="Comments" rows="10" required=""></textarea>
                              </div>
                           </div>
                           <!-- Col 12 end-->
                        </div>
                        <!-- Form row end-->
                        <div class="clearfix text-right">
                           <button class="btn btn-primary" type="submit">Post Comment</button>
                        </div>
                     </form>
                     <!-- Form end-->
                  </div>
                  <!-- Comments form end-->
               </div>
               <!-- Content Col end-->
               <div class="col-lg-4">
                  <div class="sidebar sidebar-right">
                     <div class="widget widget-search">
                        <div class="input-group" id="search">
                           <input class="form-control" placeholder="Search" type="search"><span class="input-group-btn"><i class="fa fa-search"></i></span>
                        </div>
                     </div>
                     <div class="widget recent-posts">
                        <h3 class="widget-title">Popular Posts</h3>
                        <ul class="unstyled clearfix">
                           <li class="media">
                              <div class="media-left media-middle">
                                  <?php echo $this->Html->image('/images/news/news1.jpg',
                                        ['alt' => 'img']); ?>
                              </div>
                              <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 19 Jan, 2018</a></span>
                                 <h4 class="entry-title">
                                     <?php echo $this->Html->link(
                                             'Uber is selling off its auto-leasing',
                                             ['controller' => 'news', 'action' => 'new3']); ?>
                                 </h4>
                              </div>
                              <div class="clearfix"></div>
                           </li>
                           <!-- 1st post end-->
                           <li class="media">
                              <div class="media-left media-middle">
                                 <?php echo $this->Html->image('/images/news/news2.jpg',
                                        ['alt' => 'img']); ?>
                              </div>
                              <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 24 Jan, 2018</a></span>
                                 <h4 class="entry-title">
                                     <?php echo $this->Html->link(
                                             'Apple reveals its new flagship',
                                             ['controller' => 'news', 'action' => 'new3']); ?>
                                 </h4>
                              </div>
                              <div class="clearfix"></div>
                           </li>
                           <!-- 2nd post end-->
                           <li class="media">
                              <div class="media-left media-middle">
                                 <?php echo $this->Html->image('/images/news/news3.jpg',
                                        ['alt' => 'img']); ?>
                              </div>
                              <div class="media-body media-middle"><span class="post-date"><i class="icon icon-calendar-full"></i><a href="#"> 27 Jan, 2018</a></span>
                                 <h4 class="entry-title">
                                     <?php echo $this->Html->link(
                                             'Help your business grow with help',
                                             ['controller' => 'news', 'action' => 'new3']); ?>
                                 </h4>
                              </div>
                              <div class="clearfix"></div>
                           </li>
                           <!-- 3rd post end-->
                        </ul>
                     </div>
                     <!-- Recent post end-->
                     <div class="widget">
                        <h3 class="widget-title">Categories</h3>
                        <ul class="widget-nav-tabs">
                           <li><?php echo $this->Html->link(
                                       'Tax Planning',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(05)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Saving Strategy',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(06)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Financial Planning',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(11)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Mutual Funds',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(13)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Business loan',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(13)', ['class' => 'posts-count']); ?></li>
                           <li><?php echo $this->Html->link(
                                       'Insurance Consulting',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                               <?php echo $this->Html->tag('span', '(13)', ['class' => 'posts-count']); ?></li>
                        </ul>
                     </div>
                     <!-- Categories end-->
                     <div class="widget">
                        <h3 class="widget-title">Archives </h3>
                        <ul class="widget-nav-tabs">
                           <li><?php echo $this->Html->link(
                                   'February 2016',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'January 2016',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'December 2015',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'November 2015',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'October 2015',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                        </ul>
                     </div>
                     <!-- Archives end-->
                     <div class="widget widget-tags">
                        <h3 class="widget-title">Tags </h3>
                        <ul class="unstyled clearfix">
                           <li><?php echo $this->Html->link(
                                   'Construction',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Design',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Project',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Building',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Finance',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Safety',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Contracting',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                           <li><?php echo $this->Html->link(
                                   'Planning',
                                   ['controller' => 'features', 'action' => 'feature4']); ?></li>
                        </ul>
                     </div>
                     <!-- Tags end-->
                  </div>
                  <!-- Sidebar end-->
               </div>
               <!-- Sidebar Col end-->
            </div>
            <!-- Main row end-->
         </div>
         <!-- Container end-->
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/news-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:47:11 GMT -->
</html>