<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner5.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Service</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?></li>
                        <li>service</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">

         <div class="container">
            <div class="row">
               <div class="col-lg-8">
                  <div class="service-content">
                     <div class="heading">
                        <h2 class="content-title">Financial Planning</h2>
                     </div>
                     <div class="text-block mrb-40">
                        <p>Financial engagements are typically multifaceted, solving for specific digital marketing challenges
                           while building ongoing client capabilities. In addition to defining new roles and responsibilities
                           and helping develop employees’ skills, we address technology infrastructure issues and identify
                           potential partners.</p>
                     </div>
                     <div class="fw-single-image mrb-40">
                         <?php echo $this->Html->image('/images/services/service-single.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="text-block mrb-40">
                        <p>We work with clients to integrate the flow of the customer experience across channels (e.g., face-to-face,
                           telephone), opening up new lead sources, supporting sales for smaller-value transactions,and creating
                           new models for service. We continuously provide new and practical perspectives on the evolving.</p>
                        <div id="graph"></div>
                        <blockquote class="light">
                           <h4>Innovative Insurance Solution</h4>
                           <p>Forexnic commitment to innovation is reflected in the work we do each day. For example, we’ve
                              developed tailored life and health insurance policies especially for our Muslim Clients.</p>
                        </blockquote>
                     </div>
                     <div class="heading">
                        <h3 class="content-title-medium"> Research on the business plan</h3>
                     </div>
                     <div class="text-block">
                        <p>Show candidates personalized job recommendations, let them discover employees like them.You’ll gain
                           new skills and build on the strengths you bring to the firm. Business Analysts receive exceptional
                           training as well as frequent coaching and mentoring from colleagues on their teams.</p>
                     </div>
                  </div>
                  <div class="row mrb-60 mrt-60">
                     <div class="col-md-6">
                        <div class="heading mrb-20">
                           <h4 class="list-column-title"> Featured capabilities</h4>
                        </div>
                        <ul class="ts-list">
                           <li>Assisting senior consultants in projects</li>
                           <li>Share best practices and knowledge.</li>
                           <li>Collaborate with technology, information security, and business partners</li>
                           <li>Find and address performance issues</li>
                           <li>Maintains records to allow for historical trending analysis</li>
                        </ul>
                     </div>
                     <div class="col-md-6">
                        <div class="heading mrb-20">
                           <h4 class="list-column-title">Marketing Optimization</h4>
                        </div>
                        <ul class="ts-list">
                           <li>Assisting senior consultants in projects</li>
                           <li>Share best practices and knowledge.</li>
                           <li>Collaborate with technology, information security, and business partners</li>
                           <li>Find and address performance issues</li>
                           <li>Maintains records to allow for historical trending analysis</li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="sidebar sidebar-right">
                     <div class="widget no-padding no-border">
                        <ul class="service-menu">
                           <li><?php echo $this->Html->link(
                                       'Tax Planning',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                           </li>
                           <li><?php echo $this->Html->link(
                                       'Saving Strategy',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                           </li>
                           <li><?php echo $this->Html->link(
                                       'Financial Planning',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                           </li>
                           <li><?php echo $this->Html->link(
                                       'Mutual Funds',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                           </li>
                           <li><?php echo $this->Html->link(
                                       'Business loan',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                           </li>
                           <li><?php echo $this->Html->link(
                                       'Insurance Consulting',
                                       ['controller' => 'features', 'action' => 'feature4']); ?>
                           </li>
                        </ul>
                     </div>
                     <div class="widget no-padding no-border">
                        <h3 class="download-btn">
                            <?php echo $this->Html->link(
                                    $this->Html->tag('span', 'Brand Brochure'),
                                    $this->Html->tag('i', '', ['class' => 'fa fa-download float-right']),
                                    ['escape' => false]); ?>
                        </h3>
                     </div>
                     <div class="widget no-padding testimonial-static">
                        <div class="quote-item quote-classic"><span class="quote-text faq-quote-text">The Forexnic loan has been  the most attractive loan products on the market, helping numerous</span>
                           <div class="quote-item-footer quote-footer-classic">
                               <?php echo $this->Html->image('/images/clients/testimonial1.png',
                                       ['class' => 'testimonial-thumb'],
                                       ['alt' => 'testimonial']); ?>
                              <div class="quote-item-info">
                                 <p class="quote-author">Jhon Cameron</p><span class="quote-subtext">Manager Walton</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- For Chart-->
      <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
      <script type="text/javascript" src="js/morris.js"></script>
      <script type="text/javascript">
         Morris.Area({
            element: 'graph',
            behaveLikeLine: true,
            lineColors: ['#e36217', '#2154cf', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
            data: [{
                  x: '2012',
                  y: 9,
                  z: 7
               },
               {
                  x: '2013',
                  y: 6,
                  z: 8
               },
               {
                  x: '2014',
                  y: 6,
                  z: 5
               },
               {
                  x: '2015',
                  y: 8,
                  z: 10
               }
            ],
            xkey: 'x',
            ykeys: ['y', 'z'],
            labels: ['Y', 'Z']
         });
      </script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/service-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:30 GMT -->
</html>