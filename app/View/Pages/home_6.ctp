<body>

   <div class="body-inner">

      <?php echo $this->element('header_6'); ?>

      <div class="carousel slide mrt-140" id="main-slide" data-ride="carousel">
         <!-- Indicators-->
         <ol class="carousel-indicators visible-lg visible-md">
            <li class="active" data-target="#main-slide" data-slide-to="0"></li>
            <li data-target="#main-slide" data-slide-to="1"></li>
            <li data-target="#main-slide" data-slide-to="2"></li>
         </ol>
         <!-- Indicators end-->
         <!-- Carousel inner-->
         <div class="carousel-inner">
            <div class="carousel-item active" style="background-image:url(images/slider/bg1.jpg);">
               <div class="container">
                  <div class="slider-content text-left">
                     <div class="col-md-12">
                        <h2 class="slide-title title-light">Your future is created by</h2>
                        <h3 class="slide-sub-title">What you do today</h3>
                        <p class="slider-description lead">Nobody’s more committed to connecting you with the exceptional <br> top talents with the right fit
                           for your business than us.</p>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
            <div class="carousel-item" style="background-image:url(images/slider/bg2.jpg);">
               <div class="container">
                  <div class="slider-content text-center">
                     <div class="col-md-12">
                        <h3 class="slide-sub-title">We provide solutions to<br>grow your business</h3>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
            <div class="carousel-item" style="background-image:url(images/slider/bg3.jpg);">
               <div class="container">
                  <div class="slider-content text-right">
                     <div class="col-md-12">
                        <h2 class="slide-title title-light">Your future is created by</h2>
                        <h3 class="slide-sub-title">We care about your <br/>Business</h3>
                        <p class="slider-description lead">Nobody’s more committed to connecting you with the exceptional <br> top talents with the right fit
                           for your business than us.</p>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
         </div>
         <!-- Carousel inner end-->
         <!-- Controllers--><a class="left carousel-control carousel-control-prev" href="#main-slide" data-slide="prev"><span><i class="fa fa-angle-left"></i></span></a>
         <a class="right carousel-control carousel-control-next" href="#main-slide" data-slide="next"><span><i class="fa fa-angle-right"></i></span></a>
      </div>
      <!-- Carousel end-->

      <section id="about-us" class="about-us">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <h2 class="column-title title-small"><span>About us</span>Different kind of Financial firm</h2>
                  <p>The mission of the Bizipress Financial Planning Association is to setup, promote and implement high quality
                     standards for competence and ethical behavior for the financial advisory sector.</p>
                  <div class="gap-15"></div>
                  <div class="row">
                     <div class="col-12">
                        <div class="ts-feature-box">
                           <div class="ts-feature-info icon-left">
                              <span class="feature-icon">
                                 <i class="icon icon-business"></i>
                              </span>
                              <div class="feature-content">
                                 <h3 class="ts-feature-title">80% Cost Savings</h3>

                                 <p>We have a culturally diverse, forward thinking team.</p>
                              </div>
                              <!-- feature content end -->
                           </div>
                        </div>
                        <!-- feature box end-->
                        <div class="ts-feature-box">
                           <div class="ts-feature-info icon-left">
                              <span class="feature-icon">
                                 <i class="icon icon-chart22"></i>
                              </span>
                              <div class="feature-content">
                                 <h3 class="ts-feature-title">10.5% Conversation Rate</h3>

                                 <p>Business strategy is the means by which it sets.</p>
                              </div>
                              <!-- feature content end -->
                           </div>
                        </div>
                        <!-- feature box end-->
                        <div class="ts-feature-box">
                           <div class="ts-feature-info icon-left">
                              <span class="feature-icon">
                                 <i class="icon icon-deal"></i>
                              </span>
                              <div class="feature-content">
                                 <h3 class="ts-feature-title">4.5% Tax Saved</h3>

                                 <p>Make your dream come true have ideas, goals.</p>
                              </div>
                              <!-- feature content end -->
                           </div>
                        </div>
                        <!-- feature box end-->
                     </div>
                  </div>
                  <!-- container row end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-6 align-self-center">
                   <?php echo $this->Html->image('/images/pages/about_2.png',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
               </div>
               <!-- Col end-->
            </div>
            <!-- Main row end-->
         </div>
      </section>


      <section class="ts-features no-padding">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-4 feature-box1" style="background-image: url(images/features/feature1.jpg);">
                  <div class="ts-feature text-center">
                     <div class="ts-feature-info"><i class="icon icon-consut2"></i>
                        <h3 class="ts-feature-title">Best Consulting</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas, goals, and dreams.</p>
                     </div>
                  </div>
                  <!-- feature 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4 feature-box2" style="background-image: url(images/features/feature2.jpg);">
                  <div class="ts-feature text-center">
                     <div class="ts-feature-info"><i class="icon icon-chart2"></i>
                        <h3 class="ts-feature-title">Marketing growth</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas, goals, and dreams.</p>
                     </div>
                  </div>
                  <!-- feature 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4 feature-box3" style="background-image: url(images/features/feature3.jpg);">
                  <div class="ts-feature text-center">
                     <div class="ts-feature-info"><i class="icon icon-clock3"></i>
                        <h3 class="ts-feature-title">On time services</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas, goals, and dreams.</p>
                     </div>
                  </div>
                  <!-- feature 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </section>

      <section class="ts-services solid-bg" id="ts-services">
         <div class="container">
            <div class="row text-left">
               <div class="col-lg-12">
                  <h2 class="section-title border-title-left">Our Best Services <span class="section-title-tagline title-light">What We Do</span></h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-12">
                  <div class="featured-tab">
                     <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link active animated fadeIn" href="#tab_one" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-pie-chart2"></i></span><span class="tab-text-title">Best Consulting</span></span></a></li>
                        <!-- list 1 end-->
                        <li class="nav-item"><a class="nav-link animated fadeIn" href="#tab_two" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-loan"></i></span><span class="tab-text-title">Market Growth</span></span></a></li>
                        <!-- list 2 end-->
                        <li class="nav-item"><a class="nav-link animated fadeIn" href="#tab_three" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-savings"></i></span><span class="tab-text-title">Saving Strategy</span></span></a></li>
                        <!-- list 3 end-->
                        <li class="nav-item"><a class="nav-link animated fadeIn" href="#tab_four" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-consult"></i></span><span class="tab-text-title">Best Consulting</span></span></a></li>
                        <!-- list 4 end-->
                     </ul>
                     <!-- Nav-tabs end-->
                     <div class="tab-content">
                        <div class="tab-pane active animated fadeInRight" id="tab_one">
                           <div class="row">
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle" style="background-image:url(images/tabs/tab-shape.png);">
                                     <?php echo $this->Html->image('/images/tabs/img1.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">Solution for your Financial needs</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                    <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 1 end-->
                        <div class="tab-pane animated fadeInRight" id="tab_two">
                           <div class="row">
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">We provide loan to Financial Needs</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                           <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle align-self-center" style="background-image:url(images/tabs/tab-shape.png);">
                                    <?php echo $this->Html->image('/images/tabs/img2.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 2 end-->
                        <div class="tab-pane animated fadeInRight" id="tab_three">
                           <div class="row">
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle" style="background-image:url(images/tabs/tab-shape.png);">
                                    <?php echo $this->Html->image('/images/tabs/img3.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">Register for online invoicing</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                    <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 3 end-->
                        <div class="tab-pane animated fadeInRight" id="tab_four">
                           <div class="row">
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">We Provide Insurance Solutions</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                    <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle" style="background-image:url(images/tabs/tab-shape.png);">
                                    <?php echo $this->Html->image('/images/tabs/img4.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 3 end-->
                     </div>
                     <!-- tab content-->
                     <div class="gap-60"></div>
                     <div class="text-center">
                         <?php echo $this->Html->link(
                                 'Check More Services',
                                 ['controller' => 'features', 'action' => 'feature4'],
                                 ['class' => 'btn btn-primary']); ?>
                     </div>
                  </div>
                  <!-- Schedule tab end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Service end-->

      <section class="ts-featured-cases">
         <div class="container">
            <div class="section-title-vertical">
               <h2 class="section-title">Featured Cases</h2>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="owl-carousel owl-theme featured-cases-slide" id="featured-cases-slide">
                  <div class="item">
                     <div class="featured-projects-content">
                        <div class="featured-projects-text float-left">
                           <h2 class="column-title"><span>Marketing Growth</span> Modern Woodman Ltd.</h2>
                           <p class="intro-desc">A business strategy is the means by which it sets out to achieve its desired ends. You have ideas,
                              goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like
                              you and make your dream come true.</p>
                           <p><?php echo $this->Html->link(
                                   'Read More',
                                   array('controller' => 'features', 'action' => 'feature4'),
                                   array('class' => 'btn btn-primary')); ?>
                           </p>
                        </div>
                        <div class="features-slider-img float-right">
                            <?php echo $this->Html->image('/images/slider-small/featured-slider1.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                        </div>
                     </div>
                  </div>
                  <!-- Item 1 end-->
                  <div class="item">
                     <div class="featured-projects-content">
                        <div class="featured-projects-text pull-left">
                           <h2 class="column-title"><span>Tax Planning</span>Alpha Century Software Inc.</h2>
                           <p class="intro-desc">A business strategy is the means by which it sets out to achieve its desired ends. You have ideas,
                              goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like
                              you and make your dream come true.</p>
                           <p><?php echo $this->Html->link(
                                   'Read More',
                                   array('controller' => 'features', 'action' => 'feature4'),
                                   array('class' => 'btn btn-primary')); ?>
                           </p>
                        </div>
                        <div class="features-slider-img pull-right">
                           <?php echo $this->Html->image('/images/slider-small/featured-slider2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                        </div>
                     </div>
                  </div>
                  <!-- Item 2 end-->
                  <div class="item">
                     <div class="featured-projects-content">
                        <div class="featured-projects-text float-left">
                           <h2 class="column-title"><span>Business Loan</span>Spider Microsystem</h2>
                           <p class="intro-desc">A business strategy is the means by which it sets out to achieve its desired ends. You have ideas,
                              goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like
                              you and make your dream come true.</p>
                           <p><?php echo $this->Html->link(
                                   'Read More',
                                   array('controller' => 'features', 'action' => 'feature4'),
                                   array('class' => 'btn btn-primary')); ?>
                           </p>
                        </div>
                        <div class="features-slider-img float-right">
                           <?php echo $this->Html->image('/images/slider-small/featured-slider2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                        </div>
                     </div>
                  </div>
                  <!-- Item 3 end-->
               </div>
               <!-- Featured Projects slider end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Featured projects end-->


      <section class="ts-facts-area solid-bg no-padding" id="ts-facts-area">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-6 col-md-12">
                  <div class="intro-video">
                      <?php echo $this->Html->image('/images/intro-video.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     <a class="popup" href="https://www.youtube.com/embed/XhveHKJWnOQ?autoplay=1&amp;loop=1">
                        <div class="video-icon"><i class="icon icon-play"></i></div>
                     </a>
                  </div>
                  <!-- Intro video end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-6 col-md-12 ts-padding text-lg-left text-center">
                  <h2 class="column-title">25 Years of Experience</h2>
                  <p class="intro-desc">We are rethoric question ran over her cheek When she reached the first hills of the Italic Mountains.</p>
                  <div class="gap-20"></div>
                  <div class="row facts-wrapper text-center">
                     <div class="col-md-6">
                        <div class="ts-facts"><span class="facts-icon"><i class="icon icon-chart2"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="counterUp">85</span></h4>
                              <p class="facts-desc">Business Growth %</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col 1 end-->
                     <div class="col-md-6">
                        <div class="ts-facts"><span class="facts-icon"><i class="icon icon-invest"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="counterUp">467</span></h4>
                              <p class="facts-desc">Successful Investment</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col 1 end-->
                  </div>
                  <!--Row End -->
                  <div class="gap-30"></div>
                  <div class="row facts-wrapper text-center">
                     <div class="col-md-6">
                        <div class="ts-facts facts-col"><span class="facts-icon"><i class="icon icon-money-1"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="counterUp">2435</span></h4>
                              <p class="facts-desc">Cases Completed</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col 1 end-->
                     <div class="col-md-6">
                        <div class="ts-facts"><span class="facts-icon"><i class="icon icon-deal"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="counterUp">139</span></h4>
                              <p class="facts-desc">Running Projects</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col 1 end-->
                  </div>
                  <!-- Row End -->
               </div>
               <!-- Content Row 2 end-->
            </div>
            <!-- Col end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Fun fact end-->

      <section class="ts-team">
         <div class="container">
            <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title"><span>Our People</span>Best Team</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                         <?php echo $this->Html->image('/images/team/team1.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Patrick Ryan</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team3.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Craig Robinson</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team4.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Andrew Robinson</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Section Team end-->

      <section class="quote-area bg-overlay overlay-color" id="quote-area">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 qutoe-form-inner-left">
                  <div class="quote_form">
                     <h2 class="column-title title-white"><span>We are always ready</span> Request a call back</h2>
                     <div class="row">
                        <div class="col-lg-6">
                           <div class="form-group">
                              <input class="form-control" id="name" name="name" placeholder="Full Name" required="">
                           </div>
                        </div>
                        <div class="col-lg-6">
                           <div class="form-group">
                              <input class="form-control" id="email" name="email" placeholder="Email Address" required="">
                           </div>
                        </div>
                     </div>
                     <!-- Row 1 end-->
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group">
                              <input class="form-control" id="subject" name="subject" type="text" placeholder="Subject" required="">
                           </div>
                        </div>
                     </div>
                     <!-- Row end-->
                     <div class="row">
                        <div class="col-lg-12">
                           <div class="form-group">
                              <textarea class="form-control" placeholder="Message" rows="6" name="comment" required=""></textarea>
                           </div>
                        </div>
                     </div>
                     <!-- Row end-->
                     <div class="form-group text-right mb-0">
                        <input class="button btn btn-primary" type="submit" value="Send Message">
                     </div>
                  </div>
                  <!-- Quote form end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-6 align-self-center">
                  <div class="owl-carousel owl-theme testimonial-slide owl-dark" id="testimonial-slide">
                     <div class="item">
                        <div class="quote-item quote-square"><span class="quote-text">The Bizipress loan has been  the most attractive loan products on the market, helping numerous businesses gain access to financing they would not be able to obtain conventionally and at extremely favorable rates and terms.</span>
                           <div class="quote-item-footer">
                               <?php echo $this->Html->image('/images/clients/testimonial1.png',
                                    ['alt' => 'testimonial'],
                                    ['class' => 'testimonial-thumb']); ?>
                              <div class="quote-item-info">
                                 <p class="quote-author">Gabriel Denis</p><span class="quote-subtext">Chairman, OKT</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                     <!-- Item 1 end-->
                     <div class="item">
                        <div class="quote-item quote-square"><span class="quote-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitoa tion ullamco laboris nisi aliquip consequat.</span>
                           <div class="quote-item-footer">
                              <?php echo $this->Html->image('/images/clients/testimonial2.png',
                                    ['alt' => 'testimonial'],
                                    ['class' => 'testimonial-thumb']); ?>
                              <div class="quote-item-info">
                                 <h3 class="quote-author">Weldon Cash</h3><span class="quote-subtext">CFO, First Choice</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                     <!-- Item 2 end-->
                     <div class="item">
                        <div class="quote-item quote-square"><span class="quote-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor inci done idunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitoa tion ullamco laboris nisi ut commodo consequat.</span>
                           <div class="quote-item-footer">
                              <?php echo $this->Html->image('/images/clients/testimonial3.png',
                                    ['alt' => 'testimonial'],
                                    ['class' => 'testimonial-thumb']); ?>
                              <div class="quote-item-info">
                                 <h3 class="quote-author">Minter Puchan</h3><span class="quote-subtext">Director, AKT</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                     <!-- Item 3 end-->
                  </div>
                  <!-- Testimonial carousel end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Quote area end-->

      <section class="news" id="news">
         <div class="container">
            <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title"><span>Don't Miss</span>Our Latest News</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="latest-post post-large">
                     <div class="latest-post-media">
                         <?php echo $this->Html->link(
                                 $this->Html->image('/images/news/news1.jpg',
                                         ['alt' => 'img'],
                                         ['class' => 'img-fluid']),
                                 ['controller' => 'features', 'action' => 'feature4'],
                                 ['class' => 'latest-post-img', 'escape' => false]); ?>
                        <?php echo $this->Html->link(
                                'News',
                                ['controller' => 'news', 'action' => 'new1'],
                                ['class' => 'post-cat']); ?>
                        <div class="post-body"><span class="post-item-date">20 Nov, 2017</span>
                           <h4 class="post-title">
                               <?php echo $this->Html->link(
                                       "Spacex's interviewing process is rude as hell",
                                       array('controller' => 'features', 'action' => 'feature4')); ?>
                           </h4>
                            <?php echo $this->Html->link(
                                   'Read More',
                                   array('controller' => 'features', 'action' => 'feature4'),
                                   array('class' => 'btn btn-primary')); ?>
                        </div>
                        <!-- Post body end-->
                     </div>
                     <!-- Post media end-->
                  </div>
                  <!-- Latest post end-->
               </div>
               <!-- Col big news end-->
               <div class="col-lg-6">
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="latest-post">
                           <div class="post-body"><?php echo $this->Html->link(
                                'News',
                                ['controller' => 'news', 'action' => 'new1'],
                                ['class' => 'post-cat']); ?>
                              <h4 class="post-title">
                                  <?php echo $this->Html->link(
                                          'American Express finally ditches',
                                          ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h4><span class="post-item-date">20 Nov, 2017</span>
                              <div class="post-text">
                                 <p>Earlier this year, the firm announced it had reached its goal of hiring.</p>
                                 <div class="text-right"><?php echo $this->Html->link(
                                         'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                         array('controller' => 'features', 'action' => 'feature4'),
                                         array('escape' => false)); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Latest post end-->
                     </div>
                     <!-- Col end-->
                     <div class="col-lg-6">
                        <div class="latest-post">
                           <div class="post-body"><?php echo $this->Html->link(
                                'News',
                                ['controller' => 'news', 'action' => 'new1'],
                                ['class' => 'post-cat']); ?>
                              <h4 class="post-title">
                                  <?php echo $this->Html->link(
                                          'Disney buys 21st Century Fox',
                                          ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h4><span class="post-item-date">20 Nov, 2017</span>
                              <div class="post-text">
                                 <p>Earlier this year, the firm announced it had reached its goal of hiring.</p>
                                 <div class="text-right"><?php echo $this->Html->link(
                                         'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                         array('controller' => 'features', 'action' => 'feature4'),
                                         array('escape' => false)); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Latest post end-->
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- row end-->
               </div>
               <!-- Col small news end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- News end-->

      <section class="clients-area pab-120" id="clients-area">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 owl-carousel owl-theme text-center partners" id="partners-carousel">
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client1.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client2.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client3.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client4.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client5.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client6.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client7.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
               </div>
               <!-- Owl carousel end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Partners end-->

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/index-6.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:03 GMT -->
</html>