<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div id="banner-area" class="banner-area" style="background-image:url(images/banner/banner5.jpg)">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">404</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>404</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end -->
            </div>
            <!-- Row end -->
         </div>
         <!-- Container end -->
      </div>
      <!-- Banner area end -->

      <section id="main-container" class="main-container not-found">
         <div class="container">
            <div class="row">
               <div class="col-6 text-center align-self-center">
                  <div class="error-page text-center">
                     <div class="error-code">
                        <strong>404</strong>
                     </div>
                     <div class="error-message">
                        <h3>Oops... Page Not Found!</h3>
                     </div>
                     <div class="error-body">
                        Try using the button below to go to main page of the site <br>
                        <?php echo $this->Html->link(
                                'Go to Home' . $this->Html->tag('i', '&nbsp;', ['class' => 'fa fa-arrow-circle-left']),
                                ['controller' => 'home', 'action' => 'index'],
                                ['class' => 'btn btn-primary solid blank', 'escape' => false]); ?>
                     </div>
                  </div>
               </div>
               <div class="col-lg-6 text-right">
                   <?php echo $this->Html->image('/images/404.png',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
               </div>
            </div>
            <!-- Main row end -->
         </div>
         <!-- Container end -->
      </section>

        <?php echo $this->element('footer'); ?>

      <div id="back-to-top" data-spy="affix" data-offset-top="10" class="back-to-top affix">
         <button class="btn btn-primary" title="Back to Top">
            <i class="fa fa-angle-double-up"></i>
         </button>
         <!-- Button End -->
      </div>
      <!-- Back to Top End -->


      <!-- Javascript Files
	================================================== -->

      <!-- initialize jQuery Library -->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Bootstrap jQuery -->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel -->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter -->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints -->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box -->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll -->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Template custom -->
      <script type="text/javascript" src="js/custom.js"></script>

   </div>
   <!-- Body inner end -->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:32 GMT -->
</html>
