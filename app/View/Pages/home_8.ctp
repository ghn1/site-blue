<body>

   <div class="body-inner">

      <?php echo $this->element('header_8'); ?>

      <div class="carousel slide" id="main-slide" data-ride="carousel">
         <!-- Indicators-->
         <ol class="carousel-indicators visible-lg visible-md">
            <li class="active" data-target="#main-slide" data-slide-to="0"></li>
            <li data-target="#main-slide" data-slide-to="1"></li>
            <li data-target="#main-slide" data-slide-to="2"></li>
         </ol>
         <!-- Indicators end-->
         <!-- Carousel inner-->
         <div class="carousel-inner">
            <div class="carousel-item active" style="background-image:url(images/slider/bg1.jpg);">
               <div class="container">
                  <div class="slider-content text-left">
                     <div class="col-md-12">
                        <h2 class="slide-title title-light">Your future is created by</h2>
                        <h3 class="slide-sub-title">What you do today</h3>
                        <p class="slider-description lead">Nobody’s more committed to connecting you with the exceptional <br> top talents with the right fit
                           for your business than us.</p>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
            <div class="carousel-item" style="background-image:url(images/slider/bg2.jpg);">
               <div class="container">
                  <div class="slider-content text-center">
                     <div class="col-md-12">
                        <h3 class="slide-sub-title">We provide solutions to<br>grow your business</h3>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
            <div class="carousel-item" style="background-image:url(images/slider/bg3.jpg);">
               <div class="container">
                  <div class="slider-content text-right">
                     <div class="col-md-12">
                        <h2 class="slide-title title-light">Your future is created by</h2>
                        <h3 class="slide-sub-title">We care about your Business</h3>
                        <p class="slider-description lead">Nobody’s more committed to connecting you with the exceptional <br> top talents with the right fit
                           for your business than us.</p>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
         </div>
         <!-- Carousel inner end-->
         <!-- Controllers--><a class="left carousel-control carousel-control-prev" href="#main-slide" data-slide="prev"><span><i class="fa fa-angle-left"></i></span></a>
         <a class="right carousel-control carousel-control-next" href="#main-slide" data-slide="next"><span><i class="fa fa-angle-right"></i></span></a>
      </div>
      <!-- Carousel end-->

      <section id="ts-intro-area" class="intro-area">
         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <h2 class="column-title">25 years of experience</h2>
                  <p class="intro-desc">It’s an approach that brings together the best of financial planning and the best of investment management.
                  </p>
                  <div class="gap-40"></div>
                  <div class="row facts-wrapper text-center">
                     <div class="col-md-6">
                        <div class="ts-facts"><span class="facts-icon"><i class="icon icon-chart2"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="counterUp">85</span></h4>
                              <p class="facts-desc">Business Growth %</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col end-->
                     <div class="col-md-6">
                        <div class="ts-facts"><span class="facts-icon"><i class="icon icon-invest"></i></span>
                           <div class="ts-facts-content">
                              <h4 class="ts-facts-num"><span class="counterUp">467</span></h4>
                              <p class="facts-desc">Successful Investment</p>
                           </div>
                        </div>
                        <!-- Facts end-->
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Content Row end-->
               </div>
               <div class="col-lg-6">
                  <div id="graph"></div>
               </div>
            </div>
         </div>
      </section>
      <section class="ts-features no-padding">
         <div class="container-fluid">
            <div class="row">
               <div class="col-lg-4 feature-box1" style="background-image: url(images/features/feature1.jpg);">
                  <div class="ts-feature text-center">
                     <div class="ts-feature-info"><i class="icon icon-consut2"></i>
                        <h3 class="ts-feature-title">Best Consulting</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas, goals, and dreams.</p>
                     </div>
                  </div>
                  <!-- feature 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4 feature-box2" style="background-image: url(images/features/feature2.jpg);">
                  <div class="ts-feature text-center">
                     <div class="ts-feature-info"><i class="icon icon-chart2"></i>
                        <h3 class="ts-feature-title">Marketing growth</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas, goals, and dreams.</p>
                     </div>
                  </div>
                  <!-- feature 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4 feature-box3" style="background-image: url(images/features/feature3.jpg);">
                  <div class="ts-feature text-center">
                     <div class="ts-feature-info"><i class="icon icon-clock3"></i>
                        <h3 class="ts-feature-title">On time services</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas, goals, and dreams.</p>
                     </div>
                  </div>
                  <!-- feature 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </section>


      <section id="call-to-action" class="call-to-action solid-bg">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 align-self-center">
                  <h3 class="call-to-action-title">We Offer Financial Strategies &amp; Superior Services</h3>
                  <p>
                     Our mission is to provide quality guidance, build relationships of
                     <br> trust, and develop innovative solutions
                  </p>
               </div>
               <div class="col-lg-4 text-right">
                   <?PHP echo $this->Html->link(
                           'Get Free Quote',
                           ['controller' => 'features', 'action' => 'feature4'],
                           ['class' => 'btn btn-ptimary']); ?>
               </div>
            </div>
         </div>
      </section>


      <section class="ts-team pab-120">
         <div class="container">
            <div class="row text-center">
               <div class="col-lg-12">
                  <h2 class="section-title"><span>Our People</span>Best Team</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                         <?php echo $this->Html->image('/images/team/team1.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 1 end-->
                  </div>
                  <!-- Team classic 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 2 end-->
                  </div>
                  <!-- Team classic 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team3.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 3 end-->
                  </div>
                  <!-- Team classic 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
      </section>
      <!-- Section Team end-->


      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- For Chart-->
      <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
      <script type="text/javascript" src="js/morris.js"></script>
      <script type="text/javascript">
         Morris.Area({
            element: 'graph',
            behaveLikeLine: true,
            lineColors: ['#e36217', '#2154cf', '#4da74d', '#afd8f8', '#edc240', '#cb4b4b', '#9440ed'],
            data: [{
                  x: '2012',
                  y: 9,
                  z: 7
               },
               {
                  x: '2013',
                  y: 6,
                  z: 8
               },
               {
                  x: '2014',
                  y: 6,
                  z: 5
               },
               {
                  x: '2015',
                  y: 8,
                  z: 10
               }
            ],
            xkey: 'x',
            ykeys: ['y', 'z'],
            labels: ['Y', 'Z']
         });
      </script>
      <!-- Template custom-->
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/index-8.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:03 GMT -->
</html>