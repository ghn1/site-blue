<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner1.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">About Us</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>About Us</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">

         <div class="container">
            <div class="row">
               <div class="col-lg-6">
                  <h2 class="column-title title-small"><span>About us</span>Different kind of Financial firm</h2>
                  <p>The mission of the Bizipress Financial Planning Association is to setup, promote and implement high quality
                     standards for competence and ethical behavior for the financial advisory sector.</p>
                  <div class="gap-15"></div>
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="ts-feature-box">
                           <div class="ts-feature-info">
                               <?php echo $this->Html->image('/images/pages/color_icon1.png',
                                       ['alt' => '']); ?>
                              <h3 class="ts-feature-title">Best Finance Brand</h3>
                              <p>Mission of Bizipress is Planning Association is to setup</p>
                           </div>
                        </div>
                        <!-- feature box-1 end-->
                     </div>
                     <!-- col-1 end-->
                     <div class="col-lg-6">
                        <div class="ts-feature-box">
                           <div class="ts-feature-info">
                              <?php echo $this->Html->image('/images/pages/color_icon2.png',
                                       ['alt' => '']); ?>
                              <h3 class="ts-feature-title">ISO Certified</h3>
                              <p>Mission of Bizipress Financial Planning is to promote. </p>
                           </div>
                        </div>
                        <!-- feature box-2 end-->
                     </div>
                     <!-- col-2 end-->
                  </div>
                  <!-- container row end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-6">
                   <?php echo $this->Html->image('/images/pages/about_2.png',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
               </div>
               <!-- Col end-->
            </div>
            <!-- Main row end-->
         </div>
         
         <div class="gap-80"></div>

         <div class="ts-facts-area-bg bg-overlay">
            <div class="container">
               <div class="row facts-wrapper text-center">
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-money-1"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">2435</span></h4>
                           <p class="facts-desc">Cases Completed</p>
                        </div>
                     </div>
                     <!-- Facts 1 end-->
                  </div>
                  <!-- Col 1 end-->
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-invest"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">467</span></h4>
                           <p class="facts-desc">Successful Investment</p>
                        </div>
                     </div>
                     <!-- Facts 2 end-->
                  </div>
                  <!-- Col 2 end-->
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-chart2"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">85 </span>%</h4>
                           <p class="facts-desc">Business Growth </p>
                        </div>
                     </div>
                     <!-- Facts 3 end-->
                  </div>
                  <!-- Col 3 end-->
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-deal"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">139</span></h4>
                           <p class="facts-desc">Running Projects</p>
                        </div>
                     </div>
                     <!-- Facts 4 end-->
                  </div>
                  <!-- Col 4 end-->
               </div>
               <!-- Row end-->
            </div>
            <!-- Container 2 end-->
         </div>
         <!-- Facts area end-->
         <div class="gap-80"></div>
         
         <section id="ts-features-light" class="ts-features-light">
         <div class="container">
            <div class="row">
               <div class="col-lg-4 text-center">
                  <div class="ts-feature-box">
                     <div class="ts-feature-info">
                        <i class="icon icon-consut2"></i>
                        <h3 class="ts-feature-title">Serviços</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks</p>
                     </div>
                  </div>
                  <!-- feature box-1 end-->
               </div>
               <!-- col-1 end-->
               <div class="col-lg-4 text-center border-left">
                  <div class="ts-feature-box">
                     <div class="ts-feature-info">
                        <i class="icon icon-chart2"></i>
                        <h3 class="ts-feature-title">Serviços</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks</p>
                     </div>
                  </div>
                  <!-- feature box-2 end-->
               </div>
               <!-- col-2 end-->
               <div class="col-lg-4 text-center border-left">
                  <div class="ts-feature-box">
                     <div class="ts-feature-info">
                        <i class="icon icon-clock3"></i>
                        <h3 class="ts-feature-title">Serviços</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks</p>
                     </div>
                  </div>
                  <!-- feature box-2 end-->
               </div>
               <!-- col-3 end-->
            </div>
         </div>
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/about-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:07 GMT -->
</html>