<body>
   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner5.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Addons</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>Addons</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <h3 class="addons-title">Case Box Style</h3>
               </div>
               <div class="col-lg-4 col-md-12">
                  <div class="ts-case-box">
                     <div class="ts-case-image-wrapper">
                         <?php echo $this->Html->image('/images/case-study/case-study1.jpg',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
                        <div class="ts-case-content">
                           <h3 class="case-title">
                              <small>Financial Analysis</small>Digital Transformation</h3>
                           <p>
                               <?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?>
                           </p>
                        </div>
                     </div>
                     <!-- Case Content end-->
                  </div>
               </div>
               <div class="col-lg-4 col-md-12">
                  <div class="ts-case-box">
                     <div class="ts-case-image-wrapper">
                        <?php echo $this->Html->image('/images/case-study/case-study1.jpg',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
                        <div class="ts-case-content">
                           <h3 class="case-title">
                              <small>Financial Analysis</small>Digital Transformation</h3>
                           <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?>
                           </p>
                        </div>
                     </div>
                     <!-- Case Content end-->
                  </div>
               </div>
               <div class="col-lg-4 col-md-12">
                  <div class="ts-case-box">
                     <div class="ts-case-image-wrapper">
                        <?php echo $this->Html->image('/images/case-study/case-study1.jpg',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
                        <div class="ts-case-content">
                           <h3 class="case-title">
                              <small>Financial Analysis</small>Digital Transformation</h3>
                           <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?>
                           </p>
                        </div>
                     </div>
                     <!-- Case Content end-->
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <h3 class="addons-title">Service Box Style</h3>
               </div>
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service1.jpg',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content"><span class="ts-service-icon"><i class="icon icon-pie-chart2"></i></span>
                        <h3 class="service-title">Best Consulting</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service1 end-->
               </div>
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service1.jpg',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content"><span class="ts-service-icon"><i class="icon icon-pie-chart2"></i></span>
                        <h3 class="service-title">Best Consulting</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service1 end-->
               </div>
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service1.jpg',
                           ['alt' => ''],
                           ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content"><span class="ts-service-icon"><i class="icon icon-pie-chart2"></i></span>
                        <h3 class="service-title">Best Consulting</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service1 end-->
               </div>
            </div>
         </div>
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/addons-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:30 GMT -->
</html>