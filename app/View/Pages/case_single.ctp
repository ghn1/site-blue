<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner1.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Case Single</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>cases</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container no-padding" id="main-container">

         <div class="ts-single-case-info" id="ts-single-case-info">
            <div class="container">
               <div class="row">
                  <div class="col-lg-4">
                     <div class="list-style-classic box-solid">
                        <h3 class="classic-title">Alpha Century Software Inc.</h3>
                        <ul>
                           <li>
                              <div class="case-info-label">Client</div>
                              <div class="case-info-content">Jhon Carter</div>
                           </li>
                           <li>
                              <div class="case-info-label">Catagory</div>
                              <div class="case-info-content">Marketing Growth</div>
                           </li>
                           <li>
                              <div class="case-info-label">Date</div>
                              <div class="case-info-content">20 Nov, 2017</div>
                           </li>
                           <li>
                              <div class="case-info-label">Live demo</div>
                              <div class="case-info-content">www.domain.com</div>
                           </li>
                           <li>
                              <div class="case-info-label">Status</div>
                              <div class="case-info-content">Done</div>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <!-- Col End-->
                  <div class="col-lg-8">
                     <div class="case-single-img">
                         <?php echo $this->Html->image('/images/case-study/case-details-big.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                  </div>
               </div>
               <!-- Row End-->
            </div>
            <!-- Container End-->
         </div>

         <div class="ts-case-challange" id="ts-case-challange">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="case-details-inner">
                        <div class="heading">
                           <h3 class=" content-title border-none"> The Challenge: </h3>
                        </div>
                        <div class="ts-text-block">
                           <p>A business strategy is the means by which it sets out to achieve its desired ends. You have ideas,
                              goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like
                              you and make your dream come true have ideas, goals.</p>
                        </div>
                        <ul class="ts-list">
                           <li>Collaborate with the technology information security, and business partners.</li>
                           <li>Find and address performance issues are main.</li>
                           <li>Assisting senior consultants projects priorate first.</li>
                           <li>Share best both practices and knowledge need to think.</li>
                        </ul>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="case-single-img text-right">
                        <?php echo $this->Html->image('/images/case-study/case-study1.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                  </div>
               </div>
               <div class="row case-img-left">
                  <div class="col-lg-6">
                     <div class="case-single-img text-left">
                        <?php echo $this->Html->image('/images/case-study/case-study2.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                  </div>
                  <div class="col-lg-6">
                     <div class="case-details-inner">
                        <div class="heading">
                           <h3 class=" content-title border-none">Possible Solution: </h3>
                        </div>
                        <div class="ts-text-block">
                           <p>A business strategy is the means by which it sets out to achieve its desired ends. You have ideas,
                              goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like
                              you and make your dream come true have ideas, goals.</p>
                        </div>
                        <ul class="ts-list">
                           <li>Collaborate with the technology information security, and business partners.</li>
                           <li>Find and address performance issues are main.</li>
                           <li>Assisting senior consultants projects priorate first.</li>
                           <li>Share best both practices and knowledge need to think.</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
         </div>

         <div class="ts-case-result" id="ts-case-result">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="case-details-inner">
                        <div class="heading">
                           <h3 class=" content-title border-none"> The Result: </h3>
                        </div>
                        <div class="ts-text-block">
                           <p>A business strategy is the means by which it sets out to achieve its desired ends. You have ideas,
                              goals, and dreams. We have a culturally diverse, forward thinking team looking for talent like
                              you.
                           </p>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-4">
                           <div class="case-counter">
                              <p><span class="counterUp">3000</span>$</p>
                              <h3>Cost savings</h3>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="case-counter">
                              <p><span class="counterUp">4.5</span>%</p>
                              <h3>Tax Saved</h3>
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="case-counter">
                              <p><span class="counterUp">10.5</span>%</p>
                              <h3>Conversion</h3>
                           </div>
                        </div>
                     </div>
                     <p>A business strategy is the means by which it sets out to achieve its desired forward thinking team looking
                        for talent like you and make your dream come true have ideas, goals.</p>
                  </div>
                  <div class="col-lg-6 text-right">
                     <div class="single-case-img">
                        <?php echo $this->Html->image('/images/case-study/case-study3.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <div class="gap-40"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/case-single.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:26 GMT -->
</html>