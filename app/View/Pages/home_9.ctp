<body>

   <div class="body-inner">

      <?php echo $this->element('header_9'); ?>

      <div id="box-slide" class="owl-carousel owl-theme page-slider mrt-110">
         <div class="item" style="background-image:url(images/slider/bg4.jpg)">
            <div class="container">
               <div class="box-slider-content">
                  <div class="box-slider-text animated">
                     <h2 class="box-slide-title">27 Years Young</h2>
                     <h3 class="box-slide-sub-title">To Change Your World</h3>
                     <p class="box-slide-description">You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent
                        like.
                     </p>
                     <p>
                        <?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                     </p>
                  </div>
               </div>
            </div>

         </div>
         <!-- Item 1 end -->

         <div class="item" style="background-image:url(images/slider/bg5.jpg)">
            <div class="container">
               <div class="box-slider-content">
                  <div class="box-slider-text animated">
                     <h2 class="box-slide-title">We are the Leader</h2>
                     <h3 class="box-slide-sub-title">In Finance Industry</h3>
                     <p class="box-slide-description">You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent
                        like.
                     </p>
                     <p>
                        <?php echo $this->Html->link(
                                    'Know Us',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <!-- Item 2 end -->

         <div class="item" style="background-image:url(images/slider/bg6.jpg)">
            <div class="container">
               <div class="box-slider-content">
                  <div class="box-slider-text animated">
                     <h2 class="box-slide-title">To Reach more People</h2>
                     <h3 class="box-slide-sub-title">We Care our Customers</h3>
                     <p class="box-slide-description">You have ideas, goals, and dreams. We have a culturally diverse, forward thinking team looking for talent
                        like.
                     </p>
                     <p>
                        <?php echo $this->Html->link(
                                    'Get a Quote',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                     </p>
                  </div>
               </div>
            </div>
         </div>
         <!-- Item 3 end -->
      </div>
      <!-- Box owl carousel end-->

      <section class="ts-features-col patb-100">
         <div class="container">
            <div class="row">
               <div class="col-lg-4">
                  <div class="feature-text-box">
                     <h2 class="column-title"><span>Our best Features</span>We are the best in Finance
                     </h2>
                  </div>
                  <p>
                     A business strategy is the means by which it sets out to achieve its desired ends. You have ideas, goals, and dreams
                  </p>
                  <?php echo $this->Html->link(
                          'Read More',
                          array('controller' => 'features', 'action' => 'feature4'),
                          array('class' => 'btn btn-primary')); ?>
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-feature text-center box-primary">
                     <div class="ts-feature-info"><i class="icon icon-chart2"></i>
                        <h3 class="ts-feature-title">Marketing growth</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks
                           you have ideas</p>
                     </div>
                  </div>
                  <!-- feature 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-feature text-center box-secondary">
                     <div class="ts-feature-info"><i class="icon icon-clock3"></i>
                        <h3 class="ts-feature-title">On time services</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks
                           you have ideas</p>
                     </div>
                  </div>
                  <!-- feature 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </section>

      <section class="ts-services solid-bg" id="ts-services">
         <div class="container">
            <div class="row text-left">
               <div class="col-lg-12">
                  <h2 class="section-title border-title-left">Our Best Services <span class="section-title-tagline title-light">What We Do</span></h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-12">
                  <div class="featured-tab">
                     <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"><a class="nav-link active animated fadeIn" href="#tab_one" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-pie-chart2"></i></span><span class="tab-text-title">Best Consulting</span></span></a></li>
                        <!-- list 1 end-->
                        <li class="nav-item"><a class="nav-link animated fadeIn" href="#tab_two" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-loan"></i></span><span class="tab-text-title">Market Growth</span></span></a></li>
                        <!-- list 2 end-->
                        <li class="nav-item"><a class="nav-link animated fadeIn" href="#tab_three" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-savings"></i></span><span class="tab-text-title">Saving Strategy</span></span></a></li>
                        <!-- list 3 end-->
                        <li class="nav-item"><a class="nav-link animated fadeIn" href="#tab_four" data-toggle="tab"><span class="tab-head"><span><i class="icon icon-consult"></i></span><span class="tab-text-title">Best Consulting</span></span></a></li>
                        <!-- list 4 end-->
                     </ul>
                     <!-- Nav-tabs end-->
                     <div class="tab-content">
                        <div class="tab-pane active animated fadeInRight" id="tab_one">
                           <div class="row">
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle" style="background-image:url(images/tabs/tab-shape.png);">
                                     <?php echo $this->Html->image('/images/tabs/img1.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">Solution for your Financial needs</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                    <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 1 end-->
                        <div class="tab-pane animated fadeInRight" id="tab_two">
                           <div class="row">
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">We provide loan to Financial Needs</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                           <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle align-self-center" style="background-image:url(images/tabs/tab-shape.png);">
                                    <?php echo $this->Html->image('/images/tabs/img2.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 2 end-->
                        <div class="tab-pane animated fadeInRight" id="tab_three">
                           <div class="row">
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle" style="background-image:url(images/tabs/tab-shape.png);">
                                    <?php echo $this->Html->image('/images/tabs/img3.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">Register for online invoicing</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                    <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 3 end-->
                        <div class="tab-pane animated fadeInRight" id="tab_four">
                           <div class="row">
                              <div class="col-lg-8">
                                 <div class="tab-content-info">
                                    <h3 class="tab-content-title">We Provide Insurance Solutions</h3>
                                    <p>Eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                       quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                       Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                                       nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                                       officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus
                                       error sit voluptatem accusantium doloremque laudantium.</p>
                                    <?php echo $this->Html->link(
                                            'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                            array('controller' => 'features', 'action' => 'feature4'),
                                            array('class' => 'btn-light', 'escape' => false)); ?>
                                 </div>
                                 <!-- Tab content info end-->
                              </div>
                              <!-- Col end-->
                              <div class="col-lg-4 align-self-center">
                                 <div class="bg-contain-verticle" style="background-image:url(images/tabs/tab-shape.png);">
                                    <?php echo $this->Html->image('/images/tabs/img4.png',
                                    ['alt' => ''],
                                    ['class' => 'img-center img-fluid']); ?>
                                 </div>
                              </div>
                              <!-- Col end-->
                           </div>
                           <!-- Row end-->
                        </div>
                        <!-- Tab pane 3 end-->
                     </div>
                     <!-- tab content-->
                     <div class="gap-60"></div>
                     <div class="text-center">
                         <?php echo $this->Html->link(
                                 'Check More Services',
                                 ['controller' => 'services', 'action' => 'service1'],
                                 ['class' => 'btn btn-primary']); ?>
                     </div>
                  </div>
                  <!-- Schedule tab end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Service end-->

      <section id="call-to-action" class="call-to-action-bg bg-overlay">
         <div class="container">
            <div class="row">
               <div class="col-lg-8 align-self-center">
                  <h3 class="call-to-action-title">We Offer Financial Strategies &amp; Superior Services</h3>
                  <p>
                     Our mission is to provide quality guidance, build relationships of
                     <br> trust, and develop innovative solutions
                  </p>
               </div>
               <div class="col-lg-4 text-right">
                  <?php echo $this->Html->link(
                                 'Get Free Quote',
                                 ['controller' => 'services', 'action' => 'service1'],
                                 ['class' => 'btn btn-primary']); ?>
               </div>
            </div>
         </div>
      </section>

      <section id="ts-team" class="ts-team">
         <div class="container">
            <div class="row text-center">
               <div class="col-lg-12">
                  <h2 class="section-title"><span>Our People</span>Best Team</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                         <?php echo $this->Html->image('/images/team/team1.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info team-list-border">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 1 end-->
                  </div>
                  <!-- Team classic 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>>
                     </div>
                     <div class="ts-team-info team-list-border">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic">
                           <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                        </div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 2 end-->
                  </div>
                  <!-- Team classic 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-team-classic">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team3.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-responsive']); ?>
                     </div>
                     <div class="ts-team-info team-list-border">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                        <p>Eradicate invest honesty human rights accessibility theory of social change. Diversity experience
                           in .</p>
                        <div class="team-social-classic"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team info 3 end-->
                  </div>
                  <!-- Team classic 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
      </section>

      <section class="clients-area" id="clients-area">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 owl-carousel owl-theme text-center partners" id="partners-carousel">
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client1.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client2.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client3.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client4.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client5.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client6.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client7.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
               </div>
               <!-- Owl carousel end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Partners end-->

      <div class="map" id="map"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/index-9.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:03 GMT -->
</html>