<div class="page-content">
    <div class="container-fluid">
        <section class="box-typical contacts-page">
            <header class="box-typical-header box-typical-header-bordered">
                <div class="tbl-row">
                    <div class="tbl-cell tbl-cell-title">
                        <h1>Tribo Propaganda:</h1>
                        <ul class="contacts-tabs" role="tablist">
                            <li class="nav-item">
                                <a href="#jales" role="tab" data-toggle="tab" class="active">Jales</a>
                            </li>
                            <li class="nav-item">
                                <a href="#fernandopolis" role="tab" data-toggle="tab">Fernandópolis</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tbl-cell tbl-cell-actions">
                        <button type="button" class="action-btn action-btn-expand">
                            <i class="font-icon font-icon-expand"></i>
                        </button>
                    </div>
                </div>
            </header>

            <div class="tab-content">
                <div role="tabpanel" class="clearfix tab-pane active" id="jales">
                    <div class="contacts-page-col-left">
                        <div class="contacts-page-col-left-in">
                            <div id="map_canvas" class="map"></div>
                        </div>
                    </div>
                    <div class="contacts-page-col-right">
                        <section class="contacts-page-section">
                            <header class="box-typical-header-sm">Informações de Contato</header>
                            <p class="contact-other">
                                <i class="font-icon font-icon-earth-bordered"></i>
                                <a href="http://www.tribopropaganda.com/" target="_blank">www.tribopropaganda.com</a>
                            </p>
                            <p class="contact-other">
                                <i class="font-icon font-icon-mail"></i>
                                <a href="mailto:tribodp@gmail.com" target="_blank">tribodp@gmail.com</a>
                            </p>
                            <p class="contact-other">
                                <i class="font-icon font-icon-phone"></i>
                                <a href="tel:+551736211752">(17) 3621 1752</a>
                            </p>
                            <p class="contact-other">
                                <i class="font-icon fa fa-whatsapp"></i>
                                <a href="tel:+5517997058799">(17) 99705 8799</a>
                            </p>
                        </section>
                        <?php echo $this->Form->create(false, ['url' => ['#' => 'jales'], 'class' => 'contacts-page-section']); ?>
                            <header class="box-typical-header-sm">Formulário de Contato</header>
                            <div class="form-group<?php if (isset($jales['name']) && empty($jales['name'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <input type="text" class="form-control" name="jales[name]" value="<?php echo $jales['name']; ?>" placeholder="Nome"/>
                                    <?php if (isset($jales['name']) && empty($jales['name'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer um nome.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group<?php if (isset($jales['email']) && empty($jales['email'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <input type="email" class="form-control" name="jales[email]" value="<?php echo $jales['email']; ?>" placeholder="E-mail"/>
                                    <?php if (isset($jales['email']) && empty($jales['email'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer um e-mail.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group<?php if (isset($jales['subject']) && empty($jales['subject'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <input type="text" class="form-control" name="jales[subject]" value="<?php echo $jales['subject']; ?>" placeholder="Assunto"/>
                                    <?php if (isset($jales['subject']) && empty($jales['subject'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer um assunto.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group<?php if (isset($jales['message']) && empty($jales['message'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <textarea rows="4" class="form-control" name="jales[message]" placeholder="Mensagem"><?php echo $jales['message']; ?></textarea>
                                    <?php if (isset($jales['message']) && empty($jales['message'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer uma mensagem.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-inline btn-primary ladda-button" data-style="zoom-in">
                                <span class="ladda-label">Enviar</span>
                            </button>
                        <?php echo $this->Form->end(); ?>
                        <section class="contacts-page-section">
                            <header class="box-typical-header-sm">Redes Sociais</header>
                            <a href="https://www.facebook.com/tribopp/" class="contact" target="_blank">
                                <i class="font-icon font-icon-facebook"></i>
                            </a>
                            <a href="https://www.instagram.com/tribopropaganda/" class="contact" target="_blank">
                                <i class="font-icon font-icon-instagram"></i>
                            </a>
                        </section>
                    </div>
                </div>

                <div role="tabpanel" class="clearfix tab-pane" id="fernandopolis">
                    <div class="contacts-page-col-left">
                        <div class="contacts-page-col-left-in">
                            <div id="map_canvas_2" class="map"></div>
                        </div>
                    </div>
                    <div class="contacts-page-col-right">
                        <section class="contacts-page-section">
                            <header class="box-typical-header-sm">Informações de Contato</header>
                            <p class="contact-other">
                                <i class="font-icon font-icon-earth-bordered"></i>
                                <a href="http://www.tribopropaganda.com/" target="_blank">www.tribopropaganda.com</a>
                            </p>
                            <p class="contact-other">
                                <i class="font-icon font-icon-mail"></i>
                                <a href="mailto:tribofernandopolis@gmail.com" target="_blank">tribofernandopolis@gmail.com</a>
                            </p>
                            <p class="contact-other">
                                <i class="font-icon font-icon-phone"></i>
                                <a href="tel:+5517996656621">(17) 99665 6621</a>
                            </p>
                        </section>
                        <?php echo $this->Form->create(false, ['url' => ['#' => 'fernandopolis'], 'class' => 'contacts-page-section']); ?>
                            <header class="box-typical-header-sm">Formulário de Contato</header>
                            <div class="form-group<?php if (isset($fernandopolis['name']) && empty($fernandopolis['name'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <input type="text" class="form-control" name="fernandopolis[name]" value="<?php echo $fernandopolis['name']; ?>" placeholder="Nome"/>
                                    <?php if (isset($fernandopolis['name']) && empty($fernandopolis['name'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer um nome.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group<?php if (isset($fernandopolis['email']) && empty($fernandopolis['email'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <input type="email" class="form-control" name="fernandopolis[email]" value="<?php echo $fernandopolis['email']; ?>" placeholder="E-mail"/>
                                    <?php if (isset($fernandopolis['email']) && empty($fernandopolis['email'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer um e-mail.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group<?php if (isset($fernandopolis['subject']) && empty($fernandopolis['subject'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <input type="text" class="form-control" name="fernandopolis[subject]" value="<?php echo $fernandopolis['subject']; ?>" placeholder="Assunto"/>
                                    <?php if (isset($fernandopolis['subject']) && empty($fernandopolis['subject'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer um assunto.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="form-group<?php if (isset($fernandopolis['message']) && empty($fernandopolis['message'])) echo ' error'; ?>">
                                <div class="form-control-wrapper fl-flex-label">
                                    <textarea rows="4" class="form-control" name="fernandopolis[message]" placeholder="Mensagem"><?php echo $fernandopolis['message']; ?></textarea>
                                    <?php if (isset($fernandopolis['message']) && empty($fernandopolis['message'])): ?>
                                    <div class="form-tooltip-error"><ul><li>É obrigatório fornecer uma mensagem.</li></ul></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-inline btn-primary ladda-button" data-style="zoom-in">
                                <span class="ladda-label">Enviar</span>
                            </button>
                        <?php echo $this->Form->end(); ?>
                        <section class="contacts-page-section">
                            <header class="box-typical-header-sm">Redes Sociais</header>
                            <a href="https://www.facebook.com/tribopp/" class="contact" target="_blank">
                                <i class="font-icon font-icon-facebook"></i>
                            </a>
                            <a href="https://www.instagram.com/tribopropaganda/" class="contact" target="_blank">
                                <i class="font-icon font-icon-instagram"></i>
                            </a>
                        </section>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('.fl-flex-label').flexLabel();
    });
</script>
