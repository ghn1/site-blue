<div class="login">
    <div class="logotipo"></div>
    <?php echo $this->Form->create('User', ['class' => 'sign-box', 'inputDefaults' => ['label' => false, 'div' => false]]); ?>
        <header class="sign-title">Login</header>
        <div class="form-group">
            <div class="form-control-wrapper fl-flex-label">
                <?php echo $this->Form->input('email', ['type' => 'text', 'class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => __('E-mail'), 'maxlength' => 100, 'required' => false]); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="form-control-wrapper fl-flex-label">
                <?php echo $this->Form->input('password', ['class' => 'form-control', 'autocomplete' => 'off', 'placeholder' => __('Senha'), 'required' => false]); ?>
            </div>
        </div>
        <div class="form-group">
            <div class="checkbox float-left">
                <?php echo $this->Form->input('remember', ['type' => 'checkbox', 'id' => 'signed-in', 'value' => '1']); ?>
                <label for="signed-in"><?php echo __('Mantenha-me conectado'); ?></label>
            </div>
        </div>
        <?php echo $this->Form->button(__('Entrar'), ['class' => 'btn btn-primary ladda-button', 'data-style' => 'zoom-in']); ?>
    <?php echo $this->Form->end(); ?>
    <div class="copyright">Copyright &copy; <?php echo date('Y'); ?> Tribo Propaganda. All rights reserved.</div>
</div>
<script type="text/javascript">
    $(function(){
        $('.fl-flex-label').flexLabel();
        $('#UserEmail').focus();
        $(window).resize(function(){
            if ($(window).height() < $(".login").height()) {
                if (!$("body").hasClass("absolute"))
                    $("body").addClass("absolute");
            }
            else {
                if ($("body").hasClass("absolute"))
                    $("body").removeClass("absolute");
            }
        });
    });
</script>