<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3><?php echo __('Usuários'); ?></h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li class="active"><?php echo __('Planilha de Registros'); ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>
        <section class="box-typical">
            <div class="table-responsive">
                <div class="bootstrap-table">
                    <div class="fixed-table-toolbar">
                        <div class="bars pull-left">
                            <div id="toolbar">
                                <?php echo $this->Html->link($this->Html->tag('i', '', ['class' => 'font-icon font-icon-page']) . __(' Novo'), ['action' => 'form'], ['class' => 'btn insert', 'escape' => false]); ?>
                                <?php echo $this->Form->button($this->Html->tag('i', '', ['class' => 'font-icon font-icon-trash']) . __(' Excluir'), ['class' => 'btn btn-danger remove', 'disabled' => true, 'type' => false]); ?>
                            </div>
                        </div>
                        <div class="pull-right search">
                            <?php
                            echo $this->Form->create(false, ['url' => $named, 'inputDefaults' => ['div' => false, 'label' => false]]);
                            echo $this->Form->input('search', ['class' => 'form-control pull-left', 'value' => $search]);
                            echo $this->Form->button(__('Pesquisar'), ['type' => 'submit', 'class' => 'btn btn-sm pull-right']);
                            echo $this->Form->end();
                            ?>
                        </div>
                    </div>
                    <div class="fixed-table-container" style="padding-bottom: 0px;">
                        <div class="fixed-table-body" style="overflow: visible;">
                            <table id="table" class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th class="bs-checkbox text-center">
                                            <div class="th-inner ">
                                                <div class="checkbox checkbox-only">
                                                    <input id="datatable-chekbox-select-all" name="btSelectAll" type="checkbox">
                                                    <label for="datatable-chekbox-select-all"></label>
                                                </div>
                                            </div>
                                            <div class="fht-cell"></div>
                                        </th>
                                        <th class="text-center" onclick="location = '<?php echo $this->Html->url(array_merge($named, ['sort' => 'name', 'direction' => ($named['sort'] == 'name' && $named['direction'] == 'asc') ? 'desc' : 'asc'])); ?>';">
                                            <?php echo $this->Html->div('th-inner sortable both ' . (($named['sort'] == 'name') ? $named['direction'] : ''), __('Nome')); ?>
                                        </th>
                                        <th class="text-center" onclick="location = '<?php echo $this->Html->url(array_merge($named, ['sort' => 'email', 'direction' => ($named['sort'] == 'email' && $named['direction'] == 'asc') ? 'desc' : 'asc'])); ?>';">
                                            <?php echo $this->Html->div('th-inner sortable both ' . (($named['sort'] == 'email') ? $named['direction'] : ''), __('E-mail')); ?>
                                        </th>
                                        <th class="action text-center">
                                            <?php echo $this->Html->div('th-inner', __('Ação')); ?>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($data)):
                                        foreach ($data as $key => $row):
                                    ?>
                                    <tr data-index="<?php echo $row['User']['id'] ?>">
                                        <td class="bs-checkbox ">
                                            <div class="checkbox checkbox-only">
                                                <input type="checkbox" id="datatable-checkbox-<?php echo $key; ?>" name="btSelectItem">
                                                <label for="datatable-checkbox-<?php echo $key; ?>"></label>
                                            </div>
                                        </td>
                                        <td class="text-left"><?php echo $row['User']['name']; ?></td>
                                        <td class="text-left"><?php echo $row['User']['email']; ?></td>
                                        <td class="action text-center">
                                            <?php echo $this->Html->link($this->Html->tag('i', '', ['class' => 'glyphicon glyphicon-pencil']), ['action' => 'form', 'id' => $row['User']['id']], ['class' => 'detail-icon edit', 'title' => __('Editar'), 'escape' => false]); ?>
                                            <?php echo $this->Html->tag('a', $this->Html->tag('i', '', ['class' => 'glyphicon glyphicon-trash']), ['class' => 'detail-icon remove', 'title' => __('Excluir'), 'escape' => false]); ?>
                                        </td>
                                    </tr>
                                    <?php
                                        endforeach;
                                    else:
                                    ?>
                                    <tr class="no-records-found" style="display: table-row;">
                                        <td colspan="4"><?php echo __('Nenhum registro correspondente encontrado'); ?></td>
                                    </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        <?php if (!empty($data)): ?>
                        <div class="fixed-table-pagination">
                            <div class="pull-left pagination-detail">
                                <?php echo $this->Html->tag('span', $this->Paginator->counter(__('Mostrando {:start} a {:end} de {:count} registros')), ['class' => 'pagination-info']); ?>
                                <span class="page-list">
                                    <span class="btn-group dropup">
                                        <button type="button" class="btn btn-default  dropdown-toggle" data-toggle="dropdown">
                                            <span class="page-size"><?php echo isset($named['limit']) ? $named['limit'] : 10; ?></span>
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="<?php echo $this->Html->url(array_merge($named, ['limit' => 10, 'page' => null])); ?>">10</a></li>
                                            <li><a href="<?php echo $this->Html->url(array_merge($named, ['limit' => 25, 'page' => null])); ?>">25</a></li>
                                            <li><a href="<?php echo $this->Html->url(array_merge($named, ['limit' => 50, 'page' => null])); ?>">50</a></li>
                                        </ul>
                                    </span>
                                    <?php echo __('registros por página'); ?>
                                </span>
                            </div>
                            <div class="pull-right pagination">
                                <ul class="pagination">
                                    <?php
                                    if ($this->Paginator->hasPrev())
                                        echo $this->Paginator->prev($this->Html->tag('i', '', ['class' => 'font-icon font-icon-arrow-left']), ['tag' => 'li', 'class' => 'page-pre', 'escape' => false]);
                                    echo $this->Paginator->numbers(['tag' => 'li', 'class' => 'page-number', 'currentTag' => 'a', 'currentClass' => 'active', 'separator' => '', 'modulus' => 4]);
                                    if ($this->Paginator->hasNext())
                                        echo $this->Paginator->next($this->Html->tag('i', '', ['class' => 'font-icon font-icon-arrow-right']), ['tag' => 'li', 'class' => 'page-next', 'escape' => false]);
                                    ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </section>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $("#datatable-chekbox-select-all").on("change", function(){
            var checked = $(this).prop("checked");
            $("#table tbody .bs-checkbox input[name='btSelectItem']:not(:disabled)")
                    .prop("checked", checked).trigger("change");
        });
        $("#table tbody .bs-checkbox input[name='btSelectItem']").on("change", function(){
            var lchecked = $("#table tbody .bs-checkbox input[name='btSelectItem']:checked").length;
            $("#toolbar .btn.remove").prop("disabled", !lchecked);
            $("#datatable-chekbox-select-all").prop("checked", lchecked);
            if ($(this).is(":checked"))
                $(this).parents("tr").addClass("selected");
            else
                $(this).parents("tr").removeClass("selected");
        });
        $("#toolbar .btn.remove, #table a.remove").on('click', function(){
            var data = "";
            if ($(this).is("button")) {
                var selected = $("#table tbody .bs-checkbox input[name='btSelectItem']:checked");
                if (selected.length) {
                    for (var x = 0; x < selected.length; x++)
                        data += "id[]=" + $(selected[x]).parents('tr').data('index') + "&";
                }
            } else
                data = "id=" + $(this).parents("tr").data('index');
            swal({
                title: "Atenção",
                text: "<?php echo __('Deseja realmente excluir?'); ?>",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "<?php echo __('Sim, excluí-lo!'); ?>",
                cancelButtonText: "<?php echo __('Cancelar'); ?>",
                closeOnConfirm: false,
                showLoaderOnConfirm: true
            }, function() {
                $.ajax({
                    url: "<?php echo $this->Html->url(['action' => 'delete']); ?>",
                    data: data,
                    type: "post",
                    success: function() {
                        swal({
                            title: "<?php echo __('Excluído!'); ?>",
                            text: "<?php echo __('Foi excluido com sucesso.'); ?>",
                            type: "success",
                            showConfirmButton: false,
                            timer: 1000
                        });
                        setTimeout(function(){
                            location.reload();
                        }, 800);
                    },
                    error: function() {
                        swal({
                            title: "<?php echo __('Erro!'); ?>",
                            text: "<?php echo __('Entre em contato com o suporte.'); ?>",
                            type: "error",
                            showConfirmButton: false
                        });
                    }
                });
            });
        });
        $("ul.pagination li a").on("click", function(){
            $('.bootstrap-table').block({
                message: '<div class="blockui-default-message"><i class="fa fa-circle-o-notch fa-spin"></i><h6><?php echo __('Carregando...<br>Por favor, aguarde!'); ?></h6></div>',
                overlayCSS: {background: 'rgba(142, 159, 167, 0.8)', opacity: 1, cursor: 'wait'},
                css: {width: '50%'},
                blockMsgClass: 'block-msg-default'
            });
        });
    });
</script>
