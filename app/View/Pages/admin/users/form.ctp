<div class="page-content">
    <div class="container-fluid">
        <header class="section-header">
            <div class="tbl">
                <div class="tbl-row">
                    <div class="tbl-cell">
                        <h3><?php echo __('Usuários'); ?></h3>
                        <ol class="breadcrumb breadcrumb-simple">
                            <li class="active"><?php echo __('Formulário de Cadastro'); ?></li>
                        </ol>
                    </div>
                </div>
            </div>
        </header>
        <?php echo $this->Form->create('User', ['id' => 'register', 'inputDefaults' => ['autocomplete' => 'off', 'class' => 'form-control', 'label' => false, 'required' => false]]); ?>
            <div class="card">
                <div class="card-block">
                    <?php echo $this->Form->input('id'); ?>
                    <?php echo $this->Form->input('picture', ['type' => 'hidden']); ?>
                    <div class="row">
                        <div class="photo col-sm-6">
                            <div class="form-group">
                                <label class="form-label">Foto</label>
                                <div class="form-control-wrapper">
                                    <div id="profile-picture"<?php if (!empty($this->request->data['User']['picture'])): ?> class="uploaded"<?php endif; ?>>
                                        <?php echo $this->Html->image(empty($this->request->data['User']['picture']) ? 'user/empty/picture.png' : $this->request->data['User']['picture'], ['alt' => 'Avatar']); ?>
                                        <a class="file-btn add">
                                            <span><i class="glyphicon glyphicon-camera"></i><br/><?php echo __('Adicionar Foto de Usuário'); ?></span>
                                            <input type="file" id="upload" value="Choose a file" accept="image/*">
                                        </a>
                                        <div class="file-btn dropdown">
                                            <a class="dropdown-toggle" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="glyphicon glyphicon-camera"></i><br/><?php echo __('Alterar Foto de Usuário'); ?>
                                            </a>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <a class="dropdown-item upload" href="javascript:;"><?php echo __('Carregar Foto'); ?></a>
                                                <a class="dropdown-item remove" href="javascript:;"><?php echo __('Remover Foto'); ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="data col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php
                                        echo $this->Form->label('name', __('Nome'), ['class' => 'form-label']);
                                        echo $this->Form->input('name', ['div' => ['class' => 'form-control-wrapper'], 'error' => [
                                            'attributes' => ['class' => 'form-tooltip-error'],
                                            'required' => __('Campo obrigatório'),
                                            'minlength' => __('Deve ter no mínimo 4 caracteres.')
                                        ]]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php
                                        echo $this->Form->label('email', __('E-mail'), ['class' => 'form-label']);
                                        echo $this->Form->input('email', ['div' => ['class' => 'form-control-wrapper'], 'type' => 'text', 'error' => [
                                            'attributes' => ['class' => 'form-tooltip-error'],
                                            'required' => __('Campo obrigatório'),
                                            'validate' => __('E-mail inválido'),
                                            'unique' => __('E-mail já usado com outro usuário')
                                        ]]);
                                        ?>
                                    </div>
                                </div>
                                <div class=" col-sm-6">
                                    <div class="form-group">
                                        <?php
                                        echo $this->Form->label('password', __('Senha'), ['class' => 'form-label']);
                                        echo $this->Form->input('password', ['type' => 'password', 'div' => ['class' => 'form-control-wrapper'], 'error' => [
                                            'attributes' => ['class' => 'form-tooltip-error'],
                                            'required' => __('Campo obrigatório')
                                        ]]);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <?php
                                        echo $this->Form->label('password2', __('Confirme a Senha'), ['class' => 'form-label']);
                                        echo $this->Form->input('password2', ['type' => 'password', 'div' => ['class' => 'form-control-wrapper'], 'error' => [
                                            'attributes' => ['class' => 'form-tooltip-error'],
                                            'pconfirm' => __('As senhas digitadas não coincidem')
                                        ]]);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bar-buttons">
                <?php echo $this->Form->button(__('Salvar'), ['class' => 'btn btn-inline btn-primary ladda-button', 'data-style' => 'zoom-in']); ?>
                <?php echo $this->Html->link(__('Cancelar'), ['action' => 'index'], ['class' => 'btn btn-inline btn-secondary']); ?>
            </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        $('#upload').on('change', function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                var $uploadCrop;
                reader.onload = function (e) {
                    $uploadCrop.croppie('bind', {url: e.target.result});
                };
                reader.readAsDataURL(this.files[0]);
                swal({
                    title: '',
                    html: true,
                    text: '<div id="upload-crop" style="padding: 0;"></div>',
                    allowOutsideClick: true
                }, function(){
                    $uploadCrop.croppie('result', {
                        type: 'canvas',
                        size: {width: 128, height: 128, type: 'circle'}
                    }).then(function (resp) {
                        $("#UserPicture").val(resp);
                        $("#profile-picture img").attr("src", resp);
                        $('#profile-picture').addClass('uploaded');
                    });
                });
                $uploadCrop = $('#upload-crop').croppie({
                    viewport: {width: 384, height: 384, type: 'circle'},
                    boundary: {width: 384, height: 384},
                    exif: true
                });
                setTimeout(function () {
                    $('.sweet-alert').css('margin', function () {
                        var top = -1 * ($(this).height() / 2);
                        var left = -1 * ($(this).width() / 2);
                        return top + 'px 0 0 ' + left + 'px';
                    });
                }, 1);
            }
        });
        $("#profile-picture .dropdown .dropdown-item").click(function(){
            if ($(this).is(".upload"))
                $("#upload").trigger("click");
            else {
                $("#profile-picture img").attr("src", "<?php echo $this->Html->url('/img/user/empty/picture.png'); ?>");
                $('#profile-picture').removeClass('uploaded');
                $("#UserPicture").val("delete");
            }
        });
        $('#UserPassword').password();
        $('#UserPassword2').password();
    });
</script>
