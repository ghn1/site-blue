<body>

   <div class="body-inner">

      <?php echo $this->element('header_5'); ?>

      <div class="carousel slide" id="main-slide" data-ride="carousel">
         <!-- Indicators-->
         <ol class="carousel-indicators visible-lg visible-md">
            <li class="active" data-target="#main-slide" data-slide-to="0"></li>
            <li data-target="#main-slide" data-slide-to="1"></li>
            <li data-target="#main-slide" data-slide-to="2"></li>
         </ol>
         <!-- Indicators end-->
         <!-- Carousel inner-->
         <div class="carousel-inner">
            <div class="carousel-item active" style="background-image:url(images/slider/bg1.jpg);">
               <div class="container">
                  <div class="slider-content text-left">
                     <div class="col-md-12">
                        <h2 class="slide-title title-light">Your future is created by</h2>
                        <h3 class="slide-sub-title">What you do today</h3>
                        <p class="slider-description lead">Nobody’s more committed to connecting you with the exceptional <br> top talents with the right fit
                           for your business than us.</p>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
            <div class="carousel-item" style="background-image:url(images/slider/bg2.jpg);">
               <div class="container">
                  <div class="slider-content text-center">
                     <div class="col-md-12">
                        <h3 class="slide-sub-title">We provide solutions to<br>grow your business</h3>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
            <div class="carousel-item" style="background-image:url(images/slider/bg3.jpg);">
               <div class="container">
                  <div class="slider-content text-right">
                     <div class="col-md-12">
                        <h2 class="slide-title title-light">Your future is created by</h2>
                        <h3 class="slide-sub-title">We care about your Business</h3>
                        <p class="slider-description lead">Nobody’s more committed to connecting you with the exceptional <br> top talents with the right fit
                           for your business than us.</p>
                        <p><?php echo $this->Html->link(
                                    'Our Services',
                                    ['controller' => 'services', 'action' => 'service1'],
                                    ['class' => 'slider btn btn-primary']); ?>
                            <?php echo $this->Html->link(
                                    'Contact Us',
                                    ['controller' => 'contacts', 'action' => 'contact1'],
                                    ['class' => 'slider btn btn-border']); ?>
                        </p>
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- Slider content end-->
               </div>
               <!-- Container end-->
            </div>
            <!-- Carousel item 1 end-->
         </div>
         <!-- Carousel inner end-->
         <!-- Controllers--><a class="left carousel-control carousel-control-prev" href="#main-slide" data-slide="prev"><span><i class="fa fa-angle-left"></i></span></a>
         <a class="right carousel-control carousel-control-next" href="#main-slide" data-slide="next"><span><i class="fa fa-angle-right"></i></span></a>
      </div>
      <!-- Carousel end-->

      <section class="ts-intro" id="ts-intro">
         <div class="container">
            <div class="row">
               <div class="col-md-4">
                  <h2 class="column-title"><span>Who we are</span>Providing services and happiness</h2>
               </div>
               <div class="col-md-8 border-left">
                  <div class="intro-details">
                     <p class="lead">
                        Invest in your drivers, improve their safety and reduce the risk of incidents with our compre hensive courses and seminars
                        tailored to the needs of your organisation. With a choice of full or half-day sessions, all are flexible
                        to fit with.
                     </p>
                     <p><?php echo $this->Html->link(
                             'Read More',
                             array('controller' => 'features', 'action' => 'feature4'),
                             array('class' => 'btn btn-primary')); ?>
                     </p>
                  </div>

               </div>
            </div>
         </div>
         <!-- Container end-->
      </section>
      <!-- Service end-->

      <section class="ts-features-col">
         <div class="container">
            <div class="row">
               <div class="col-lg-4">
                  <div class="ts-feature text-center box-primary">
                     <div class="ts-feature-info"><i class="icon icon-consut2"></i>
                        <h3 class="ts-feature-title">Best Consulting</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas</p>
                     </div>
                  </div>
                  <!-- feature 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-feature text-center box-secondary">
                     <div class="ts-feature-info"><i class="icon icon-chart2"></i>
                        <h3 class="ts-feature-title">Marketing growth</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas</p>
                     </div>
                  </div>
                  <!-- feature 2 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-4">
                  <div class="ts-feature text-center box-dark">
                     <div class="ts-feature-info"><i class="icon icon-clock3"></i>
                        <h3 class="ts-feature-title">On time services</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, such as stocks.
                           You have ideas</p>
                     </div>
                  </div>
                  <!-- feature 3 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </section>

      <section class="ts-services solid-bg" id="ts-services">
         <div class="container">
            <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title"><span>Our Services</span>What We Do</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                         <?php echo $this->Html->image('/images/services/service1.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-pie-chart2"></i>
                        </span>
                        <h3 class="service-title">Financial Planning</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service1 end-->
               </div>
               <!-- Col 1 end-->
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service2.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-tax"></i>
                        </span>
                        <h3 class="service-title">Tax Planning</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service2 end-->
               </div>
               <!-- Col 2 end-->
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service3.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-savings"></i>
                        </span>
                        <h3 class="service-title">Saving Strategy</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service3 end-->
               </div>
               <!-- Col 3 end-->
            </div>
            <!-- Content 1 row end-->
            <div class="gap-60"></div>
            <div class="row">
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service4.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-mutual-fund"></i>
                        </span>
                        <h3 class="service-title">Mutual Funds</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service4 end-->
               </div>
               <!-- Col 4 end-->
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service5.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-loan"></i>
                        </span>
                        <h3 class="service-title">Business Loan</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service5 end-->
               </div>
               <!-- Col 5 end-->
               <div class="col-lg-4 col-md-12">
                  <div class="ts-service-box">
                     <div class="ts-service-image-wrapper">
                        <?php echo $this->Html->image('/images/services/service6.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-service-content">
                        <span class="ts-service-icon">
                           <i class="icon icon-consult"></i>
                        </span>
                        <h3 class="service-title">Insurance Consulting</h3>
                        <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                           tea such as stocks.</p>
                        <p><?php echo $this->Html->link(
                                'Read More' . $this->Html->tag('i', '', array('class' => 'icon icon-right-arrow2')),
                                array('controller' => 'features', 'action' => 'feature4'),
                                array('class' => 'link-more', 'escape' => false)); ?>
                        </p>
                     </div>
                  </div>
                  <!-- Service6 end-->
               </div>
               <!-- Col 6 end-->
            </div>
            <!-- Content Row 2 end-->
         </div>
         <!-- Container end-->
      </section>

      <section id="ts-facts-area" class="ts-facts-area-bg bg-overlay">
         <div class="container">
            <div class="row facts-wrapper text-center">
               <div class="col-lg-3 col-md-3">
                  <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-money-1"></i></span>
                     <div class="ts-facts-content">
                        <h4 class="ts-facts-num"><span class="counterUp">2435</span></h4>
                        <p class="facts-desc">Cases Completed</p>
                     </div>
                  </div>
                  <!-- Facts 1 end-->
               </div>
               <!-- Col 1 end-->
               <div class="col-lg-3 col-md-3">
                  <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-invest"></i></span>
                     <div class="ts-facts-content">
                        <h4 class="ts-facts-num"><span class="counterUp">467</span></h4>
                        <p class="facts-desc">Successful Investment</p>
                     </div>
                  </div>
                  <!-- Facts 2 end-->
               </div>
               <!-- Col 2 end-->
               <div class="col-lg-3 col-md-3">
                  <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-chart2"></i></span>
                     <div class="ts-facts-content">
                        <h4 class="ts-facts-num"><span class="counterUp">85</span>%</h4>
                        <p class="facts-desc">Business Growth </p>
                     </div>
                  </div>
                  <!-- Facts 3 end-->
               </div>
               <!-- Col 3 end-->
               <div class="col-lg-3 col-md-3">
                  <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-deal"></i></span>
                     <div class="ts-facts-content">
                        <h4 class="ts-facts-num"><span class="counterUp">139</span></h4>
                        <p class="facts-desc">Running Projects</p>
                     </div>
                  </div>
                  <!-- Facts 4 end-->
               </div>
               <!-- Col 4 end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container 2 end-->
      </section>

      <section class="ts-team">
         <div class="container">
            <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title"><span>Our People</span>Best Team</h2>
               </div>
            </div>
            <!-- Title row end-->
            <div class="row">
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                         <?php echo $this->Html->image('/images/team/team1.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Denise Brewer</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team2.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Patrick Ryan</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team3.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Craig Robinson</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-3 col-md-12">
                  <div class="ts-team-wrapper">
                     <div class="team-img-wrapper">
                        <?php echo $this->Html->image('/images/team/team4.jpg',
                                 ['alt' => ''],
                                 ['class' => 'img-fluid']); ?>
                     </div>
                     <div class="ts-team-content">
                        <h3 class="team-name">Andrew Robinson</h3>
                        <p class="team-designation">Senior Project Manager</p>
                     </div>
                     <!-- Team content end-->
                     <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                     <!-- social-icons-->
                  </div>
                  <!-- Team wrapper 1 end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Section Team end-->


      <section id="ts-testimonial-slide" class="ts-testimonial-slide solid-bg">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="testimonial-slide owl-carousel owl-theme">
                     <div class="row quote-item-area">
                        <div class="col-md-5">
                           <div class="quote-thumb">
                               <?php echo $this->Html->image('/images/clients/testimonial1.png',
                                 ['alt' => 'Julia Mayer'],
                                 ['class' => 'quote-thumb-img']); ?>
                           </div>
                           <!-- Quote thumb end -->
                        </div>
                        <!-- Col end -->

                        <div class="col-md-7">
                           <div class="quote-item-content">
                              <h3 class="quote-name">Julia Mayer </h3>
                              <span class="quote-name-desg">
                                       HR Manager</span>
                              <p class="quote-message">
                                 The Forexnic loan has been the most attractive loan products on the market, would not be able to obtain conventionally and
                                 at extremely. </p>
                           </div>
                           <!-- Quote content end -->
                        </div>
                        <!-- Col end -->
                     </div>
                     <div class="row quote-item-area">
                        <div class="col-md-5">
                           <div class="quote-thumb">
                              <?php echo $this->Html->image('/images/clients/testimonial2.png',
                                 ['alt' => 'Julia Mayer'],
                                 ['class' => 'quote-thumb-img']); ?>
                           </div>
                           <!-- Quote thumb end -->
                        </div>
                        <!-- Col end -->

                        <div class="col-md-7">
                           <div class="quote-item-content">
                              <h3 class="quote-name">Julia Mayer </h3>
                              <span class="quote-name-desg">
                                       HR Manager</span>
                              <p class="quote-message">
                                 The Forexnic loan has been the most attractive loan products on the market, would not be able to obtain conventionally and
                                 at extremely. </p>
                           </div>
                           <!-- Quote content end -->
                        </div>
                        <!-- Col end -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="news" id="news">
         <div class="container">
            <div class="row text-center">
               <div class="col-md-12">
                  <h2 class="section-title"><span>Don't Miss</span>Our Latest News</h2>
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6">
                  <div class="latest-post post-large">
                     <div class="latest-post-media">
                         
                         <?php echo $this->Html->link(
                              $this->Html->image('/images/news/news1.jpg',
                              ['alt' => 'img'],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['class' => 'latest-post-img','escape' => false]); ?>
                         <?php echo $this->Html->link(
                                'News',
                                ['controller' => 'news', 'action' => 'new1'],
                                ['class' => 'post-cat']); ?>
                        <div class="post-body"><span class="post-item-date">20 Nov, 2017</span>
                           <h4 class="post-title"><?php echo $this->Html->link(
                                   "Spacex's interviewing process is rude as hell"),
                                   array('controller' => 'features', 'action' => 'feature4'); ?>
                           </h4><?php echo $this->Html->link(
                                   'Read More',
                                   array('controller' => 'features', 'action' => 'feature4'),
                                   array('class' => 'btn btn-primary')); ?>
                        </div>
                        <!-- Post body end-->
                     </div>
                     <!-- Post media end-->
                  </div>
                  <!-- Latest post end-->
               </div>
               <!-- Col big news end-->
               <div class="col-lg-6">
                  <div class="row">
                     <div class="col-lg-6">
                        <div class="latest-post">
                           <div class="post-body"><?php echo $this->Html->link(
                                'News',
                                ['controller' => 'news', 'action' => 'new1'],
                                ['class' => 'post-cat']); ?>
                              <h4 class="post-title"><?php echo $this->Html->link(
                                          'American Express finally ditches',
                                          ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h4><span class="post-item-date">20 Nov, 2017</span>
                              <div class="post-text">
                                 <p>Earlier this year, the firm announced it had reached its goal of hiring.</p>
                                 <div class="text-right"><?php echo $this->Html->link(
                                         'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                         array('controller' => 'features', 'action' => 'feature4')); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Latest post end-->
                     </div>
                     <!-- Col end-->
                     <div class="col-lg-6">
                        <div class="latest-post">
                           <div class="post-body"><?php echo $this->Html->link(
                                'News',
                                ['controller' => 'news', 'action' => 'new1'],
                                ['class' => 'post-cat']); ?>
                              <h4 class="post-title"><?php echo $this->Html->link(
                                          'Disney buys 21st Century Fox',
                                          ['controller' => 'features', 'action' => 'feature4']); ?>
                              </h4><span class="post-item-date">20 Nov, 2017</span>
                              <div class="post-text">
                                 <p>Earlier this year, the firm announced it had reached its goal of hiring.</p>
                                 <div class="text-right"><?php echo $this->Html->link(
                                         'Read More' . $this->Html->tag('i', '', array('class' => 'fa fa-long-arrow-right')),
                                         array('controller' => 'features', 'action' => 'feature4')); ?>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <!-- Latest post end-->
                     </div>
                     <!-- Col end-->
                  </div>
                  <!-- row end-->
               </div>
               <!-- Col small news end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- News end-->

      <section class="clients-area" id="clients-area">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 owl-carousel owl-theme text-center partners" id="partners-carousel">
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client1.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client2.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client3.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client4.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client5.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client6.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client7.png',
                              ['alt' => ''],
                              ['class' => 'img-fluid']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
               </div>
               <!-- Owl carousel end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Partners end-->

      <div class="map" id="map"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/index-5.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:03 GMT -->
</html>