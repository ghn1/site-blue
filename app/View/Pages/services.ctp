<body>

   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner5.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Service</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'feature4']) ?></li>
                        <li>service</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container no-padding" id="main-container">
         <div class="ts-services ts-service-pattern" id="ts-services">
            <div class="container">
               <div class="row text-center">
                  <div class="col-md-12">
                     <h2 class="section-title"><span>Our Services</span>What We Do</h2>
                  </div>
               </div>
               <!-- Title row end-->
               <div class="row">
                  <div class="col-lg-4 col-md-12">
                     <div class="ts-service-box">
                        <div class="ts-service-image-wrapper">
                            <?php echo $this->Html->image('/images/services/service1.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-service-content">
                           <span class="ts-service-icon">
                              <i class="icon icon-pie-chart2"></i>
                           </span>
                           <h3 class="service-title">Financial Planning</h3>
                           <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                              tea such as stocks.</p>
                           <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?></p>
                        </div>
                     </div>
                     <!-- Service1 end-->
                  </div>
                  <!-- Col 1 end-->
                  <div class="col-lg-4 col-md-12">
                     <div class="ts-service-box">
                        <div class="ts-service-image-wrapper">
                           <?php echo $this->Html->image('/images/services/service2.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-service-content">
                           <span class="ts-service-icon">
                              <i class="icon icon-tax"></i>
                           </span>
                           <h3 class="service-title">Tax Planning</h3>
                           <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                              tea such as stocks.</p>
                           <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?></p>
                        </div>
                     </div>
                     <!-- Service2 end-->
                  </div>
                  <!-- Col 2 end-->
                  <div class="col-lg-4 col-md-12">
                     <div class="ts-service-box">
                        <div class="ts-service-image-wrapper">
                           <?php echo $this->Html->image('/images/services/service3.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-service-content">
                           <span class="ts-service-icon">
                              <i class="icon icon-savings"></i>
                           </span>
                           <h3 class="service-title">Saving Strategy</h3>
                           <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                              tea such as stocks.</p>
                           <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?></p>
                        </div>
                     </div>
                     <!-- Service3 end-->
                  </div>
                  <!-- Col 3 end-->
               </div>
               <!-- Content 1 row end-->
               <div class="gap-60"></div>
               <div class="row">
                  <div class="col-lg-4 col-md-12">
                     <div class="ts-service-box">
                        <div class="ts-service-image-wrapper">
                           <?php echo $this->Html->image('/images/services/service4.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-service-content">
                           <span class="ts-service-icon">
                              <i class="icon icon-mutual-fund"></i>
                           </span>
                           <h3 class="service-title">Mutual Funds</h3>
                           <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                              tea such as stocks.</p>
                           <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?></p>
                        </div>
                     </div>
                     <!-- Service4 end-->
                  </div>
                  <!-- Col 4 end-->
                  <div class="col-lg-4 col-md-12">
                     <div class="ts-service-box">
                        <div class="ts-service-image-wrapper">
                           <?php echo $this->Html->image('/images/services/service5.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-service-content">
                           <span class="ts-service-icon">
                              <i class="icon icon-loan"></i>
                           </span>
                           <h3 class="service-title">Business Loan</h3>
                           <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                              tea such as stocks.</p>
                           <p><?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?></p>
                        </div>
                     </div>
                     <!-- Service5 end-->
                  </div>
                  <!-- Col 5 end-->
                  <div class="col-lg-4 col-md-12">
                     <div class="ts-service-box">
                        <div class="ts-service-image-wrapper">
                           <?php echo $this->Html->image('/images/services/service6.jpg',
                                        ['alt' => ''],
                                        ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-service-content">
                           <span class="ts-service-icon">
                              <i class="icon icon-consult"></i>
                           </span>
                           <h3 class="service-title">Insurance Consulting</h3>
                           <p>Mutual funds pool money from many investors to purchase broad range of investments, forward thinking
                              tea such as stocks.</p>
                           <p>
                               <?php echo $this->Html->link(
                                       'Read More' . $this->Html->tag('i', '', ['class' => 'icon icon-right-arrow2']),
                                       ['controller' => 'features', 'action' => 'feature4'],
                                       ['class' => 'link-more', 'escape' => false]); ?>
                           </p>
                        </div>
                     </div>
                     <!-- Service6 end-->
                  </div>
                  <!-- Col 6 end-->
               </div>
               <!-- Content Row 2 end-->
            </div>
            <!-- Container end-->
         </div>
      </section>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/service.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:28 GMT -->
</html>