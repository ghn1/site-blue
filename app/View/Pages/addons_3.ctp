<body>
   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner5.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">Addons</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?></li>
                        <li>Addons</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container" id="main-container">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <h3 class="addons-title">
                     Counter Up
                  </h3>
               </div>
            </div>
         </div>
         <div class="ts-facts-area-bg bg-overlay">
            <div class="container">
               <div class="row facts-wrapper text-center">
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-money-1"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">2435</span></h4>
                           <p class="facts-desc">Cases Completed</p>
                        </div>
                     </div>
                     <!-- Facts 1 end-->
                  </div>
                  <!-- Col 1 end-->
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-invest"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">467</span></h4>
                           <p class="facts-desc">Successful Investment</p>
                        </div>
                     </div>
                     <!-- Facts 2 end-->
                  </div>
                  <!-- Col 2 end-->
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-chart2"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">85</span>%</h4>
                           <p class="facts-desc">Business Growth </p>
                        </div>
                     </div>
                     <!-- Facts 3 end-->
                  </div>
                  <!-- Col 3 end-->
                  <div class="col-lg-3 col-md-3">
                     <div class="ts-facts-bg"><span class="facts-icon"><i class="icon icon-deal"></i></span>
                        <div class="ts-facts-content">
                           <h4 class="ts-facts-num"><span class="counterUp">139</span></h4>
                           <p class="facts-desc">Running Projects</p>
                        </div>
                     </div>
                     <!-- Facts 4 end-->
                  </div>
                  <!-- Col 4 end-->
               </div>
               <!-- Row end-->
            </div>
            <!-- Container 2 end-->
         </div>
      </section>

      <div class="container">
         <div class="row">
            <div class="col-12">
               <h3 class="addons-tile">Pricing Table</h3>
            </div>
            <div class="col-lg-12">
               <div class="pricing-boxed">
                  <div class="single-price-box">
                     <div class="pricing-plan">
                        <div class="pricing-header border-left">
                           <h2 class="plan-name">Starter Plan</h2>
                           <h3 class="plan-price">
                              <sup class="currency">$</sup>
                              <strong>49.99</strong>
                              <sub>/mo</sub>
                           </h3>
                        </div>
                        <ul class="list-unstyled">
                           <li>Invoices/Estimates</li>
                           <li>Online Payments</li>
                           <li>Projects and Time Sheet</li>
                           <li>Recurring Transations</li>
                           <li>Client Portal</li>
                        </ul>
                        <div>
                           <?php echo $this->Html->link(
                                   'Buy Now',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                   ['class' => 'btn btn-primary', 'target' => '_self']); ?>
                            <!--<a target="_self" href="#" class="btn btn-primary">Buy Now</a> -->
                        </div>
                     </div>
                  </div>
                  <div class="single-price-box featured">
                     <div class="pricing-plan">
                        <div class="pricing-header">
                           <h2 class="plan-name">
                              Basic Plan
                           </h2>
                           <h3 class="plan-price">
                              <sup class="currency">$</sup>
                              <strong>69.99</strong>
                              <sub>/mo</sub>
                           </h3>
                        </div>
                        <ul class="list-unstyled">
                           <li>Invoices/Estimates</li>
                           <li>Online Payments</li>
                           <li>Projects and Time Sheet</li>
                           <li>Recurring Transations</li>
                           <li>Client Portal</li>
                        </ul>
                        <div>
                           <?php echo $this->Html->link(
                                   'Buy Now',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                   ['class' => 'btn btn-primary', 'target' => '_self']); ?>
                        </div>
                     </div>
                  </div>
                  <div class="single-price-box">
                     <div class="pricing-plan">
                        <div class="pricing-header border-right">
                           <h2 class="plan-name">Advanced Plan</h2>
                           <h3 class="plan-price">
                              <sup class="currency">$</sup>
                              <strong>99.99</strong>
                              <sub>/mo</sub>
                           </h3>
                        </div>
                        <ul class="list-unstyled">
                           <li>Invoices/Estimates</li>
                           <li>Online Payments</li>
                           <li>Projects and Time Sheet</li>
                           <li>Recurring Transations</li>
                           <li>Client Portal</li>
                        </ul>
                        <div>
                           <?php echo $this->Html->link(
                                   'Buy Now',
                                   ['controller' => 'features', 'action' => 'feature4'],
                                   ['class' => 'btn btn-primary', 'target' => '_self']); ?>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>

      <div class="gap-100"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/addons-3.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:30 GMT -->
</html>