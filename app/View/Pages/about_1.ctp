<body>
   <div class="body-inner">

      <?php echo $this->element('header'); ?>

      <div class="banner-area" id="banner-area" style="background-image:url(images/banner/banner1.jpg);">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col">
                  <div class="banner-heading">
                     <h1 class="banner-title">About Us</h1>
                     <ol class="breadcrumb">
                        <li><?php echo $this->Html->link(
                                'Home',
                                ['controller' => 'home', 'action' => 'index']); ?>
                        </li>
                        <li>About Us</li>
                     </ol>
                  </div>
               </div>
               <!-- Col end-->
            </div>
            <!-- Row end-->
         </div>
         <!-- Container end-->
      </div>
      <!-- Banner area end-->
      <section class="main-container no-padding" id="main-container">

         <div class="about-pattern">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 about-desc">
                     <h2 class="column-title title-small"><span>About us</span>Different kind of Financial firm</h2>
                     <p>The mission of the Bizipress Financial Planning Association is to setup, promote and implement high
                        quality standards for competence and ethical behavior for the financial advisory sector.</p>
                     <div class="gap-15"></div>
                     <p>It’s an approach that consistently drives best decisions. And Bizipress, we carry out those decisions
                        free from the pressure of product bias.</p>
                     <div class="ceo-signature">
                        <div class="ceo-desc">
                            <?php echo $this->Html->image('/images/pages/profile_image.png',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                           <div class="ceo-designation">
                              <p>Daniel Statham</p>
                              <small>CEO Bizipress</small>
                           </div>
                        </div>
                        <!-- Desc End-->
                        <?php echo $this->Html->image('/images/pages/sign.png',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <!-- Signature End-->
                  </div>
                  <!-- Col end-->
                  <div class="col-lg-6 text-md-center mrt-40">
                      <?php echo $this->Html->image('/images/pages/work_circle.png',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                  </div>
                  <!-- Col end-->
               </div>
               <!-- Main row end-->
            </div>
            <!-- Container 1 end-->
         </div>
         <!-- About pattern End-->

         <div class="ts-skills-area solid-bg" id="ts-skills-area">
            <div class="container-fluid no-padding">
               <div class="row">
                  <div class="col-lg-6">
                     <div class="skills-image">
                         <?php echo $this->Html->image('/images/pages/about_1.png',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                     </div>
                     <!-- Skills image end-->
                  </div>
                  <!-- Col end-->
                  <div class="col-lg-6 ts-padding">
                     <h2 class="column-title title-small"><span>Our Skills</span>Our Reputation & Integrity</h2>
                     <p class="intro-desc">A business strategy is the means by which it sets out to achieve its desired ends. You have ideas, goals,
                        and dreams. We have a culturally diverse, forward thinking team looking for talent like you and make
                        your dream come true.</p>
                     <div class="gap-20"></div>
                     <div class="ts-progress-bar">
                        <h3>Planning</h3>
                        <div class="progress">
                           <div class="progress-bar" style="width:75%; background:blue;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                              <div class="progress-value">75%</div>
                           </div>
                        </div>
                     </div>
                     <!-- End ts-progress bar 1-->
                     <div class="ts-progress-bar">
                        <h3>Consulting</h3>
                        <div class="progress">
                           <div class="progress-bar" style="width:90%; background:blue;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                              <div class="progress-value">90%</div>
                           </div>
                        </div>
                     </div>
                     <!-- End ts-progress bar 2-->
                     <div class="ts-progress-bar">
                        <h3>Marketing</h3>
                        <div class="progress">
                           <div class="progress-bar" style="width:80%; background:blue;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                              <div class="progress-value">80%</div>
                           </div>
                        </div>
                     </div>
                     <!-- End ts-progress bar 3-->
                  </div>
                  <!-- Col end-->
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container 2 end-->
         </div>
         <!-- Skills area end-->
         <div class="gap-100"></div>

         <div class="ts-team">
            <div class="container">
               <div class="row text-center">
                  <div class="col-md-12">
                     <h2 class="section-title"><span>Our People</span>Best Team</h2>
                  </div>
               </div>
               <!-- Title row end-->
               <div class="row">
                  <div class="col-lg-3 col-md-6">
                     <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                            <?php echo $this->Html->image('/images/team/team1.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-team-content">
                           <h3 class="team-name">Denise Brewer</h3>
                           <p class="team-designation">Senior Project Manager</p>
                        </div>
                        <!-- Team content end-->
                        <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team wrapper 1 end-->
                  </div>
                  <!-- Col end-->
                  <div class="col-lg-3 col-md-6">
                     <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                           <?php echo $this->Html->image('/images/team/team2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-team-content">
                           <h3 class="team-name">Patrick Ryan</h3>
                           <p class="team-designation">Senior Project Manager</p>
                        </div>
                        <!-- Team content end-->
                        <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team wrapper 1 end-->
                  </div>
                  <!-- Col end-->
                  <div class="col-lg-3 col-md-6">
                     <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                           <?php echo $this->Html->image('/images/team/team2.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-team-content">
                           <h3 class="team-name">Craig Robinson</h3>
                           <p class="team-designation">Senior Project Manager</p>
                        </div>
                        <!-- Team content end-->
                        <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team wrapper 1 end-->
                  </div>
                  <!-- Col end-->
                  <div class="col-lg-3 col-md-6">
                     <div class="ts-team-wrapper">
                        <div class="team-img-wrapper">
                           <?php echo $this->Html->image('/images/team/team4.jpg',
                                    ['alt' => ''],
                                    ['class' => 'img-fluid']); ?>
                        </div>
                        <div class="ts-team-content">
                           <h3 class="team-name">Andrew Robinson</h3>
                           <p class="team-designation">Senior Project Manager</p>
                        </div>
                        <!-- Team content end-->
                        <div class="team-social-icons"><a target="_blank" href="#"><i class="fa fa-facebook"></i></a><a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                           <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a><a target="_blank" href="#"><i class="fa fa-linkedin"></i></a></div>
                        <!-- social-icons-->
                     </div>
                     <!-- Team wrapper 1 end-->
                  </div>
                  <!-- Col end-->
               </div>
               <!-- Content row end-->
            </div>
            <!-- Container end-->
         </div>
         <!-- Section Team end-->
         <div class="gap-100"></div>
      </section>

      <section class="quote-area bg-overlay overlay-color" id="quote-area">
         <div class="container">
            <div class="row">
               <div class="col-lg-6 align-self-center">
                  <div class="owl-carousel owl-theme testimonial-slide owl-dark" id="testimonial-slide">
                     <div class="item">
                        <div class="quote-item quote-square"><span class="quote-text">The Bizipress loan has been  the most attractive loan products on the market, helping numerous businesses gain access to financing they would not be able to</span>
                           <div class="quote-item-footer">
                               <?php echo $this->Html->image('/images/clients/testimonial1.png',
                                      ['class' => 'testimonial-thumb'],
                                       ['alt' => 'testimonial']); ?>
                              <div class="quote-item-info">
                                 <p class="quote-author">Gabriel Denis</p><span class="quote-subtext">Chairman, OKT</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                     <!-- Item 1 end-->
                     <div class="item">
                        <div class="quote-item quote-square"><span class="quote-text">The Bizipress loan has been  the most attractive loan products on the market, helping numerous businesses gain access to financing they would not be able to</span>
                           <div class="quote-item-footer">
                              <?php echo $this->Html->image('/images/clients/testimonial2.png',
                                      ['class' => 'testimonial-thumb'],
                                       ['alt' => 'testimonial']); ?>
                              <div class="quote-item-info">
                                 <h3 class="quote-author">Weldon Cash</h3><span class="quote-subtext">CFO, First Choice</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                     <!-- Item 2 end-->
                     <div class="item">
                        <div class="quote-item quote-square"><span class="quote-text">The Bizipress loan has been  the most attractive loan products on the market, helping numerous businesses gain access to financing they would not be able to</span>
                           <div class="quote-item-footer">
                              <?php echo $this->Html->image('/images/clients/testimonial3.png',
                                      ['class' => 'testimonial-thumb'],
                                       ['alt' => 'testimonial']); ?>
                              <div class="quote-item-info">
                                 <h3 class="quote-author">Minter Puchan</h3><span class="quote-subtext">Director, AKT</span>
                              </div>
                           </div>
                        </div>
                        <!-- Quote item end-->
                     </div>
                     <!-- Item 3 end-->
                  </div>
                  <!-- Testimonial carousel end-->
               </div>
               <!-- Col end-->
               <div class="col-lg-6 qutoe-form-inner-right">
                  <div class="quote_form">
                     <h2 class="column-title title-white"><span>We are always ready</span> Request a call back</h2>
                     <div class="row">
                        <div class="col-md-6">
                           <div class="form-group">
                              <input class="form-control" id="name" name="name" placeholder="Full Name" required="">
                           </div>
                        </div>
                        <div class="col-md-6">
                           <div class="form-group">
                              <input class="form-control" id="email" name="email" placeholder="Email Address" required="">
                           </div>
                        </div>
                     </div>
                     <!-- Row 1 end-->
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group">
                              <input class="form-control" id="subject" name="subject" type="text" placeholder="Subject" required="">
                           </div>
                        </div>
                     </div>
                     <!-- Row end-->
                     <div class="row">
                        <div class="col-xs-12 col-md-12">
                           <div class="form-group">
                              <textarea class="form-control" placeholder="Message" rows="6" name="comment" required=""></textarea>
                           </div>
                        </div>
                     </div>
                     <!-- Row end-->
                     <div class="form-group text-right mb-0">
                        <input class="button btn btn-primary" type="submit" value="Send Message">
                     </div>
                  </div>
                  <!-- Quote form end-->
               </div>
               <!-- Col end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Quote area end-->

      <section class="clients-area bg-white" id="clients-area">
         <div class="container">
            <div class="row">
               <div class="col-sm-12 owl-carousel owl-theme text-center partners" id="partners-carousel">
                  <figure class="item partner-logo">
                      <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client1.png',
                                      ['class' => 'img-fluid'],
                                      ['alt' => '']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client2.png',
                                      ['class' => 'img-fluid'],
                                      ['alt' => '']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client3.png',
                                      ['class' => 'img-fluid'],
                                      ['alt' => '']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client4.png',
                                      ['class' => 'img-fluid'],
                                      ['alt' => '']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client5.png',
                                      ['class' => 'img-fluid'],
                                      ['alt' => '']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client6.png',
                                      ['class' => 'img-fluid'],
                                      ['alt' => '']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
                  <figure class="item partner-logo last">
                     <?php echo $this->Html->link(
                              $this->Html->image('/images/clients/client7.png',
                                      ['class' => 'img-fluid'],
                                      ['alt' => '']),
                              ['controller' => 'features', 'action' => 'feature4'],
                              ['escape' => false]); ?>
                  </figure>
               </div>
               <!-- Owl carousel end-->
            </div>
            <!-- Content row end-->
         </div>
         <!-- Container end-->
      </section>
      <!-- Partners end-->
      <div class="gap-40"></div>

      <?php echo $this->element('footer'); ?>

      <div class="back-to-top affix" id="back-to-top" data-spy="affix" data-offset-top="10">
         <button class="btn btn-primary" title="Back to Top"><i class="fa fa-angle-double-up"></i>
            <!-- icon end-->
         </button>
         <!-- button end-->
      </div>
      <!-- End Back to Top-->

      <!--
      Javascript Files
      ==================================================
      -->
      <!-- initialize jQuery Library-->
      <script type="text/javascript" src="js/jquery.js"></script>
      <!-- Popper-->
      <script type="text/javascript" src="js/popper.min.js"></script>
      <!-- Bootstrap jQuery-->
      <script type="text/javascript" src="js/bootstrap.min.js"></script>
      <!-- Owl Carousel-->
      <script type="text/javascript" src="js/owl.carousel.min.js"></script>
      <!-- Counter-->
      <script type="text/javascript" src="js/jquery.counterup.min.js"></script>
      <!-- Waypoints-->
      <script type="text/javascript" src="js/waypoints.min.js"></script>
      <!-- Color box-->
      <script type="text/javascript" src="js/jquery.colorbox.js"></script>
      <!-- Smoothscroll-->
      <script type="text/javascript" src="js/smoothscroll.js"></script>
      <!-- Google Map API Key-->
      <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCsa2Mi2HqyEcEnM1urFSIGEpvualYjwwM"></script>
      <!-- Google Map Plugin-->
      <script type="text/javascript" src="js/gmap3.js"></script>
      <!-- Template custom-->
      <script type="text/javascript" src="js/custom.js"></script>
   </div>
   <!--Body Inner end-->
</body>


<!-- Mirrored from themewinter.com/demo/html/bizipress/blue/about-1.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 18 Apr 2018 12:46:07 GMT -->
</html>