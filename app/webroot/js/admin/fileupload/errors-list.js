var fileUploadErrors = {
    1: 'O arquivo enviado excede a diretiva upload_max_filesize no php.ini',
    2: 'O arquivo enviado excede a directiva MAX_FILE_SIZE que foi especificado no formulário HTML',
    3: 'O arquivo enviado foi apenas parcialmente carregado',
    4: 'Nenhum arquivo foi enviado',
    6: 'Faltando uma pasta temporária',
    7: 'Falha ao gravar o arquivo em disco',
    8: 'A extensão PHP parou o upload de arquivos',
    postMaxSize: 'O arquivo enviado excede a directiva post_max_size no php.ini',
    maxFileSize: 'O arquivo é muito grande',
    acceptFileTypes: 'Tipo de arquivo não permitido',
    maxNumberOfFiles: 'O número máximo de arquivos excedeu',
    uploadedBytes: 'Bytes enviados excedeu o tamanho do arquivo',
    minFileSize: 'O arquivo é muito pequeno',
    emptyResult: 'Erro no upload do arquivo'
};