$(document).ready(function(){
    
    /* ==========================================================================
     Scroll
     ========================================================================== */

    if (!("ontouchstart" in document.documentElement)) {

            document.documentElement.className += " no-touch";

            var jScrollOptions = {
                    autoReinitialise: true,
                    autoReinitialiseDelay: 100
            };

            $('.scrollable .box-typical-body').jScrollPane(jScrollOptions);
            $('.side-menu').jScrollPane(jScrollOptions);
            $('.side-menu-addl').jScrollPane(jScrollOptions);
            $('.scrollable-block').jScrollPane(jScrollOptions);
    }
    
    /* ==========================================================================
     Side menu list
     ========================================================================== */

    $('.side-menu-list li.with-sub').each(function () {
        var parent = $(this),
                clickLink = parent.find('>span'),
                subMenu = parent.find('>ul');
        clickLink.click(function () {
            if (parent.hasClass('opened')) {
                parent.removeClass('opened');
                subMenu.slideUp();
                subMenu.find('.opened').removeClass('opened');
            } else {
                if (clickLink.parents('.with-sub').size() == 1) {
                    $('.side-menu-list .opened').removeClass('opened').find('ul').slideUp();
                }
                parent.addClass('opened');
                subMenu.slideDown();
            }
        });
    });

    /* ==========================================================================
    Header mobile menu
    ========================================================================== */

    // Left mobile menu
    $('.hamburger').click(function () {
        if ($('body').hasClass('menu-left-opened')) {
            $(this).removeClass('is-active');
            $('body').removeClass('menu-left-opened');
            $('html').css('overflow', 'auto');
        } else {
            $(this).addClass('is-active');
            $('body').addClass('menu-left-opened');
            $('html').css('overflow', 'hidden');
        }
    });

    /* ==========================================================================
    Bootstrap maxlength
    ========================================================================== */
    
    if ($(':input.form-control[maxlength]').length)
        $(':input.form-control[maxlength]').maxlength();

    /* ==========================================================================
    Select2
    ========================================================================== */
    
    var language = {
        'noResults': function(){return 'Nenhum resultado encontrado.';}
    };
    
    if ($('select.select2').length) {
        $('select.select2').select2({
            'language': language
        });
    }
    
    if ($('select.select2-arrow').length) {
        $('select.select2-arrow').select2({
            theme: 'arrow',
            'language': language
        });
    }

    /* ==========================================================================
    Ladda Buttons Submit
    ========================================================================== */
    
    $('.ladda-button:submit').on('click', function() {
        Ladda.create(this).start();
        $(this).removeAttr('disabled').submit();
    });

    /* ==========================================================================
    Form Group Remove Error Focus
    ========================================================================== */
    
    $('.form-error').on('focusin', function(){
        $(this).parents('.error').find('.form-tooltip-error').remove();
        $(this).parents('.error').removeClass('error');
        $(this).removeClass('form-error');
    });

    /* ==========================================================================
    Datatable Row Re-order Position
    ========================================================================== */
    
    if ($('#table.reorder').length) {
        var table = $('#table.reorder').DataTable({
            autoWidth: false,
            info: false,
            length: false,
            ordering: false,
            paging: false,
            searching: false,
            rowReorder: {
                selector: 'td:not(.bs-checkbox,.action)'
            }
        }).on('row-reorder', function (e, diff, edit) {
            var data = Array(), index = null, position = null;
            for (var i = 0, ien = diff.length; i < ien; i++) {
                index = $('#table tbody tr:eq('+diff[i].oldPosition+')').data('index');
                position = $('#table tbody tr:eq('+diff[i].newPosition+')').find('td.position').text();
                data[i] = '"' + index + '":"' + position + '"';
            }
            if (data.length > 0) {
                $('.bootstrap-table').block({
                    message: '<div class="blockui-default-message"><i class="fa fa-circle-o-notch fa-spin"></i><h6>Reordenando...<br>Por favor, aguarde!</h6></div>',
                    overlayCSS: {background: 'rgba(142, 159, 167, 0.8)', opacity: 1, cursor: 'wait'},
                    css: {width: '50%'},
                    blockMsgClass: 'block-msg-default'
                });
                $.ajax({
                    data: {reorder: '{' + data + '}'},
                    type: 'post',
                    success: function() {
                        location.reload();
                    }
                });
            }
        });
    }

    /* ==========================================================================
    Popovers
    ========================================================================== */
    
    if ($('[data-toggle="popover"]').length)
        $('[data-toggle="popover"]').popover({trigger: 'focus'});

    /* ==========================================================================
    Bar Buttons Fixed
    ========================================================================== */
    
    if ($('.bar-buttons').length) {
        $(window).bind({
            scroll: function(){
                var pageContentHeight = $('.page-content').innerHeight();
                var barbuttonsHeight = 59;
                var scrollTop = $(this).scrollTop();
                var windowHeight = $(window).innerHeight();
                if (scrollTop + windowHeight < pageContentHeight - barbuttonsHeight)
                    $('.page-content').addClass('bar-buttons-fixed');
                else
                    $('.page-content').removeClass('bar-buttons-fixed');
            },
            resize: function() {
                $(window).trigger('scroll');
            },
            load: function() {
                $(window).trigger('scroll');
            }
        });
    }

});