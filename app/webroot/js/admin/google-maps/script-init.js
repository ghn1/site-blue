var mapMarker = new google.maps.MarkerImage(
                    '/img/admin/google-map-marker.png', // my 16x48 sprite with 3 circular icons
                    new google.maps.Size(28,28),
                    null,
                    null,
                    new google.maps.Size(28,28)
                );

// Jales
function initMap() {
    var secheltLoc = new google.maps.LatLng(-20.274320, -50.546387);

    var myMapOptions = {
        zoom: 15,
        center: secheltLoc,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var theMap = new google.maps.Map(document.getElementById("map_canvas"), myMapOptions);


    var marker = new google.maps.Marker({
        position: {lat: -20.274320, lng: -50.546387},
        map: theMap,
        icon: mapMarker
    });

    var myOptions = {
        content:
            "<p><i class='font-icon font-icon-pin'></i>Rua França, 2305, Jardim Micena</p>" +
            "<p><i class='font-icon font-icon-phone'></i>(17) 3621 1752 &nbsp; <i class='fa fa-whatsapp'></i> (17) 99705 8799</p>" +
            "<p><i class='font-icon font-icon-mail'></i>tribodp@gmail.com</p>" +
            "<p><i class='font-icon fa fa-skype'></i>agtribo</p>",
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-170, 0),
        zIndex: null,
        boxStyle: {
            width: "340px"
        },
        closeBoxURL: "/img/admin/close.png",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: false
    };

    google.maps.event.addListener(marker, "click", function (e) {
        ib.open(theMap, this);
    });

    var ib = new InfoBox(myOptions);

    ib.open(theMap, marker);
}

// Fernandópolis
function initMap2() {
    var secheltLoc = new google.maps.LatLng(-20.2837055, -50.2521861);

    var myMapOptions = {
        zoom: 15,
        center: secheltLoc,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var theMap = new google.maps.Map(document.getElementById("map_canvas_2"), myMapOptions);


    var marker = new google.maps.Marker({
        position: {lat: -20.2837055, lng: -50.2521861},
        map: theMap,
        icon: mapMarker
    });

    var myOptions = {
        content:
            "<p><i class='font-icon font-icon-pin'></i>Av. dos Arnaldos, 1333, 2º andar, sala 10, Centro</p>" +
            "<p><i class='font-icon font-icon-phone'></i>(17) 99665 6621</p>" +
            "<p><i class='font-icon font-icon-mail'></i>tribofernandopolis@gmail.com</p>",
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-210, 0),
        zIndex: null,
        boxStyle: {
            width: "420px"
        },
        closeBoxURL: "/img/admin/close.png",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: false
    };

    google.maps.event.addListener(marker, "click", function (e) {
        ib.open(theMap, this);
    });

    var ib = new InfoBox(myOptions);

    ib.open(theMap, marker);
}


$(document).ready(function() {

    initMap();

    function mapContactsHeight() {
        $('.contacts-page').each(function(){
            var box = $(this),
                boxHeader = box.find('.box-typical-header'),
                rightCol = box.find('.contacts-page-col-right:visible'),
                map = box.find('.map');

            if($(window).width() > 700) {
                if (!box.hasClass('box-typical-full-screen')) {
                    map.height(
                        $(window).height() -
                        parseInt($('.page-content').css('padding-top')) -
                        parseInt($('.page-content').css('padding-bottom')) -
                        parseInt(box.css('margin-bottom')) - 2 -
                        boxHeader.outerHeight()
                    );
                } else {
                    map.height(
                        $(window).height() - 2 - boxHeader.outerHeight()
                    );
                }

                if (map.height() < rightCol.outerHeight()) {
                    map.height(rightCol.outerHeight())
                }
            }
        });
    }

    mapContactsHeight();

    $(window).resize(function(){
        mapContactsHeight();
    });

    $('a[href="#jales"]').on('shown.bs.tab', function (e) {
        mapContactsHeight();
        initMap();
    });

    $('a[href="#fernandopolis"]').on('shown.bs.tab', function (e) {
        mapContactsHeight();
        initMap2();
    });

    $('.contacts-page').each(function(){
        var parent = $(this),
            btnExpand = parent.find('.action-btn-expand'),
            classExpand = 'box-typical-full-screen';

        btnExpand.click(function(){
            if (parent.hasClass(classExpand)) {
                parent.removeClass(classExpand);
                $('html').css('overflow','auto');
                parent.find('.tab-content').height('auto').css('overflow','visible');

                console.log('close');
            } else {
                parent.addClass(classExpand);
                $('html').css('overflow','hidden');
                parent.find('.tab-content').css('overflow','auto').height(
                    $(window).height() - 2 - parent.find('.box-typical-header').outerHeight()
                );

                console.log('open');
            }
            mapContactsHeight();
            initMap();
            initMap2();
        });
    });
    
    if (window.location.hash) {
        $('a[href="' + window.location.hash + '"]').trigger('click');
    }

});