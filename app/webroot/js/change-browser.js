/**
 * Change Browser jQuery v1.0
 * 
 * @author Cesar Augustus Silva <cesar@infoartweb.com>
 * @copyright Copyright (c) 2015, InfoArt
 * @license http://www.gnu.org/licenses/gpl.html GNU GPL v3.0
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <­http://www.gnu.org/licenses/>.
 */

jQuery.noConflict()(function($) {
    var supported = $.support.ajax && Modernizr.borderradius && Modernizr.boxshadow && Modernizr.json &&
                    Modernizr.cssgradients && Modernizr.csstransitions && Modernizr.fontface &&
                    Modernizr.opacity && Modernizr.rgba && Modernizr.textshadow && Modernizr.canvas &&
                    Modernizr.cookies && Modernizr.eventlistener && Modernizr.history && Modernizr.svg &&
                    Modernizr.cssanimations && Modernizr.ligatures && Modernizr.messagechannel &&
                    Modernizr.notification && Modernizr.queryselector && Modernizr.canvastext &&
                    Modernizr.webgl && Modernizr.appearance && Modernizr.multiplebgs && Modernizr.video &&
                    Modernizr.bgpositionxy && Modernizr.csscalc && Modernizr.checked && Modernizr.ellipsis &&
                    Modernizr.cssfilters && Modernizr.fontface && Modernizr.lastchild && Modernizr.nthchild;
    if (!supported) {
        var ela = $(document.createElement("a"));
        var elli = $(document.createElement("li")).append(ela);
        var elul = $(document.createElement("ul")).append(elli);
        var elclose = $(document.createElement("a")).addClass("close");
        var eltitle = $(document.createElement("p")).addClass("title");
        var eltext = $(document.createElement("p")).addClass("text");
        var elbody = $(document.createElement("div"))
                        .addClass("body")
                        .append(elclose, eltitle, eltext, elul);
        var elbrowser = $(document.createElement("div"))
                            .attr("id", "change-browser")
                            .append(elbody);
        $("body").append(elbrowser);
        $("html").css({ overflow: "hidden" });
        $("#change-browser .close").attr("href", "javascript:;");
        $("#change-browser .title").html("A versão do seu navegador de internet não é suportada!");
        $("#change-browser .text").html("Esta versão não suporta todos os recursos. Atualize seu navegador com uma das opções abaixo:");

        var chrome = $("#change-browser ul li:first-child").clone(true);
        chrome.find("a").addClass("chrome")
            .attr("href", "http://www.google.com/chrome/")
            .attr("target", "_blank")
            .html("Chrome");

        var edge = $("#change-browser ul li:first-child").clone(true);
        edge.find("a").addClass("edge")
            .attr("href", "https://www.microsoft.com/pt-br/windows/microsoft-edge")
            .attr("target", "_blank")
            .html("Edge");
    
        var firefox = $("#change-browser ul li:first-child").clone(true);
        firefox.find("a").addClass("firefox")
            .attr("href", "http://www.mozilla.org/firefox/")
            .attr("target", "_blank")
            .html("Firefox");

        var opera = $("#change-browser ul li:first-child").clone(true);
        opera.find("a").addClass("opera")
            .attr("href", "http://www.opera.com/")
            .attr("target", "_blank")
            .html("Opera");

        var safari = $("#change-browser ul li:first-child").clone(true);
        safari.find("a").addClass("safari")
            .attr("href", "http://support.apple.com/pt_BR/downloads/#safari")
            .attr("target", "_blank")
            .html("Safari");

        $("#change-browser ul li:last-child").after(chrome, edge, firefox, opera, safari);

        $("#change-browser ul li:first-child").remove();

        $("#change-browser .body").fadeIn("slow", function(){
            $("#change-browser .close").click(function(){
                $("#change-browser .body").fadeOut("fast", function() {
                    $("html").css({ overflow: "" });
                    $("#change-browser").remove();
                });
            });
        });
    }
});