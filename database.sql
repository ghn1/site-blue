SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Identidade',
  `email` varchar(100) NOT NULL COMMENT 'E-mail',
  `password` varchar(50) NOT NULL COMMENT 'Senha',
  `name` varchar(100) NOT NULL COMMENT 'Nome',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Usuários';

INSERT IGNORE INTO `users` (`id`, `email`, `password`, `name`) VALUES
(1, 'root', 'c069b5694a18b8303fe0c3ebadf1f6d0', 'Administrador');
